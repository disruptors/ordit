module.exports = {
  moduleNameMapper: {
    '^@/business(.*)$': '<rootDir>/app/javascript/business_app$1',
    '^@/customer(.*)$': '<rootDir>/app/javascript/customer_app$1',
    '^@/utils(.*)$': '<rootDir>/app/javascript/utils$1',
    '^@/store(.*)$': '<rootDir>/app/javascript/store$1',
    '^@/api(.*)$': '<rootDir>/app/javascript/api$1',
  },
};
