# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_17_135256) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "businesses", force: :cascade do |t|
    t.string "name"
    t.string "type"
    t.string "location"
    t.string "phone"
    t.string "email"
    t.string "fb_link"
    t.bigint "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "current_menu_id"
    t.index ["user_id"], name: "index_businesses_on_user_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.boolean "active", default: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "business_id"
    t.index ["business_id"], name: "index_categories_on_business_id"
    t.index ["name", "business_id"], name: "index_categories_on_name_and_business_id", unique: true
  end

  create_table "choice_options", force: :cascade do |t|
    t.bigint "choice_id"
    t.string "name", null: false
    t.string "price"
    t.boolean "active", default: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["choice_id"], name: "index_choice_options_on_choice_id"
  end

  create_table "choices", force: :cascade do |t|
    t.bigint "menu_item_id"
    t.string "name", null: false
    t.boolean "active", default: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "optional", default: false
    t.index ["menu_item_id"], name: "index_choices_on_menu_item_id"
  end

  create_table "customers", force: :cascade do |t|
    t.bigint "sitting_id"
    t.string "ip_address"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "order_id"
    t.integer "business_id"
    t.integer "qr_code_id"
    t.string "pin_number_used"
    t.datetime "expired_at"
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "name"
    t.string "email"
    t.json "tokens"
    t.index ["confirmation_token"], name: "index_customers_on_confirmation_token", unique: true
    t.index ["email"], name: "index_customers_on_email", unique: true
    t.index ["sitting_id"], name: "index_customers_on_sitting_id"
    t.index ["uid", "provider"], name: "index_customers_on_uid_and_provider", unique: true
  end

  create_table "feedbacks", force: :cascade do |t|
    t.integer "rate"
    t.text "comment"
    t.bigint "business_id"
    t.bigint "customer_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["business_id"], name: "index_feedbacks_on_business_id"
    t.index ["customer_id"], name: "index_feedbacks_on_customer_id"
  end

  create_table "menu_items", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "price"
    t.bigint "menu_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "business_id"
    t.bigint "category_id"
    t.integer "menu_item_id"
    t.string "status"
    t.index ["business_id"], name: "index_menu_items_on_business_id"
    t.index ["category_id"], name: "index_menu_items_on_category_id"
    t.index ["menu_id"], name: "index_menu_items_on_menu_id"
  end

  create_table "menus", force: :cascade do |t|
    t.string "name"
    t.boolean "is_primary"
    t.bigint "business_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["business_id"], name: "index_menus_on_business_id"
  end

  create_table "order_items", force: :cascade do |t|
    t.bigint "order_id"
    t.bigint "customer_id"
    t.bigint "menu_item_id"
    t.string "order_status"
    t.money "amount", scale: 2, default: "0.0"
    t.index ["customer_id"], name: "index_order_items_on_customer_id"
    t.index ["menu_item_id"], name: "index_order_items_on_menu_item_id"
    t.index ["order_id"], name: "index_order_items_on_order_id"
  end

  create_table "orders", force: :cascade do |t|
    t.bigint "qr_code_id"
    t.bigint "sitting_id"
    t.bigint "business_id"
    t.string "order_status"
    t.money "total_amount", scale: 2, default: "0.0"
    t.boolean "paid", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "customer_id"
    t.integer "menu_item_id"
    t.integer "quantity"
    t.money "amount", scale: 2, default: "0.0"
    t.index ["business_id"], name: "index_orders_on_business_id"
    t.index ["qr_code_id"], name: "index_orders_on_qr_code_id"
    t.index ["sitting_id"], name: "index_orders_on_sitting_id"
  end

  create_table "plans", force: :cascade do |t|
    t.bigint "user_id"
    t.string "name"
    t.integer "menu_item_limit", default: 0
    t.integer "menu_item_image_limit", default: 0
    t.integer "qr_code_limit", default: 0
    t.decimal "price", precision: 8, scale: 2
    t.index ["user_id"], name: "index_plans_on_user_id"
  end

  create_table "qr_codes", force: :cascade do |t|
    t.string "name"
    t.boolean "active"
    t.string "pin_number"
    t.bigint "business_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "svg"
    t.integer "current_sitting_id"
    t.index ["business_id"], name: "index_qr_codes_on_business_id"
  end

  create_table "sittings", force: :cascade do |t|
    t.boolean "active"
    t.bigint "qr_code_id"
    t.bigint "business_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["business_id"], name: "index_sittings_on_business_id"
    t.index ["qr_code_id"], name: "index_sittings_on_qr_code_id"
  end

  create_table "tags", force: :cascade do |t|
    t.bigint "business_id"
    t.string "name"
    t.boolean "active", default: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["business_id"], name: "index_tags_on_business_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.boolean "allow_password_change", default: false
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "name"
    t.string "nickname"
    t.string "image"
    t.string "email"
    t.json "tokens"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "role"
    t.boolean "onboarding_completed", default: false
    t.integer "current_business_id"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "businesses", "menus", column: "current_menu_id", on_delete: :nullify
  add_foreign_key "businesses", "users"
  add_foreign_key "categories", "businesses"
  add_foreign_key "choice_options", "choices"
  add_foreign_key "choices", "menu_items"
  add_foreign_key "feedbacks", "businesses"
  add_foreign_key "feedbacks", "customers"
  add_foreign_key "menu_items", "businesses"
  add_foreign_key "menu_items", "categories"
  add_foreign_key "menu_items", "menus"
  add_foreign_key "menus", "businesses"
  add_foreign_key "order_items", "customers"
  add_foreign_key "order_items", "menu_items"
  add_foreign_key "order_items", "orders"
  add_foreign_key "orders", "businesses"
  add_foreign_key "orders", "qr_codes"
  add_foreign_key "orders", "sittings"
  add_foreign_key "plans", "users"
  add_foreign_key "qr_codes", "businesses"
  add_foreign_key "sittings", "businesses"
  add_foreign_key "sittings", "qr_codes"
  add_foreign_key "tags", "businesses"
  add_foreign_key "users", "businesses", column: "current_business_id", on_delete: :nullify
end
