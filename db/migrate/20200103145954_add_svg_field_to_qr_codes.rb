class AddSvgFieldToQrCodes < ActiveRecord::Migration[6.0]
  def change
    add_column :qr_codes, :svg, :text
  end
end
