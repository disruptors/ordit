class AddFksToCustomer < ActiveRecord::Migration[6.0]
  def change
    add_column :customers, :order_id, :integer
    add_column :customers, :business_id, :integer
    add_column :customers, :qr_code_id, :integer
    add_column :customers, :pin_number_used, :string
  end
end
