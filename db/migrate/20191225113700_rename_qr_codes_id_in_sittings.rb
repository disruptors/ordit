class RenameQrCodesIdInSittings < ActiveRecord::Migration[6.0]
  def change
    rename_column :sittings, :qr_codes_id, :qr_code_id
  end
end
