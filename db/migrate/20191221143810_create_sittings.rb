class CreateSittings < ActiveRecord::Migration[6.0]
  def change
    create_table :sittings do |t|
      t.boolean :active
      t.references :qr_codes, foreign_key: true
      t.references :business, foreign_key: true

      t.timestamps
    end
  end
end
