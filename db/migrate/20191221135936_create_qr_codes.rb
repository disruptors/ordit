class CreateQrCodes < ActiveRecord::Migration[6.0]
  def change
    create_table :qr_codes do |t|
      t.string :name
      t.boolean :active
      t.string :pin_number
      t.references :business, foreign_key: true

      t.timestamps
    end
  end
end
