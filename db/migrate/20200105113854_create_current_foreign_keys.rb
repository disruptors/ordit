class CreateCurrentForeignKeys < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :current_business_id, :integer
    add_column :businesses, :current_menu_id, :integer

    add_foreign_key :users, :businesses, column: :current_business_id, on_delete: :nullify
    add_foreign_key :businesses, :menus, column: :current_menu_id, on_delete: :nullify
  end
end
