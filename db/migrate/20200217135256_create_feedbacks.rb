class CreateFeedbacks < ActiveRecord::Migration[6.0]
  def change
    create_table :feedbacks do |t|
      t.integer :rate
      t.text :comment

      t.references :business, foreign_key: true
      t.references :customer, foreign_key: true

      t.timestamps
    end
  end
end
