class AddFieldsForOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :quantity, :integer
    add_column :orders, :amount, :money, default: 0
  end
end
