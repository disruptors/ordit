class CreateMenus < ActiveRecord::Migration[6.0]
  def change
    create_table :menus do |t|
      t.string :name
      t.boolean :is_primary
      t.references :business, foreign_key: true

      t.timestamps
    end
  end
end
