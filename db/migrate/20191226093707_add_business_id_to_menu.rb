class AddBusinessIdToMenu < ActiveRecord::Migration[6.0]
  def change
    add_reference :menu_items, :business, foreign_key: true
  end
end
