class AddIndexNameBusinessIdToCategories < ActiveRecord::Migration[6.0]
  def change
    add_index :categories, [:name, :business_id], unique: true
  end
end
