class AddBusinessReferenceToCategories < ActiveRecord::Migration[6.0]
  def change
    add_reference :categories, :business, foreign_key: true
  end
end
