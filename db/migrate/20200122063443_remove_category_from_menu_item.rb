class RemoveCategoryFromMenuItem < ActiveRecord::Migration[6.0]
  def change
    remove_column :menu_items, :category, :string
  end
end
