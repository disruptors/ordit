class CreateTags < ActiveRecord::Migration[6.0]
  def change
    create_table :tags do |t|
      t.references :business, foreign_key: true
      t.string :name
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
