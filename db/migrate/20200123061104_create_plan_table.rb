class CreatePlanTable < ActiveRecord::Migration[6.0]
  def change
    create_table :plans do |t|
      t.references :user, foreign_key: true
      t.string :name
      t.integer :menu_item_limit, default: 0
      t.integer :menu_item_image_limit, default: 0
      t.integer :qr_code_limit, default: 0
      t.decimal :price, precision: 8, scale: 2
    end
  end
end
