class AddOptionalToChoice < ActiveRecord::Migration[6.0]
  def change
    add_column :choices, :optional, :boolean, default: false
  end
end
