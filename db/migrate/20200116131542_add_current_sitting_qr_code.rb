class AddCurrentSittingQrCode < ActiveRecord::Migration[6.0]
  def change
    add_column :qr_codes, :current_sitting_id, :integer
  end
end
