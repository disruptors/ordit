class CreateBusinesses < ActiveRecord::Migration[6.0]
  def change
    create_table :businesses do |t|
      t.string :name
      t.string :type
      t.string :location
      t.string  :phone
      t.string :email
      t.string :fb_link
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
