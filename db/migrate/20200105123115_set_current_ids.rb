class SetCurrentIds < ActiveRecord::Migration[6.0]
  def change
    User.all.each do |user|
      user.current_business_id = user.businesses.first&.id
      user.save
    end


    Business.all.each do |bizz|
      bizz.current_menu_id = bizz.menus.first&.id
      bizz.save
    end
  end
end
