class CreateChoices < ActiveRecord::Migration[6.0]
  def change
    create_table :choices do |t|
      t.references :menu_item, foreign_key: true
      t.string  :name,   null: false
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
