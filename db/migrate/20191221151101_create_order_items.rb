class CreateOrderItems < ActiveRecord::Migration[6.0]
  def change
    create_table :order_items do |t|
      t.references :order, foreign_key: true
      t.references :customer, foreign_key: true
      t.references :menu_item, foreign_key: true

      t.string :order_status
      t.money :amount, default: 0
    end
  end
end
