class AddDefaultMenu < ActiveRecord::Migration[6.0]
  def change
    Business.all.each do |business|
      if business.menus.empty?
        menu = business.menus.build
        menu.save!
      end
    end
  end
end
