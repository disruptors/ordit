class CreateChoiceOptions < ActiveRecord::Migration[6.0]
  def change
    create_table :choice_options do |t|
      t.references :choice, foreign_key: true
      t.string  :name, null: false
      t.string  :price
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
