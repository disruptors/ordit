class AddStatusToMenuItem < ActiveRecord::Migration[6.0]
  def change
    add_column :menu_items, :status, :string
  end
end
