class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.references :qr_code, foreign_key: true
      t.references :sitting, foreign_key: true
      t.references :business, foreign_key: true

      t.string :order_status
      t.money :total_amount, default: 0
      t.boolean :paid, default: false

      t.timestamps
    end
  end
end
