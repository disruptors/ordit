class Add < ActiveRecord::Migration[6.0]
  def change
    add_column :customers, :expired_at, :datetime, default: nil
  end
end
