class BusinessSerializer < AppSerializer
  attributes :name, :type, :location, :phone, :avatar, :user_id

  belongs_to :user
  has_many :tags

  attribute :current_menu_id do |business|
    "#{business.current_menu&.id}"
  end

  attribute :avatar do |business|
    url_helpers.rails_blob_url(business.avatar) if business.avatar.attached?
  end

  default_includes [:user, :tags]
end
