class CustomerSerializer < AppSerializer
  attributes :ip_address, :pin_number_used, :expired_at, :business_id, :sitting_id
  belongs_to :business
  belongs_to :qr_code
  has_many :orders

  attribute :qr_code_name do |customer|
    customer.qr_code.name
  end

  default_includes [:business, :qr_code]
end
