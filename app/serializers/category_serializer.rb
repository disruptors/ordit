class CategorySerializer < AppSerializer
  belongs_to :business
  attributes :id, :name, :business_id, :active

  attribute :total_menu_items do |category|
    MenuItem.where(category_id: category.id).count
  end
end
