class QrCodeSerializer < AppSerializer
  attributes :name, :pin_number

  belongs_to :business

  has_many :orders
  has_many :sittings
  has_many :customers

  default_includes [:business]

  attribute :svg do |qr_code|
    url_helpers.qr_code_url(qr_code, format: :svg)
  end

  attribute :table_name do |qr_code|
    qr_code.table_name
  end

  attribute :store_url do |qr_code|
    store_url = url_helpers.store_url(qr_code.business, table: qr_code.id)
    url_helpers.new_auth_url(callback_url: store_url, qr_code_id: qr_code.id, business_id: qr_code.business_id)
  end

  attribute :current_sitting_id do |qr_code|
    qr_code.sitting&.id
  end

  attribute :sitting_customers_count do |qr_code|
    if qr_code.sitting.nil?
      0
    else
      qr_code.sitting.customers.count
    end
  end
end
