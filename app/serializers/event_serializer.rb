class EventSerializer < AppSerializer
  attributes :id, :name, :resource_id, :resource_type, :created_at, :creator_type, :creator_id, :description
  set_type :event
end
