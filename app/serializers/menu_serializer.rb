class MenuSerializer < AppSerializer
  belongs_to :business
  has_many :menu_items

  default_includes [:business, :menu_items]
end
