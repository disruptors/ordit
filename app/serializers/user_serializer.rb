class UserSerializer < AppSerializer
  attributes :email, :name, :onboarding_completed, :role
  has_many :businesses

  attribute :current_business_id do |user|
    "#{user.current_business&.id}"
  end

  default_includes [:businesses]
end
