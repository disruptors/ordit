class TagSerializer < AppSerializer
  belongs_to :business
  attributes :id, :name, :business_id
end
