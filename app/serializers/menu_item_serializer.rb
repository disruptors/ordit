class MenuItemSerializer < AppSerializer
  attributes :name, :description, :price, :status

  belongs_to :menu
  belongs_to :business
  belongs_to :category

  attribute :current_image do |menu_item|
    url_helpers.rails_blob_url(menu_item.current_image) if menu_item.images.attached?
  end

  default_includes [:menu, :category]
end
