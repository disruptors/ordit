class ChoiceOptionSerializer < AppSerializer
  attributes :name, :price, :active

  belongs_to :choice
end
