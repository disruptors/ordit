class OrderSerializer < AppSerializer
  belongs_to :qr_code
  belongs_to :customer
  belongs_to :sitting
  belongs_to :menu_item

  attributes :name, :amount, :order_status, :quantity

  attribute :table do |order|
    order.qr_code.name
  end

  default_includes [:customer]
end
