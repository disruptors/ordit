class SittingSerializer < AppSerializer
  has_many :customers
  has_many :orders
  belongs_to :qr_code

  default_includes [:customers, :orders]

  attribute :name do |sitting|
    sitting.qr_code.name
  end

  attribute :total_order_amount do |sitting|
    sitting.total_order_amount
  end
end
