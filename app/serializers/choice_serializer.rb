class ChoiceSerializer < AppSerializer
  attributes :name, :active, :optional

  belongs_to :menu_item

  has_many :choice_options do |choice|
    choice.choice_options.where(active: true)
  end
end
