class QrCodes::ClearTableService < Upframework::BaseService
  def post_initialize(id:, **_options)
    @qr_code = QrCode.find(id)
    @sitting = @qr_code.sitting
  end

  def execute
    return if @sitting.nil?

    ActiveRecord::Base.transaction do
      # Deactivate sitting
      @sitting.update(active: false)

      # Sign out customers
      @sitting.customers.update_all(expired_at: DateTime.current)
    end
  end

  def result
    @qr_code
  end
end
