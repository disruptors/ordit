class QrCodes::GenerateImageService < Upframework::BaseService
  delegate :url_helpers, to: :'Rails.application.routes'

  def post_initialize(qr_code_id: nil, qr_code: nil, **_options)
    @qr_code = qr_code.is_a?(QrCode) ? qr_code : QrCode.find_by(id: qr_code_id)
    @business_id = @qr_code.business_id
  end

  def execute
    args = {
      offset:          0,
      color:           '000',
      shape_rendering: 'crispEdges',
      module_size:     6,
      standalone:      true,
    }

    @qr_code_image = RQRCode::QRCode.new(qr_code_url).as_svg(args)

    qr_code.svg = @qr_code_image
    qr_code.save
  end

  def result
    @qr_code
  end

  private

  def qr_code_url
    store_url = url_helpers.store_url(@business_id, table: @qr_code.id)
    url_helpers.new_auth_url(callback_url: store_url, qr_code_id: @qr_code.id, business_id: @business_id)
  end
end
