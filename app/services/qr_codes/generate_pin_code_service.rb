class QrCodes::GeneratePinCodeService < Upframework::BaseService
  def post_initialize(qr_code_id:, **options)
    @qr_code = QrCode.find(qr_code_id)
  end

  def execute
    pin = @qr_code.generate_four_digit_number
    @qr_code.pin_number = pin
    @qr_code.save
  end

  def result
    @qr_code
  end
end
