class QrCodes::BulkCreateQrCodeService < Upframework::BaseService
  attr_accessor :qr_code

  def post_initialize(business_id:, qr_code:, **_options)
    @business_id = business_id
    @qr_code     = qr_code
    @qr_codes    = []
  end

  def execute
    @business = Business.find(@business_id)
    qr_codes_args.each do |args|
      @business.qr_codes.build(args)
    end

    if @business.save
      update_qr_code_image
      @qr_codes = @business.qr_codes.entries
    else
      add_error(@business.errors.full_messages.join('. '))
    end
  end

  def result
    @qr_codes
  end

  private

  def qr_codes_args
    qr_code.map! do |qr_code|
      args = {
        pin_number:  rand.to_s[2..5],
        business_id: @business_id,
        active:      true,
        created_at:  Time.current,
        updated_at:  Time.current,
      }

      qr_code.merge!(args)
    end
  end

  def update_qr_code_image
    @qr_codes.each do |qr_code|
      qr_code.generate_qr_code_image
      qr_code.name ||= "Table #{Business.find(@business_id).qr_codes.count}"
      qr_code.save
    end
  end
end
