class SittingChannel < ApplicationCable::Channel
  def subscribed
    sitting = Sitting.find(params[:sitting_id])
    stream_for sitting
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
