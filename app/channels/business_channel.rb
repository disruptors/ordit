class BusinessChannel < ApplicationCable::Channel
  def subscribed
    business = Business.find(params[:business_id])
    stream_for business
  end
end
