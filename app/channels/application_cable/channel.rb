module ApplicationCable
  class Channel < ActionCable::Channel::Base
    include ActiveSupport::Callbacks
    before_subscribe :transofrm_params

    def transofrm_params
      params.deep_transform_keys!(&:underscore)
    end
  end
end
