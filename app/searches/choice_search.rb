class ChoiceSearch < Upframework::BaseSearch
  def post_initialize(**_options)
    @active      = true
    @model_scope = ::Choice.accessible_by(@current_ability)
  end

  def execute
    query(:menu_item_id) { query_menu_item_id }
    query(:active)       { query_active }
  end

  private

  def query_menu_item_id
    @model_scope.where(menu_item_id: @menu_item_id)
  end

  def query_active
    @model_scope.where(active: @active)
  end
end
