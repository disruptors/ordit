class CategorySearch < Upframework::BaseSearch
  def post_initialize(**_options)
    @model_scope = ::Category.accessible_by(@current_ability)
  end

  def execute
    query(:name)        { query_name }
    query(:active)      { query_active }
    query(:business_id) { query_business_id }
  end

  private

  def query_business_id
    @model_scope.where(business_id: @business_id)
  end

  def query_active
    @model_scope.where(active: @active.downcase == 'active')
  end

  def query_name
    @model_scope.where('name ILIKE :name', name: "%#{@name}%")
  end
end
