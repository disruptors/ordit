class MenuItemSearch < Upframework::BaseSearch
  def post_initialize(**_options)
    @model_scope = MenuItem.accessible_by(@current_ability)
  end

  def execute
    query(:name)        { query_name }
    query(:business_id) { query_business_id }
    query(:category_id) { query_category_id }

    paginate_scope
  end

  private

  def query_category_id
    @model_scope.where(category_id: @category_id)
  end

  def query_name
    @model_scope.where('name ILIKE :name', name: "%#{@name}%")
  end

  def query_business_id
    @model_scope.where(business_id: @business_id)
  end
end
