class UserSearch < Upframework::BaseSearch
  def post_initialize(**_options)
    @per_page    = 15
    @model_scope = User.accessible_by(@current_ability)
  end

  def execute
    query(:email) { query_email }
    query(:role)  { query_role }

    paginate_scope
  end

  private

  def query_email
    @model_scope.where('email ILIKE :email', email: "%#{@email}%")
  end

  def query_role
    @model_scope.where(role: @role)
  end
end
