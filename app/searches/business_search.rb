class BusinessSearch < Upframework::BaseSearch
  def post_initialize(**_options)
    @per_page    = 15
    @model_scope = ::Business.accessible_by(@current_ability)
  end

  def execute
    query(:name)    { query_name }
    query(:user_id) { query_user_id }

    paginate_scope
  end

  private

  def query_user_id
    @model_scope.where(user_id: @user_id)
  end

  def query_name
    @model_scope.where('name ILIKE :name', name: "%#{@name}%")
  end
end
