import {
  SESSION_CLEAR,
  SESSION_SET_CURRENT_BUSINESS_ID,
  SESSION_SET_CURRENT_CUSTOMER,
  SESSION_SET_CURRENT_USER,
} from '@/store/session/types';
import { createReducer, PayloadAction } from '@reduxjs/toolkit';

interface State {
  validating: boolean;
  currentUserId: string | undefined;
  currentBusinessId: string | undefined;
  currentCustomerId: string | undefined;
}

const initialState: State = {
  validating: true,
  currentUserId: undefined,
  currentBusinessId: undefined,
  currentCustomerId: undefined,
};

const setSession = (
  state: State,
  {
    currentUserId = undefined,
    currentBusinessId = undefined,
    currentCustomerId = undefined,
  }: {
    currentUserId?: string;
    currentBusinessId?: string;
    currentCustomerId?: string;
  }
): State => {
  return {
    ...state,
    currentUserId,
    currentBusinessId,
    currentCustomerId,
    validating: false,
  };
};

export default createReducer(initialState, {
  [SESSION_SET_CURRENT_USER]: (
    state,
    action: PayloadAction<{ currentCustomerId: string; currentBusinessId: string }>
  ) => setSession(state, action.payload),
  [SESSION_SET_CURRENT_CUSTOMER]: (
    state,
    action: PayloadAction<{ currentCustomerId: string }>
  ) => setSession(state, action.payload),
  [SESSION_SET_CURRENT_BUSINESS_ID]: (state, action: PayloadAction<string>) => {
    state.currentBusinessId = action.payload;
  },
  [SESSION_CLEAR]: () => ({ ...initialState, validating: false }),
});
