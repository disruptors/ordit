import { mergeEntities } from '@/store/entities/actions';
import { SUBSCRIBE_BUSINESS_CHANNEL } from '@/store/entities/business/types';
import { ActionType } from '@/store/entities/helpers';
import { SUBSCRIBE_SITTING_CHANNEL } from '@/store/entities/sitting/types';
import { sessionActions } from '@/store/session';
import ApiClient from '@/utils/ApiClient';
import { TokenNotFoundError } from '@/utils/errors/TokenNotFoundError';
import SagaEvents from '@/utils/SagaEvents';
import UserAuth from '@/utils/UserAuth';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import * as api from '@/api';
import {
  LOGIN,
  REGISTER,
  REGISTER_CUSTOMER,
  SESSION_CLEAR,
  SESSION_VALIDATE,
  SESSION_VALIDATE_CUSTOMER,
  SESSIONS_SERVER_SIGNOUT,
  SESSION_BECOME,
} from './types';

function* clearSession() {
  yield UserAuth.clearData();
}

function* serverSignout() {
  const { token, uid, client } = UserAuth.getData();

  if (token && uid && client) {
    yield call(ApiClient.destroy, '/api/auth/sign_out');
  }

  yield put(sessionActions.clear());
}

function* become(action: ActionType) {
  const { id, successCallback } = action.payload;
  const response = yield call(api.sessions.become, id);
  yield call(setupSession, response);

  if (successCallback) {
    successCallback();
  }
}

function* login(action) {
  const response = yield call(ApiClient.post, '/api/auth/sign_in', action.payload);
  yield call(setupSession, response);
}

function* register(action) {
  const response = yield call(ApiClient.post, '/api/auth', action.payload);
  yield call(setupSession, response);
}

function* setupSession(response) {
  const token = response.headers.get('access-token') || '';
  const uid = response.headers.get('uid') || '';
  const client = response.headers.get('client') || '';
  const { data: user } = response;

  UserAuth.setData({ token, uid, client });
  yield all([
    put(mergeEntities(response)),
    put(
      sessionActions.initSession({
        currentUserId: user.id,
        currentBusinessId: user.attributes.currentBusinessId,
      })
    ),
  ]);

  yield put({
    type: SUBSCRIBE_BUSINESS_CHANNEL,
    payload: { businessId: user.attributes.currentBusinessId },
  });
}

function* registerCustomer(action: ActionType) {
  const { successCallback } = action.payload;
  const response = yield call(ApiClient.post, '/api/customer_auth', action.payload);
  const token = response.headers.get('access-token') || '';
  const uid = response.headers.get('uid') || '';
  const client = response.headers.get('client') || '';
  const { data: customer } = response;

  UserAuth.setPrefix('customer');
  UserAuth.setData({ token, uid, client });
  yield all([
    put(mergeEntities(response)),
    put(
      sessionActions.initCustomerSession({
        currentCustomerId: customer.id,
        currentBusinessId: customer.attributes.businessId,
      })
    ),
  ]);
  if (successCallback) {
    successCallback();
  }
}

function* validateSession() {
  UserAuth.setPrefix('');
  const { token, uid, client } = UserAuth.getData();

  if (!token || !uid || !client) {
    throw new TokenNotFoundError();
  }

  const res = yield ApiClient.get('/api/users/profile');
  const { data: user } = res;
  yield all([
    put(mergeEntities(res)),
    put(
      sessionActions.initSession({
        currentUserId: user.id,
        currentBusinessId: user.attributes.currentBusinessId,
      })
    ),
  ]);
  yield put({
    type: SUBSCRIBE_BUSINESS_CHANNEL,
    payload: { businessId: user.attributes.currentBusinessId },
  });
}

function* validateCustomerSession() {
  UserAuth.setPrefix('customer');
  const { token, uid, client } = UserAuth.getData();

  if (!token || !uid || !client) {
    throw new TokenNotFoundError();
  }

  const res = yield ApiClient.get('/api/customers/profile');
  const { data: customer } = res;
  yield all([
    put(mergeEntities(res)),
    put(
      sessionActions.initCustomerSession({
        currentCustomerId: customer.id,
        currentBusinessId: customer.attributes.businessId,
      })
    ),
  ]);
  yield put({
    type: SUBSCRIBE_SITTING_CHANNEL,
    payload: { sittingId: customer.attributes.sittingId },
  });
}

function* sessionSaga() {
  yield takeLatest(LOGIN, SagaEvents.requestCycle, login);
  yield takeLatest(REGISTER, SagaEvents.requestCycle, register);
  yield takeLatest(REGISTER_CUSTOMER, SagaEvents.requestCycle, registerCustomer);
  yield takeLatest(SESSION_VALIDATE, SagaEvents.requestCycle, validateSession);
  yield takeLatest(
    SESSION_VALIDATE_CUSTOMER,
    SagaEvents.requestCycle,
    validateCustomerSession
  );
  yield takeLatest(SESSION_CLEAR, SagaEvents.requestCycle, clearSession);
  yield takeLatest(SESSIONS_SERVER_SIGNOUT, SagaEvents.requestCycle, serverSignout);
  yield takeLatest(SESSION_BECOME, SagaEvents.requestCycle, become);
}

export default sessionSaga;
