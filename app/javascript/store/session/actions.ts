import { createAction } from '@reduxjs/toolkit';
import {
  LOGIN,
  REGISTER,
  REGISTER_CUSTOMER,
  SESSIONS_SERVER_SIGNOUT,
  SESSION_CLEAR,
  SESSION_SET_CURRENT_BUSINESS_ID,
  SESSION_SET_CURRENT_CUSTOMER,
  SESSION_SET_CURRENT_USER,
  SESSION_VALIDATE,
  SESSION_VALIDATE_CUSTOMER,
  SESSION_BECOME,
} from './types';

export const login = createAction<{ email: string; password: string }>(LOGIN);

export const register = createAction<{ email: string; password: string }>(REGISTER);

export const registerCustomer = createAction<{
  pinNumberUsed: string;
  businessId: string;
  qrCodeId: string;
}>(REGISTER_CUSTOMER);

export const setCurrentBusinessId = createAction<string>(SESSION_SET_CURRENT_BUSINESS_ID);

export const validate = createAction(SESSION_VALIDATE);

export const validateCustomer = createAction(SESSION_VALIDATE_CUSTOMER);

export const initSession = createAction<{
  currentUserId: string;
  currentBusinessId: string;
}>(SESSION_SET_CURRENT_USER);

export const initCustomerSession = createAction<{
  currentCustomerId: string;
  currentBusinessId: string;
}>(SESSION_SET_CURRENT_CUSTOMER);

export const clear = createAction(SESSION_CLEAR);

export const serverSignout = createAction(SESSIONS_SERVER_SIGNOUT);

export const become = createAction<{ id: string }>(SESSION_BECOME);
