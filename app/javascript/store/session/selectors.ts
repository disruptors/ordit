import { StoreState } from '@/store';
import { getUser } from '@/store/entities/user/selectors';
import { getCustomer } from '@/store/entities/customer/selectors';

export const getIsValidating = (state: StoreState) => state.sessions.validating;

export const getIsLoggedIn = (state: StoreState) => !!state.sessions.currentUserId;

export const getCurrentUserId = (state: StoreState) => state.sessions.currentUserId;

export const getCurrentUser = (state: StoreState) =>
  state.sessions.currentUserId ? getUser(state.sessions.currentUserId)(state) : null;

export const getCurrentCustomerId = (state: StoreState) =>
  state.sessions.currentCustomerId;

export const getCurrentCustomer = (state: StoreState) =>
  state.sessions.currentCustomerId
    ? getCustomer(state.sessions.currentCustomerId)(state)
    : null;

export const getCurrentBusinessId = (state: StoreState) =>
  state.sessions.currentBusinessId;
