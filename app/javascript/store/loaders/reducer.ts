import { createReducer, PayloadAction } from '@reduxjs/toolkit';

type State = {
  [key: string]: boolean;
};

// Would result to this structure
// loaders: {
//    FETCH_USERS: false
//    FETCH_USER: false
//    FETCH_MENU_ITEM: true
//    etc..
// }
const initialState: State = {};

export default createReducer(initialState, {
  REQUEST_TRIGGER: (state, action: PayloadAction<string>) => {
    state[action.payload] = true;
  },
  REQUEST_DONE: (state, action: PayloadAction<string>) => {
    state[action.payload] = false;
  },
});
