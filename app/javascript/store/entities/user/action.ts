import { User } from '@/schemas';
import { createAction } from '@reduxjs/toolkit';
import { CREATE_USER, FETCH_USER, FETCH_USERS, UPDATE_USER } from './types';

export const createUser = createAction<Partial<User>>(CREATE_USER);

export const updateUser = createAction(
  UPDATE_USER,
  (id: string, user: Partial<User>) => ({
    payload: { user, id },
  })
);

export const fetchUser = createAction<string>(FETCH_USER);

export const fetchUsers = createAction(FETCH_USERS);
