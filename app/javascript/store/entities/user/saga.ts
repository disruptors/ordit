import * as api from '@/api';
import history from '@/business/history';
import { User } from '@/schemas';
import { mergeEntities } from '@/store/entities/actions';
import { ActionType } from '@/store/entities/helpers';
import {
  CREATE_USER,
  FETCH_USER,
  FETCH_USERS,
  UPDATE_USER,
} from '@/store/entities/user/types';
import SagaEvents from '@/utils/SagaEvents';
import { notification } from 'antd';
import { call, put, takeLatest } from 'redux-saga/effects';

function* fetchUser(action: ActionType): any {
  const data = yield call(api.users.getById, action.payload);
  yield put(mergeEntities(data));
}

function* fetchUsers() {
  const res = yield call(api.users.getAll);
  yield put(mergeEntities(res));
}

function* createUser(action: ActionType<Partial<User>>) {
  const res = yield call(api.users.create, action.payload);
  yield put(mergeEntities(res));

  notification['success']({ message: 'Create Successful!' });
  history.push('/users');
}

function* updateUser(action: ActionType<{ id: string; user: Partial<User> }>) {
  const res = yield call(api.users.update, action.payload.id, action.payload.user);
  yield put(mergeEntities(res));

  notification['success']({ message: 'Update Successful!' });
}

function* userSaga() {
  yield takeLatest(FETCH_USERS, SagaEvents.requestCycle, fetchUsers);
  yield takeLatest(FETCH_USER, SagaEvents.requestCycle, fetchUser);
  yield takeLatest(UPDATE_USER, SagaEvents.requestCycle, updateUser);
  yield takeLatest(CREATE_USER, SagaEvents.requestCycle, createUser);
}

export default userSaga;
