import { StoreState } from '@/store/index';

const getIds = (state: StoreState) => state.entities.users.all;

export const getUser = (id: string) => (state: StoreState) =>
  state.entities.users.byId[id];

export const getUsersByIds = (ids: string[]) => (state: StoreState) =>
  ids.map(id => getUser(id)(state)).filter(user => user);

export const getUsers = (state: StoreState) => getUsersByIds(getIds(state))(state);
