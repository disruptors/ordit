import { MERGE_ENTITIES } from '@/store/entities/types';
import { JsonApiObject, ResourceData } from 'json-api-normalizer';

export const mergeEntities = <R extends ResourceData | ResourceData[]>(
  data: JsonApiObject<R>
) => ({
  type: MERGE_ENTITIES,
  payload: data,
});
