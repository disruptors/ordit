export const FETCH_QR_CODES = 'FETCH_QR_CODES';
export const FETCH_QR_CODE = 'FETCH_QR_CODE';
export const ADD_QR_CODE = 'ADD_QR_CODE';
export const SUBMIT_QR_CODE = 'SUBMIT_QR_CODE';
export const REMOVE_QR_CODE = 'REMOVE_QR_CODE';
export const CALL_QR_CODE_SERVICE = 'CALL_QR_CODE_SERVICE';
