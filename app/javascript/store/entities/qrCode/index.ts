import * as qrCodeAction from './action';
import * as qrCodeTypes from './types';
import qrCodeSaga from './saga';
import qrCodeReducer from './reducer';

export { qrCodeSaga, qrCodeAction, qrCodeTypes };

export default qrCodeReducer;
