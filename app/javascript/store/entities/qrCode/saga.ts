import history from '@/business/history';
import { mergeEntities } from '@/store/entities/actions';
import { ActionType } from '@/store/entities/helpers';
import {
  FETCH_QR_CODES,
  REMOVE_QR_CODE,
  SUBMIT_QR_CODE,
  CALL_QR_CODE_SERVICE,
  FETCH_QR_CODE,
} from '@/store/entities/qrCode/types';
import ApiClient from '@/utils/ApiClient';
import SagaEvents from '@/utils/SagaEvents';
import { call, put, takeLatest } from 'redux-saga/effects';
import * as api from '@/api';

function* fetchQrCode(action: ActionType) {
  const { id } = action.payload;
  const data = yield call(api.qrCodes.getById, id);

  yield put(mergeEntities(data));
}

function* fetchQrCodes(action: ActionType) {
  const data = yield call(ApiClient.get, 'api/qr_codes', action.payload.query);
  yield put(mergeEntities(data));
}

function* submitQrCode(action: ActionType) {
  const { payload } = action;

  if (payload.id) {
    yield call(updateQrCode, payload);
  } else if (Array.isArray(payload.qrCode)) {
    yield call(bulkCreateQrCode, payload);
  } else {
    yield call(createQrCode, payload);
  }
}

function* bulkCreateQrCode(payload: [{ name: string }]) {
  yield call([api.qrCodes, 'callService'], 'bulk_create_qr_code', payload);

  history.push('/qr_codes');
}

function* createQrCode(payload: { businessId: string }) {
  const response = yield call(ApiClient.post, '/api/qr_codes', payload, {
    showAlerts: true,
  });
  yield put(mergeEntities(response));
  history.push('/qr_codes');
}

function* updateQrCode(payload: { id: string }) {
  const response = yield call(ApiClient.patch, `/api/qr_codes/${payload.id}`, payload, {
    showAlerts: true,
  });

  yield put(mergeEntities(response));
}

function* removeQrCode(action: ActionType<{ id: string }>) {
  yield call(ApiClient.destroy, `/api/qr_codes/${action.payload.id}`, {
    showAlerts: true,
  });
}

function* callService(action: ActionType<{ name: string; params: any }>) {
  const response = yield call(
    [api.qrCodes, 'callService'],
    action.payload.name,
    action.payload.params
  );

  yield put(mergeEntities(response));
}

function* qrCodeSaga() {
  yield takeLatest(FETCH_QR_CODES, SagaEvents.requestCycle, fetchQrCodes);
  yield takeLatest(SUBMIT_QR_CODE, SagaEvents.requestCycle, submitQrCode);
  yield takeLatest(REMOVE_QR_CODE, SagaEvents.requestCycle, removeQrCode);
  yield takeLatest(CALL_QR_CODE_SERVICE, SagaEvents.requestCycle, callService);
  yield takeLatest(FETCH_QR_CODE, SagaEvents.requestCycle, fetchQrCode);
}

export default qrCodeSaga;
