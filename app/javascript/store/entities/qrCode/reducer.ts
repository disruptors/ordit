import { QrCode } from '@/schemas/QrCode';
import {
  addEntity,
  createInitialEntityState,
  removeEntity,
} from '@/store/entities/helpers';
import { ADD_QR_CODE, REMOVE_QR_CODE } from '@/store/entities/qrCode/types';
import { createReducer } from '@reduxjs/toolkit';

const initialState = createInitialEntityState<QrCode>();

export default createReducer(initialState, {
  [ADD_QR_CODE]: (state, action) => addEntity(state, action.payload),
  [REMOVE_QR_CODE]: (state, action) => removeEntity(state, action.payload.id),
});
