import { StoreState } from '@/store/index';

const getIds = (state: StoreState) => state.entities.qrCodes.all;

export const getQrCode = (id: string) => (state: StoreState) =>
  state.entities.qrCodes.byId[id];

export const getQrCodesByIds = (ids: string[]) => (state: StoreState) =>
  ids.map(id => getQrCode(id)(state)).filter(item => item);

export const getQrCodes = (state: StoreState) => getQrCodesByIds(getIds(state))(state);
