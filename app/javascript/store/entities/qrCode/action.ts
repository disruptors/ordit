import {
  CALL_QR_CODE_SERVICE,
  FETCH_QR_CODES,
  REMOVE_QR_CODE,
  SUBMIT_QR_CODE,
  FETCH_QR_CODE,
} from '@/store/entities/qrCode/types';
import { createAction } from '@reduxjs/toolkit';

export const fetchQrCodes = (query: any = {}) => ({
  type: FETCH_QR_CODES,
  payload: { query },
});

export const submitQrCode = createAction<any>(SUBMIT_QR_CODE);
export const removeQrCode = createAction(REMOVE_QR_CODE, (id: string) => ({
  payload: { id },
}));
export const callService = createAction(
  CALL_QR_CODE_SERVICE,
  (name: string, data: any) => ({
    payload: { name, params: data },
  })
);

export const fetchQrCode = createAction(FETCH_QR_CODE, (id: string) => ({
  payload: { id },
}));
