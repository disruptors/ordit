import { FETCH_CHOICES, SUBMIT_CHOICE } from './types';
import { createAction } from '@reduxjs/toolkit';
import { Choices } from '@/schemas';

export const fetchChoices = createAction(FETCH_CHOICES, (query: any = {}) => ({
  payload: { query },
}));

export const submitChoice = createAction<Partial<Choices>>(SUBMIT_CHOICE);
