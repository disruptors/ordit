import { mergeEntities } from '@/store/entities/actions';
import { FETCH_CHOICES, SUBMIT_CHOICE } from '@/store/entities/choice/types';
import ApiClient from '@/utils/ApiClient';
import SagaEvents from '@/utils/SagaEvents';
import { call, put, takeLatest } from 'redux-saga/effects';
import { ActionType } from '@/store/entities/helpers';

function* fetchChoices(action: ActionType) {
  const data = yield call(ApiClient.get, '/api/choices', action.payload.query);
  yield put(mergeEntities(data));
}

function* submitChoice(action: ActionType) {
  if (action.payload.id) {
    yield call(updateChoice, action.payload);
  } else {
    yield call(createChoice, action.payload);
  }
}

function* updateChoice(payload: any) {
  const response = yield call(ApiClient.patch, `/api/choices/${payload.id}`, payload);
  yield put(mergeEntities(response));
}

function* createChoice(payload: any) {
  const response = yield call(ApiClient.post, '/api/choices', payload);
  yield put(mergeEntities(response));
}

function* choiceSaga() {
  yield takeLatest(FETCH_CHOICES, SagaEvents.requestCycle, fetchChoices);
  yield takeLatest(SUBMIT_CHOICE, SagaEvents.requestCycle, submitChoice);
}

export default choiceSaga;
