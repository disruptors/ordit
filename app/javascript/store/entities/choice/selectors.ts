import { StoreState } from '@/store/index';
import { ChoiceOptions, Choices } from '@/schemas';
import { getChoiceOptionsPerChoiceId } from '../choiceOption/selectors';

const filterChoices = (menuItemId: string | undefined) => (state: StoreState) => {
  const choices = state.entities.choices.all;

  return choices.filter((choiceId: string) => {
    const choice: Choices = getChoice(choiceId)(state);
    return choice.menuItem === menuItemId && choice.active;
  });
};

export const getChoice = (choiceId: string) => (state: StoreState) =>
  state.entities.choices.byId[choiceId];

export const getChoices = (menuItemId: string) => (state: StoreState) =>
  filterChoices(menuItemId)(state).map((choiceId: string) => getChoice(choiceId)(state));

export const getChoicesIdsFormat = (menuItemId: string | undefined) => (
  state: StoreState
): { [key: string]: Array<string> } => {
  let choiceOptionKeys: { [key: string]: Array<string> } = {};

  filterChoices(menuItemId)(state).forEach((choiceId: string) => {
    const optionIds = getChoiceOptionsPerChoiceId(choiceId)(state).map(
      (choiceOption: ChoiceOptions) => choiceOption.id
    );

    choiceOptionKeys = { ...choiceOptionKeys, [choiceId]: optionIds };
  });

  return choiceOptionKeys;
};
