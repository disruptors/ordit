import { Choices } from '@/schemas';
import { ADD_CHOICE } from '@/store/entities/choice/types';
import { addEntity, createInitialEntityState } from '@/store/entities/helpers';
import { createReducer } from '@reduxjs/toolkit';

const initialState = createInitialEntityState<Choices>();

export default createReducer(initialState, {
  [ADD_CHOICE]: (state, action) => addEntity(state, action.payload),
});
