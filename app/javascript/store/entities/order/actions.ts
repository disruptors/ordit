import { createAction } from '@reduxjs/toolkit';
import { ADD_ORDER, FETCH_ORDERS, SUBMIT_ORDER } from './types';

export const fetchOrders = createAction(FETCH_ORDERS, (query: any = {}) => ({
  payload: { query },
}));

export const addOrder = createAction<any>(ADD_ORDER);

export const submitOrder = createAction<any>(SUBMIT_ORDER);
