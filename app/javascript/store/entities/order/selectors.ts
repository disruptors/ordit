import { StoreState } from '@/store/index';

const getIds = (state: StoreState) => state.entities.orders.all;

export const getOrder = (id: string) => (state: StoreState) =>
  state.entities.orders.byId[id];

export const getOrdersByIds = (ids: string[]) => (state: StoreState) =>
  ids.map(id => getOrder(id)(state)).filter(order => order);

export const getOrders = (state: StoreState) => getOrdersByIds(getIds(state))(state);
