import { Order } from '@/schemas/Order';
import { addEntity, createInitialEntityState } from '@/store/entities/helpers';
import { ADD_ORDER } from '@/store/entities/order/types';
import { createReducer } from '@reduxjs/toolkit';

const initialState = createInitialEntityState<Order>();

export default createReducer(initialState, {
  [ADD_ORDER]: (state, action) => addEntity(state, action.payload),
});
