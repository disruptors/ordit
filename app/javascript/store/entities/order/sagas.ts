import * as api from '@/api';
import { call, put, takeLatest } from 'redux-saga/effects';
import SagaEvents from '@/utils/SagaEvents';
import { FETCH_ORDERS, SUBMIT_ORDER } from '@/store/entities/order/types';
import { ActionType } from '@/store/entities/helpers';
import { mergeEntities } from '@/store/entities/actions';
import ApiClient from '@/utils/ApiClient';

function* fetchOrders(action: ActionType) {
  const data = yield call(api.orders.getAll, action.payload.query);
  yield put(mergeEntities(data));
}

function* submitOrder(action: ActionType) {
  if (action.payload.id) {
    yield call(updateOrder, action.payload);
  } else {
    yield call(createOrder, action.payload);
  }
}

function* updateOrder(payload: any) {
  const response = yield call(ApiClient.patch, `/api/orders/${payload.id}`, payload, {
    showAlerts: true,
  });

  yield put(mergeEntities(response));
}

function* createOrder(payload: any) {
  const response = yield call(ApiClient.post, `/api/orders`, payload, {
    showAlerts: true,
  });

  yield put(mergeEntities(response));
}

function* orderSaga() {
  yield takeLatest(FETCH_ORDERS, SagaEvents.requestCycle, fetchOrders);
  yield takeLatest(SUBMIT_ORDER, SagaEvents.requestCycle, submitOrder);
}

export default orderSaga;
