import { combineReducers } from 'redux';
import users from '@/store/entities/user';
import business from '@/store/entities/business';
import qrCodes from '@/store/entities/qrCode';
import menuItems from '@/store/entities/menuItem';
import customers from '@/store/entities/customer';
import orders from '@/store/entities/order';
import sittings from '@/store/entities/sitting';
import category from '@/store/entities/category';
import tag from '@/store/entities/tag';
import events from '@/store/entities/event';
import choiceOptions from '@/store/entities/choiceOption';
import choices from '@/store/entities/choice';

export default combineReducers({
  users,
  qrCodes,
  business,
  menuItems,
  customers,
  orders,
  sittings,
  category, // TODO: pluralize
  tag, // TODO: pluralize
  events,
  choiceOptions,
  choices,
});
