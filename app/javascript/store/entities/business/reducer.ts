import { Business } from '@/schemas';
import { ADD_BUSINESS } from '@/store/entities/business/types';
import { addEntity, createInitialEntityState } from '@/store/entities/helpers';
import { createReducer } from '@reduxjs/toolkit';

const initialState = createInitialEntityState<Business>();

export default createReducer(initialState, {
  [ADD_BUSINESS]: (state, action) => addEntity(state, action.payload),
});
