import * as api from '@/api';
import history from '@/business/history';
import { Business } from '@/schemas';
import { mergeEntities } from '@/store/entities/actions';
import {
  FETCH_BUSINESS,
  SUBMIT_BUSINESS,
  SUBSCRIBE_BUSINESS_CHANNEL,
} from '@/store/entities/business/types';
import { ActionType } from '@/store/entities/helpers';
import { sessionActions } from '@/store/session';
import SagaEvents from '@/utils/SagaEvents';
import { call, put, takeLatest, take } from 'redux-saga/effects';
import { notification } from 'antd';
import Realtime from '@/utils/Realtime';

function* submitBusiness(action: ActionType) {
  if (action.payload.data.id) {
    yield call(updateBusiness, action.payload.data);
  } else {
    yield call(createBusiness, action.payload);
  }
}

function* updateBusiness(payload: any) {
  const response = yield call(api.business.update, payload.id, payload);
  notification['success']({ message: 'Update Successful!' });
  yield put(mergeEntities(response));
}

function* createBusiness(payload: {
  data: Partial<Business>;
  redirectTo?: string;
  onboarding?: boolean;
}) {
  const response = yield call(api.business.create, payload.data);
  yield put(mergeEntities(response));

  if (payload.onboarding) {
    yield put(sessionActions.setCurrentBusinessId(response.data.id));
  }
  history.push(payload.redirectTo || '/businesses');
}

function* fetchBusiness(action: ActionType) {
  const res = yield call(api.business.getById, action.payload);
  yield put(mergeEntities(res));
}

function* subscribeChannel(action: ActionType) {
  const channel = yield call(Realtime.createChannel, 'BusinessChannel', action.payload);

  while (true) {
    try {
      // Recieving data from sockets
      const payload = yield take(channel);
      yield put(mergeEntities(payload));
      Realtime.notifyEvent(payload);
    } catch (err) {
      console.error(err);
      channel.close();
      throw err;
    }
  }
}

function* businessSaga() {
  yield takeLatest(SUBMIT_BUSINESS, SagaEvents.requestCycle, submitBusiness);
  yield takeLatest(FETCH_BUSINESS, SagaEvents.requestCycle, fetchBusiness);
  yield takeLatest(SUBSCRIBE_BUSINESS_CHANNEL, subscribeChannel);
}

export default businessSaga;
