import { Business } from '@/schemas';
import { FETCH_BUSINESS, SUBMIT_BUSINESS } from '@/store/entities/business/types';
import { createAction } from '@reduxjs/toolkit';

interface SubmitBusinessOptions {
  redirectTo?: string;
  onboarding?: boolean;
}
export const submitBusiness = createAction(
  SUBMIT_BUSINESS,
  (business: Partial<Business>, options?: SubmitBusinessOptions) => ({
    payload: {
      data: business,
      redirectTo: options?.redirectTo,
      onboarding: options?.onboarding,
    },
  })
);

export const fetchBusiness = createAction<string>(FETCH_BUSINESS);
