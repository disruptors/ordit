import { StoreState } from '@/store';

export const getBusiness = (id: string) => (state: StoreState) =>
  state.entities.business.byId[id];

export const getBusinessesByIds = (ids: string[]) => (state: StoreState) =>
  ids.map(id => getBusiness(id)(state)).filter(business => business);
