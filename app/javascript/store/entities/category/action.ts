import { createAction } from '@reduxjs/toolkit';
import { FETCH_CATEGORIES, SUBMIT_CATEGORY } from './types';

export const submitCategory = createAction(SUBMIT_CATEGORY, (data: any) => ({
  payload: { category: data },
}));

export const fetchCategories = createAction(FETCH_CATEGORIES, (query: any = {}) => ({
  payload: { category: query },
}));
