import { StoreState } from '@/store';
import { trimDowncase } from '@/utils/Text';

export const getCategory = (id: string) => (state: StoreState) =>
  state.entities.category.byId[id];

export const getCategoryPerBusiness = (businessId: string | null | undefined) => (
  state: StoreState
) =>
  state.entities.category.all
    .map(id => getCategory(id)(state))
    .filter(category => category.businessId == businessId);

export const selectedCategories = (list: Array<string>, name: string, active: string) => (
  state: StoreState
) =>
  list.filter((id: string) => {
    const category = getCategory(id)(state);
    const activeCategory = active == 'active';

    const compareName =
      name && trimDowncase(name)
        ? trimDowncase(category.name).includes(trimDowncase(name))
        : true;

    const compareActive =
      active && trimDowncase(active) ? activeCategory == category.active : true;

    return compareName && compareActive;
  });
