import ApiClient from '@/utils/ApiClient';
import SagaEvents from '@/utils/SagaEvents';
import { call, put, takeLatest } from 'redux-saga/effects';
import { SUBMIT_CATEGORY, FETCH_CATEGORIES } from './types';
import { ActionType } from '@/store/entities/helpers';
import { mergeEntities } from '@/store/entities/actions';

function* submitCategory(action: ActionType) {
  if (action.payload?.category?.id) {
    yield call(updateCategory, action.payload.category);
  } else {
    yield call(createCategory, action.payload);
  }
}

function* createCategory(payload: any) {
  const response = yield call(ApiClient.post, '/api/categories', payload, {
    showAlerts: true,
  });

  yield put(mergeEntities(response));
}

function* updateCategory(payload: any) {
  const response = yield call(ApiClient.patch, `/api/categories/${payload.id}`, payload, {
    showAlerts: true,
  });

  yield put(mergeEntities(response));
}

function* fetchCategories(action: ActionType) {
  const data = yield call(ApiClient.get, '/api/categories', action.payload);
  yield put(mergeEntities(data));
}

function* categorySaga() {
  yield takeLatest(FETCH_CATEGORIES, SagaEvents.requestCycle, fetchCategories);
  yield takeLatest(SUBMIT_CATEGORY, SagaEvents.requestCycle, submitCategory);
}

export default categorySaga;
