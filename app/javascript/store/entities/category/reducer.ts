import { Category } from '@/schemas/Category';
import { addEntity, createInitialEntityState } from '@/store/entities/helpers';
import { createReducer } from '@reduxjs/toolkit';
import { ADD_CATEGORY } from './types';

const initialState = createInitialEntityState<Category>();

export default createReducer(initialState, {
  [ADD_CATEGORY]: (state, action) => addEntity(state, action.payload),
});
