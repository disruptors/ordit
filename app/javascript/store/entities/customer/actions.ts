import { FETCH_CUSTOMERS } from '@/store/entities/customer/types';

export const fetchCustomers = (query: any = {}) => ({
  type: FETCH_CUSTOMERS,
  payload: { query },
});
