import * as customerAction from './actions';
import * as customerTypes from './types';
import customerSaga from './sagas';
import customerReducer from './reducer';

export { customerSaga, customerAction, customerTypes };

export default customerReducer;
