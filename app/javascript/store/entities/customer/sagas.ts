import * as api from '@/api';
import { mergeEntities } from '@/store/entities/actions';
import { FETCH_CUSTOMERS } from '@/store/entities/customer/types';
import { ActionType } from '@/store/entities/helpers';
import SagaEvents from '@/utils/SagaEvents';
import { call, put, takeLatest } from 'redux-saga/effects';

function* fetchCustomers(action: ActionType) {
  const data = yield call(api.customers.getAll, action.payload.query);
  yield put(mergeEntities(data));
}

function* customerSaga() {
  yield takeLatest(FETCH_CUSTOMERS, SagaEvents.requestCycle, fetchCustomers);
}

export default customerSaga;
