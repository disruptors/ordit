import { Customer } from '@/schemas/Customer';
import { ADD_CUSTOMER } from '@/store/entities/customer/types';
import { addEntity, createInitialEntityState } from '@/store/entities/helpers';
import { createReducer } from '@reduxjs/toolkit';

const initialState = createInitialEntityState<Customer>();

export default createReducer(initialState, {
  [ADD_CUSTOMER]: (state, action) => addEntity(state, action.payload),
});
