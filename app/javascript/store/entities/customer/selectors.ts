import { StoreState } from '@/store/index';

const getIds = (state: StoreState) => state.entities.customers.all;

export const getCustomer = (id: string) => (state: StoreState) =>
  state.entities.customers.byId[id];

export const getActiveCustomers = (state: StoreState) => {
  const items = getIds(state)
    .filter(id => !getCustomer(id)(state).expiredAt)
    .map(id => getCustomer(id)(state));
  return items;
};
