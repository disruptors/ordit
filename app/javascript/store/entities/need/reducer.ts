import { Need } from '@/schemas';
import { addEntity, createInitialEntityState } from '@/store/entities/helpers';
import { ADD_USER } from '@/store/entities/user/types';
import { createReducer } from '@reduxjs/toolkit';

const initialState = createInitialEntityState<Omit<Need, 'password'>>();

export default createReducer(initialState, {
  // TODO: Maybe rename this to `MERGE_USERS` or `UPDATE_USERS` or `SET_USERS`, since
  // `addEntity` is performing a merge.
  [ADD_USER]: (state, action) => addEntity(state, action.payload),
});
