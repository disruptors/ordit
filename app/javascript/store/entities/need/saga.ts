import { CREATE_NEED } from '@/store/entities/need/types';
import { takeLatest, call } from 'redux-saga/effects';
import SagaEvents from '@/utils/SagaEvents';
import { Need } from '@/schemas/Need';
import { ActionType } from '@/store/entities/helpers';
import * as api from '@/api';

function* createNeed(action: ActionType<{ need: Need }>) {
  const { need, successCallback } = action.payload;
  yield call(api.needs.create, need);

  if (successCallback) {
    successCallback();
  }
}

function* needSaga() {
  yield takeLatest(CREATE_NEED, SagaEvents.requestCycle, createNeed);
}

export default needSaga;
