import { CREATE_NEED } from './types';
import { createAction } from '@reduxjs/toolkit';
import { Need } from '@/schemas';

export const createNeed = createAction<{ need: Partial<Need> }>(CREATE_NEED);
