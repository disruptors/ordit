import { createReducer } from '@reduxjs/toolkit';
import { addEntity, createInitialEntityState } from '@/store/entities/helpers';
import { ADD_EVENT } from '@/store/entities/event/types';

const initialState = createInitialEntityState<Event>();

export default createReducer(initialState, {
  [ADD_EVENT]: (state, action) => addEntity(state, action.payload),
});
