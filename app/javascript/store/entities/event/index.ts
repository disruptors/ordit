import * as eventTypes from './types';
import eventReducer from './reducer';

export { eventTypes };

export default eventReducer;
