import { SUBMIT_CHOICE_OPTION } from './types';
import { createAction } from '@reduxjs/toolkit';
import { ChoiceOptions } from '@/schemas';

export const submitChoiceOption = createAction<Partial<ChoiceOptions>>(
  SUBMIT_CHOICE_OPTION
);
