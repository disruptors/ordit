import { mergeEntities } from '@/store/entities/actions';
import { SUBMIT_CHOICE_OPTION } from '@/store/entities/choiceOption/types';
import ApiClient from '@/utils/ApiClient';
import SagaEvents from '@/utils/SagaEvents';
import { call, put, takeLatest } from 'redux-saga/effects';
import { ActionType } from '@/store/entities/helpers';

function* submitChoiceOption(action: ActionType) {
  if (action.payload.id) {
    yield call(updateChoiceOption, action.payload);
  } else {
    yield call(createChoiceOption, action.payload);
  }
}

function* updateChoiceOption(payload: any) {
  const response = yield call(
    ApiClient.patch,
    `/api/choice_options/${payload.id}`,
    payload
  );
  yield put(mergeEntities(response));
}

function* createChoiceOption(payload: any) {
  const response = yield call(ApiClient.post, '/api/choice_options', payload);
  yield put(mergeEntities(response));
}

function* choiceOptionSaga() {
  yield takeLatest(SUBMIT_CHOICE_OPTION, SagaEvents.requestCycle, submitChoiceOption);
}

export default choiceOptionSaga;
