import { StoreState } from '@/store/index';
import { ChoiceOptions } from '@/schemas';

const filterChoiceOptions = (choiceIds: string | Array<string>) => (
  state: StoreState
) => {
  const choiceOptionsIds = state.entities.choiceOptions.all;

  return choiceOptionsIds.filter((choiceOptionId: string) => {
    const choiceOption: ChoiceOptions = getChoiceOption(choiceOptionId)(state);

    if (Array.isArray(choiceIds)) {
      return choiceIds.includes(choiceOption.choice) && choiceOption.active;
    }

    return choiceOption.choice === choiceIds && choiceOption;
  });
};

export const getChoiceOption = (choiceOptionId: string) => (state: StoreState) =>
  state.entities.choiceOptions.byId[choiceOptionId];

export const getChoiceOptionsPerChoiceId = (choiceId: string) => (state: StoreState) =>
  filterChoiceOptions(choiceId)(state).map((choiceOptionId: string) =>
    getChoiceOption(choiceOptionId)(state)
  );
