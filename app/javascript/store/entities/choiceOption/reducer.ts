import { ChoiceOptions } from '@/schemas';
import { ADD_CHOICE_OPTION } from '@/store/entities/choiceOption/types';
import { addEntity, createInitialEntityState } from '@/store/entities/helpers';
import { createReducer } from '@reduxjs/toolkit';

const initialState = createInitialEntityState<ChoiceOptions>();

export default createReducer(initialState, {
  [ADD_CHOICE_OPTION]: (state, action) => addEntity(state, action.payload),
});
