import { StoreState } from '@/store';

export const getTag = (id: string) => (state: StoreState) => state.entities.tag.byId[id];

export const getTagPerBusiness = (businessId: string | null) => (state: StoreState) =>
  state.entities.tag.all
    .map(id => getTag(id)(state))
    .filter(category => category.businessId == businessId);
