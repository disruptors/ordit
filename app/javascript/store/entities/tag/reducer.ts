import { Tag } from '@/schemas/Tag';
import { addEntity, createInitialEntityState } from '@/store/entities/helpers';
import { createReducer } from '@reduxjs/toolkit';
import { ADD_TAG } from './types';

const initialState = createInitialEntityState<Tag>();

export default createReducer(initialState, {
  [ADD_TAG]: (state, action) => addEntity(state, action.payload),
});
