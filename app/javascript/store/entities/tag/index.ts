import * as tagTypes from './types';
import tagReducer from './reducer';

export { tagTypes };

export default tagReducer;
