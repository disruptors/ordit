import { TransformedEntities } from '@/utils/ApiTransformer';
import { Attributes, ResourceData } from 'json-api-normalizer';
import omit from 'lodash/omit';
import { Action } from 'redux';

export interface State<A extends Attributes> {
  all: any[];
  byId: TransformedEntities<A>;
}

export const createInitialEntityState = <A extends Attributes>(): State<A> => ({
  all: [],
  byId: {},
});

interface Callbacks {
  successCallback?: Function;
  errorCallback?: Function;
}

export interface ActionType<P = any> extends Action<string> {
  payload: P & Callbacks;
}

export interface EntityType<A extends Attributes = Attributes> {
  [id: string]: ResourceData<A>;
}

/**
 * Note: This function will actually merge entities with the existing
 * entities in the redux store.
 */
export const addEntity = <A>(
  state: State<A>,
  entity: TransformedEntities<A>
): State<A> => ({
  ...state,
  byId: {
    ...state.byId,
    ...entity,
  },
  all: [...new Set((state || []).all.concat(Object.keys(entity)))],
});

export const updateEntity = <A>(
  state: State<A>,
  entityId: string,
  entity: TransformedEntities<A>
): State<A> => ({
  ...state,
  byId: {
    ...state.byId,
    [entityId]: {
      ...state.byId[entityId],
      ...entity,
    },
  },
});

export const removeEntity = <A>(state: State<A>, entityId: string): State<A> => ({
  ...state,
  all: state.all.filter((id: string) => id !== entityId),
  byId: omit(state.byId, [entityId]),
});
