import { MenuItem } from '@/schemas/MenuItem';
import {
  addEntity,
  createInitialEntityState,
  removeEntity,
} from '@/store/entities/helpers';
import { ADD_MENU_ITEM, REMOVE_MENU_ITEM } from '@/store/entities/menuItem/types';
import { createReducer } from '@reduxjs/toolkit';

const initialState = createInitialEntityState<MenuItem>();

export default createReducer(initialState, {
  [ADD_MENU_ITEM]: (state, action) => addEntity(state, action.payload),
  [REMOVE_MENU_ITEM]: (state, action) => removeEntity(state, action.payload),
});
