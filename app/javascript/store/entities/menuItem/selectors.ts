import { StoreState } from '@/store/index';

const getIds = (state: StoreState) => state.entities.menuItems.all;

export const getMenuItem = (id: string) => (state: StoreState) =>
  state.entities.menuItems.byId[id];

export const getBusinessMenuItems = (businessId: string) => (state: StoreState) => {
  const filteredIds = getIds(state).filter(
    id => getMenuItem(id)(state).business === businessId
  );
  const items = filteredIds.map((id: string) => getMenuItem(id)(state));
  return items;
};

export const getMenuItemsByIds = (ids: string[]) => (state: StoreState) =>
  ids.map(id => getMenuItem(id)(state)).filter(item => item);
