import history from '@/business/history';
import { mergeEntities } from '@/store/entities/actions';
import { ActionType } from '@/store/entities/helpers';
import {
  FETCH_MENU_ITEM,
  FETCH_MENU_ITEMS,
  REMOVE_MENU_ITEM,
  SUBMIT_MENU_ITEM,
} from '@/store/entities/menuItem/types';
import ApiClient from '@/utils/ApiClient';
import SagaEvents from '@/utils/SagaEvents';
import { call, put, takeLatest } from 'redux-saga/effects';

function* fetchMenuItem(action: ActionType<string>) {
  const data = yield call(ApiClient.get, `/api/menu_items/${action.payload}`);
  yield put(mergeEntities(data));
}

function* fetchMenuItems() {
  const data = yield call(ApiClient.get, '/api/menu_items');
  yield put(mergeEntities(data));
}

function* submitMenuItem(action: ActionType) {
  if (action.payload.id) {
    yield call(updateMenuItem, action.payload);
  } else {
    yield call(createMenuItem, action.payload);
  }
}

function* updateMenuItem(payload: any) {
  const response = yield call(ApiClient.patch, `/api/menu_items/${payload.id}`, payload, {
    showAlerts: true,
  });
  yield put(mergeEntities(response));
}

function* createMenuItem(payload: any) {
  const response = yield call(ApiClient.post, '/api/menu_items', payload, {
    showAlerts: true,
  });
  yield put(mergeEntities(response));
  history.push('/menu_items');
}

function* removeMenuItem(action: ActionType<string>) {
  yield call(ApiClient.destroy, `/api/menu_items/${action.payload}`, {
    showAlerts: true,
  });
}

function* menuItemSaga() {
  yield takeLatest(FETCH_MENU_ITEM, SagaEvents.requestCycle, fetchMenuItem);
  yield takeLatest(FETCH_MENU_ITEMS, SagaEvents.requestCycle, fetchMenuItems);
  yield takeLatest(SUBMIT_MENU_ITEM, SagaEvents.requestCycle, submitMenuItem);
  yield takeLatest(REMOVE_MENU_ITEM, SagaEvents.requestCycle, removeMenuItem);
}

export default menuItemSaga;
