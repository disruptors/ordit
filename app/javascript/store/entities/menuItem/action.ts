import { MenuItem } from '@/schemas';
import {
  FETCH_MENU_ITEM,
  FETCH_MENU_ITEMS,
  REMOVE_MENU_ITEM,
  SUBMIT_MENU_ITEM,
} from '@/store/entities/menuItem/types';
import { createAction } from '@reduxjs/toolkit';

export const fetchMenuItems = createAction(FETCH_MENU_ITEMS, (query: any = {}) => ({
  payload: { query },
}));
export const fetchMenuItem = createAction<string>(FETCH_MENU_ITEM);
export const submitMenuItem = createAction<Partial<MenuItem>>(SUBMIT_MENU_ITEM);
export const removeMenuItem = createAction<string>(REMOVE_MENU_ITEM);
