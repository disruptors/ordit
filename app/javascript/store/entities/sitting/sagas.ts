import { takeLatest, call, put } from 'redux-saga/effects';
import * as api from '@/api';
import {
  SUBMIT_SITTING,
  SUBSCRIBE_SITTING_CHANNEL,
  FETCH_SITTING,
} from '@/store/entities/sitting/types';
import SagaEvents from '@/utils/SagaEvents';
import { ActionType } from '@/store/entities/helpers';
import { mergeEntities } from '@/store/entities/actions';
import ApiClient from '@/utils/ApiClient';
import consumer from 'channels/consumer';

function* submitSitting(action: ActionType) {
  if (action.payload.sitting.id) {
    yield call(updateSitting, action.payload);
  } else {
    yield call(createSitting, action.payload);
  }
}

function* updateSitting(payload: any) {
  const { sitting, successCallback, options } = payload;
  const response = yield call(ApiClient.patch, `/api/sittings/${sitting.id}`, sitting, {
    showAlerts: true,
    ...options,
  });

  yield put(mergeEntities(response));

  if (successCallback) {
    yield call(successCallback, response);
  }
}

function* createSitting(payload: any) {
  const { sitting, successCallback, options } = payload;
  const response = yield call(ApiClient.post, `/api/sittings`, sitting, {
    showAlerts: true,
    ...options,
  });

  yield put(mergeEntities(response));

  if (successCallback) {
    yield call(successCallback, response);
  }
}

function* fetchSitting(action: ActionType) {
  const response = yield call(api.sittings.getById, action.payload.id);
  yield put(mergeEntities(response));
}

function* subscribeChannel(action: ActionType) {
  const channel = {
    channel: 'SittingChannel',
    ...action.payload,
  };

  const handlers = {
    connected() {
      console.log('connected');
    },
    received(data: any) {
      console.log(data);
    },
  };

  yield call([consumer.subscriptions, 'create'], channel, handlers);
}

function* sittingSaga() {
  yield takeLatest(SUBMIT_SITTING, SagaEvents.requestCycle, submitSitting);
  yield takeLatest(SUBSCRIBE_SITTING_CHANNEL, subscribeChannel);
  yield takeLatest(FETCH_SITTING, SagaEvents.requestCycle, fetchSitting);
}

export default sittingSaga;
