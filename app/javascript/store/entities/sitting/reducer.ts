import { Sitting } from '@/schemas/Sitting';
import { addEntity, createInitialEntityState } from '@/store/entities/helpers';
import { ADD_SITTING } from '@/store/entities/sitting/types';
import { createReducer } from '@reduxjs/toolkit';

const initialState = createInitialEntityState<Sitting>();

export default createReducer(initialState, {
  [ADD_SITTING]: (state, action) => addEntity(state, action.payload),
});
