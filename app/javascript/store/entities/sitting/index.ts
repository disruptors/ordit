import * as sittingAction from './actions';
import * as sittingTypes from './types';
import sittingSaga from './sagas';
import sittingReducer from './reducer';
import * as sittingSelectors from './selectors';

export { sittingSaga, sittingAction, sittingTypes, sittingSelectors };

export default sittingReducer;
