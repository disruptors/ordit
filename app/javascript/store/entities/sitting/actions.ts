import {
  FETCH_SITTING,
  FETCH_SITTINGS,
  SUBSCRIBE_SITTING_CHANNEL,
} from '@/store/entities/sitting/types';
import { createAction } from '@reduxjs/toolkit';
import { ADD_SITTING, SUBMIT_SITTING } from './types';

export const fetchSittings = (query: any = {}) => ({
  type: FETCH_SITTINGS,
  payload: { query },
});

export const fetchSitting = (payload: { id: string; query?: any }) => ({
  type: FETCH_SITTING,
  payload,
});

export const addSitting = createAction<any>(ADD_SITTING);

export const submitSitting = createAction<any>(SUBMIT_SITTING);

export const subscribeChannel = createAction<any>(SUBSCRIBE_SITTING_CHANNEL);
