import { StoreState } from '@/store/index';

const getIds = (state: StoreState) => state.entities.sittings.all;

export const getSitting = (id: string) => (state: StoreState) =>
  state.entities.sittings.byId[id];

export const getSittingsByIds = (ids: string[]) => (state: StoreState) =>
  ids.map(id => getSitting(id)(state)).filter(sitting => sitting);

export const getSittings = (state: StoreState) => getSittingsByIds(getIds(state))(state);
