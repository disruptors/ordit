import { ActionType } from '@/store/entities/helpers';
import { MERGE_ENTITIES } from '@/store/entities/types';
import ApiNormalizer from '@/utils/ApiNormalizer';
import ApiTransformer from '@/utils/ApiTransformer';
import SagaEvents from '@/utils/SagaEvents';
import Text from '@/utils/Text';
import { all, put, takeEvery } from 'redux-saga/effects';

/**
 * This saga will first normalize the json api object, and then transforms
 * the normalized data, and finally merges the entity to the entities store.
 */
function* mergeEntities(action: ActionType) {
  const normalized = ApiNormalizer.normalize(action.payload);
  yield all(
    Object.keys(normalized).map(entity =>
      put({
        type: `ADD_${Text.camelToSnakeCase(entity).toUpperCase()}`,
        payload: ApiTransformer.transform(normalized[entity]),
      })
    )
  );
}

function* entitiesSaga() {
  yield takeEvery(MERGE_ENTITIES, SagaEvents.requestCycle, mergeEntities);
}

export default entitiesSaga;
