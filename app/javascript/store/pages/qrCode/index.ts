import * as qrCodeAction from './actions';
import qrCodeReducer from './reducer';

export { qrCodeAction };

export default qrCodeReducer;
