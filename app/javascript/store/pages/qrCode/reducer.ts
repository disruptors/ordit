import { createReducer } from '@reduxjs/toolkit';
import { SELECTED_BUSINESS } from './types';

interface State {
  selectedBusinessId: string | number | null;
}

const initialState: State = {
  selectedBusinessId: null,
};

export default createReducer(initialState, {
  [SELECTED_BUSINESS]: (state, action) => ({
    ...initialState,
    selectedBusinessId: action.payload,
  }),
});
