import { createAction } from '@reduxjs/toolkit';
import { FETCH_QR_CODE, SELECTED_BUSINESS } from './types';

export const selectedBusiness = createAction<string | number>(SELECTED_BUSINESS);

export const fetchQrCode = createAction(FETCH_QR_CODE, (id: string) => ({
  payload: { id },
}));
