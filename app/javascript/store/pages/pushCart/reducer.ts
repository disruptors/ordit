import { ADD_TO_CART, CLEAR_CART } from '@/store/pages/pushCart/types';
import { createReducer } from '@reduxjs/toolkit';

export interface PickedItemType {
  tempId: string; //unix timestamp ? will be used for deletion.
  menuItemId: string;
  name: string;
  quantity: number;
  price: number;
}

interface State {
  pickedItems: PickedItemType[];
}

const initialState: State = {
  pickedItems: [],
};

export default createReducer(initialState, {
  [ADD_TO_CART]: (state, action) => {
    state.pickedItems = [...state.pickedItems.concat(action.payload)];
  },
  [CLEAR_CART]: state => {
    state.pickedItems = [];
  },
});
