import { ADD_TO_CART } from '@/store/pages/pushCart/types';
import { createAction } from '@reduxjs/toolkit';

export const addToCart = createAction<any>(ADD_TO_CART);
