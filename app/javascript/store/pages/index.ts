import { combineReducers } from 'redux';
import qrCode from './qrCode';
import pushCart from './pushCart';

export default combineReducers({
  qrCode,
  pushCart,
  dashboard: () => ({}),
});
