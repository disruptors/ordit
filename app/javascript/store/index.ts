import entities from '@/store/entities';
import { businessSaga } from '@/store/entities/business';
import { menuItemSaga } from '@/store/entities/menuItem';
import { qrCodeSaga } from '@/store/entities/qrCode';
import { customerSaga } from '@/store/entities/customer';
import { userSaga } from '@/store/entities/user';
import { orderSaga } from '@/store/entities/order';
import { sittingSaga } from '@/store/entities/sitting';
import { categorySaga } from '@/store/entities/category';
import { choiceSaga } from '@/store/entities/choice';
import { choiceOptionSaga } from '@/store/entities/choiceOption';
import { needSaga } from './entities/need';

import entitiesSaga from '@/store/entities/sagas';
import loaders from '@/store/loaders';
import pages from '@/store/pages';
import sessions, { sessionSaga } from '@/store/session';
import { combineReducers } from 'redux';
import { all, fork } from 'redux-saga/effects';

// REGISTER ALL ENTITY SAGAS HERE
//
//
function* rootSaga() {
  yield all([
    fork(userSaga),
    fork(businessSaga),
    fork(sessionSaga),
    fork(qrCodeSaga),
    fork(menuItemSaga),
    fork(customerSaga),
    fork(orderSaga),
    fork(sittingSaga),
    fork(entitiesSaga),
    fork(categorySaga),
    fork(choiceSaga),
    fork(choiceOptionSaga),
    fork(needSaga),
  ]);
}

// REGISTER ALL ENTITY REDUCERS HERE
//
//
const rootReducer = combineReducers({
  entities,
  sessions,
  pages,
  loaders,
});

export type StoreState = ReturnType<typeof rootReducer>;

export { rootSaga, rootReducer };
