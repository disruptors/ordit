import { ComponentType } from 'react';

declare global {
  /**
   * This is a utility type specifically used for React components. This
   * utility type allows you to extract the Prop type of a component.
   */
  type ExtractProps<T> = T extends ComponentType<infer P> ? P : never;
}
