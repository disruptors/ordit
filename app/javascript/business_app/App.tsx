import GuestRoute from '@/business/components/shared/routes/GuestRoute';
import OnboardingRoute from '@/business/components/shared/routes/OnboardingRoute';
import PrivateRoute from '@/business/components/shared/routes/PrivateRoute';
import History from '@/business/history';
import { Role } from '@/schemas';
import { sessionActions } from '@/store/session';
import { getIsValidating } from '@/store/session/selectors';
import 'assets/styles.css';
import React, { lazy, Suspense, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Route, Router, Switch } from 'react-router-dom';

const SignUp = lazy(() => import('@/business/components/SignUp'));
const SignIn = lazy(() => import('@/business/components/SignIn'));
const Onboarding = lazy(() => import('@/business/components/Onboarding'));
const Dashboard = lazy(() => import('@/business/components/Dashboard'));
const Users = lazy(() => import('@/business/components/Users'));
const Businesses = lazy(() => import('@/business/components/Businesses'));
const MenuItems = lazy(() => import('@/business/components/MenuItems'));
const Orders = lazy(() => import('@/business/components/Orders'));
const QrCodes = lazy(() => import('@/business/components/QrCodes'));
const Customers = lazy(() => import('@/business/components/Customers'));
const Feedbacks = lazy(() => import('@/business/components/Feedbacks'));
const Profile = lazy(() => import('@/business/components/Profile'));
const NotFound = lazy(() => import('@/business/components/NotFound'));

const App = () => {
  const dispatch = useDispatch();
  const sessionValidating = useSelector(getIsValidating);

  useEffect(() => {
    dispatch(sessionActions.validate());
  }, [dispatch]);

  return !sessionValidating ? (
    <Suspense fallback={null}>
      <Router history={History}>
        <Switch>
          <GuestRoute exact path="/sign_up" children={<SignUp />} />
          <GuestRoute exact path={['/', '/sign_in']} children={<SignIn />} />
          <OnboardingRoute exact path="/business/new" children={<Onboarding />} />
          <PrivateRoute exact path="/dashboard" children={<Dashboard />} />
          <PrivateRoute path="/users" children={<Users />} allowed={[Role.Admin]} />
          <PrivateRoute path="/businesses" children={<Businesses />} />
          <PrivateRoute path={['/menu_items', '/categories']} children={<MenuItems />} />
          <PrivateRoute path="/orders" children={<Orders />} />
          <PrivateRoute path="/qr_codes" children={<QrCodes />} />
          <PrivateRoute path="/customers" children={<Customers />} />
          <PrivateRoute path="/feedbacks" children={<Feedbacks />} />
          <PrivateRoute path="/profile" children={<Profile />} />
          <Route children={<NotFound />} />
        </Switch>
      </Router>
    </Suspense>
  ) : null; // TODO: Show some loading indicator
};

export default App;
