import { getCurrentUser } from '@/store/session/selectors';
import React from 'react';
import { useSelector } from 'react-redux';
import MainLayout from '../shared/layouts/MainLayout';

const Dashboard = () => {
  const currentUser = useSelector(getCurrentUser);

  return (
    <MainLayout wrapCard>
      <h1>Dashboard</h1>
      <h3>{`Hello ${currentUser?.email}`}</h3>
    </MainLayout>
  );
};

export default Dashboard;
