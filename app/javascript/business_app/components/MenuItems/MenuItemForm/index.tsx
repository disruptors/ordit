import CategoryAdd from '@/business/components/Categories/CategoryAdd';
import AdminForm from '@/business/components/shared/forms/AdminForm';
import AvatarUploader from '@/business/components/shared/forms/AvatarUploader';
import FormActions from '@/business/components/shared/forms/FormActions';
import { ChoiceOptions, Choices, MenuItem } from '@/schemas';
import { categoryActions } from '@/store/entities/category';
import { getCategoryPerBusiness } from '@/store/entities/category/selectors';
import { getChoicesIdsFormat } from '@/store/entities/choice/selectors';
import { menuItemAction } from '@/store/entities/menuItem';
import { TransformedData } from '@/utils/ApiTransformer';
import * as FormRules from '@/utils/FormRules';
import ChoicesFormat from '@/utils/NestedAttributes/ChoicesFormat';
import { PlusOutlined } from '@ant-design/icons';
import { Form } from '@ant-design/compatible';
import '@ant-design/compatible/assets/index.css';
import { Col, Divider, Input, Row, Select } from 'antd';
import { FormComponentProps } from '@ant-design/compatible/lib/form';
import { omit } from 'lodash';
import React, { FormEvent, FunctionComponent, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ChoicesInput from './ChoicesInput';

const { Option } = Select;

export const menuItemStatus = ['available', 'unavailable', 'out of stock'];

export interface ChoicesFormValues {
  [key: string]: {
    id?: string;
    name: string;
    choiceOptions?: {
      [key: string]: {
        id?: string;
        name: string;
        price: string;
      };
    };
  };
}

export interface FormValues {
  id: string;
  name: string;
  categoryId: string;
  price: string;
  businessId: string;
  currentImage: string;
  status: string;
  choices: ChoicesFormValues;
}

interface MenuItemForm extends Partial<MenuItem> {
  choicesAttributes?:
    | Array<Partial<Choices> & { choiceOptionsAttributes: Array<Partial<ChoiceOptions>> }>
    | ChoicesFormValues;
}

interface Props extends FormComponentProps<FormValues> {
  initialValues?: Partial<TransformedData<MenuItem>>;
  title: string;
  businessId: string;
}

const MenuItemForm: FunctionComponent<Props> = ({
  form,
  initialValues,
  title,
  businessId,
}) => {
  const isEdit = !!initialValues;

  const { getFieldDecorator, validateFieldsAndScroll } = form;
  const dispatch = useDispatch();
  const categories = useSelector(getCategoryPerBusiness(businessId));

  const menuItemId: string | undefined = initialValues?.id;

  const existChoiceOptionKeys: { [key: string]: Array<string> } = useSelector(
    getChoicesIdsFormat(menuItemId)
  );

  const categoriesName: Array<string> = categories.map(category =>
    category.name.toLowerCase()
  );

  const [visibleModal, setVisibleModal] = useState<boolean>(false);

  useEffect(() => {
    dispatch(
      categoryActions.fetchCategories({
        businessId,
      })
    );
  }, [businessId, dispatch]);

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    validateFieldsAndScroll((err, values) => {
      if (err) {
        return;
      }

      const data: MenuItemForm = {
        ...values,
        businessId,
        choicesAttributes: ChoicesFormat(values.choices),
      };

      if (isEdit) {
        data.id = menuItemId;
      }

      dispatch(menuItemAction.submitMenuItem({ ...omit(data, ['choices']) }));
    });
  };

  const optionsMap = categories.map(category => (
    <Option key={category.id} value={category.id}>
      {category.name}
    </Option>
  ));

  const statusMap = menuItemStatus.map((status: string) => (
    <Option key={status} value={status}>
      {status}
    </Option>
  ));

  const responseCol: { [key: string]: number } = {
    xs: 24,
    sm: 24,
    md: 24,
    lg: 12,
    xl: 12,
  };

  return (
    <>
      <AdminForm
        title={title}
        onSubmit={handleSubmit}
        actions={<FormActions cancelTo="/menu_items" isEdit={isEdit} />}
      >
        <Row gutter={16}>
          <Col span={12} {...responseCol}>
            <Form.Item label="Image">
              {getFieldDecorator(
                'image',
                {}
              )(<AvatarUploader previewImage={initialValues?.currentImage} />)}
            </Form.Item>

            <Form.Item label="Name">
              {getFieldDecorator('name', {
                rules: [FormRules.required('name')],
                initialValue: initialValues?.name,
              })(<Input />)}
            </Form.Item>

            <Form.Item label="Status">
              {getFieldDecorator('status', {
                rules: [FormRules.required('status')],
                initialValue: initialValues?.status || 'available',
              })(<Select>{statusMap}</Select>)}
            </Form.Item>

            <Form.Item label="Category">
              {getFieldDecorator('categoryId', {
                rules: [FormRules.required('category')],
                initialValue: initialValues?.category,
              })(
                <Select
                  notFoundContent="-- No Category Yet! --"
                  placeholder="Please select or add category"
                  dropdownRender={menu => (
                    <>
                      {menu}
                      <Divider style={{ margin: '0' }} />
                      <div
                        style={{ padding: '4px 8px', cursor: 'pointer' }}
                        onMouseDown={e => {
                          e.preventDefault();
                          setVisibleModal(true);
                        }}
                      >
                        <PlusOutlined />
                        Add Category
                      </div>
                    </>
                  )}
                >
                  {optionsMap}
                </Select>
              )}
            </Form.Item>
            <Form.Item label="Price">
              {getFieldDecorator('price', {
                rules: [FormRules.required('price')],
                initialValue: initialValues?.price,
              })(<Input />)}
            </Form.Item>
          </Col>
          <Col span={12} {...responseCol}>
            <ChoicesInput
              existChoiceOptionKeys={existChoiceOptionKeys}
              getFieldDecorator={getFieldDecorator}
            />
          </Col>
        </Row>
      </AdminForm>

      <CategoryAdd
        businessId={businessId}
        categoriesName={categoriesName}
        visible={visibleModal}
        setVisible={setVisibleModal}
      />
    </>
  );
};

export default Form.create<Props>({ name: 'menuItemForm' })(MenuItemForm);
