import React, { FunctionComponent, useState, useEffect } from 'react';
import * as FormRules from '@/utils/FormRules';
import { CloseOutlined, DeleteOutlined } from '@ant-design/icons';
import { Form } from '@ant-design/compatible';
import '@ant-design/compatible/assets/index.css';
import { Input, Row, Col, InputNumber, Button, Card, Popconfirm } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import { StoreState } from '@/store';
import { isDash1st } from '@/utils/Text';
import { choiceAction } from '@/store/entities/choice';
import { choiceOptionAction } from '@/store/entities/choiceOption';
import { omit } from 'lodash';
import { ChoicesInactiveFormat } from '@/utils/NestedAttributes/ChoicesFormat';
import { GetFieldDecoratorOptions } from '@ant-design/compatible/lib/form/Form';

interface Props {
  existChoiceOptionKeys: { [key: string]: Array<string> };
  getFieldDecorator: <T extends Object = {}>(
    id: keyof T,
    options?: GetFieldDecoratorOptions | undefined
  ) => (node: React.ReactNode) => React.ReactNode;
}

const randomizeStr = (num: number = 7): string =>
  `-${Math.random()
    .toString(36)
    .substring(num)}`;

const ChoicesInput: FunctionComponent<Props> = ({
  existChoiceOptionKeys,
  getFieldDecorator,
}) => {
  const [isUpdated, setIsUpdated] = useState<boolean>(false);
  const [choiceOptionKeys, setChoiceOptionKeys] = useState<{
    [key: string]: Array<string>;
  }>({});

  useEffect(() => {
    const notEmptyOptions = (): boolean => {
      const options = Object.keys(existChoiceOptionKeys).filter(
        (choiceKey: string) => existChoiceOptionKeys[choiceKey].length > 0
      );
      return options.length > 0;
    };

    if (
      !isUpdated &&
      Object.keys(choiceOptionKeys).length === 0 &&
      Object.keys(existChoiceOptionKeys).length > 0 &&
      notEmptyOptions()
    ) {
      setIsUpdated(true);
      setChoiceOptionKeys(existChoiceOptionKeys);
    }
  }, [
    isUpdated,
    setIsUpdated,
    setChoiceOptionKeys,
    choiceOptionKeys,
    existChoiceOptionKeys,
  ]);

  const dispatch = useDispatch();
  const choices = useSelector((state: StoreState) => state.entities.choices.byId);
  const choiceOptions = useSelector(
    (state: StoreState) => state.entities.choiceOptions.byId
  );

  const addChoices = () => {
    setChoiceOptionKeys({
      ...choiceOptionKeys,
      [randomizeStr()]: [],
    });
  };

  const addChoiceOption = (choiceKey: string) => {
    const keyStr: string = randomizeStr();
    const optionsData: Array<string> = choiceOptionKeys[choiceKey] || [];

    setChoiceOptionKeys({
      ...choiceOptionKeys,
      [choiceKey]: [...optionsData, keyStr],
    });
  };

  const deleteChoices = (choiceKey: string) => {
    setChoiceOptionKeys(omit(choiceOptionKeys, [choiceKey]));

    if (!isDash1st(choiceKey)) {
      dispatch(
        choiceAction.submitChoice(ChoicesInactiveFormat(choiceKey, existChoiceOptionKeys))
      );
    }
  };

  const deleteChoiceOptions = (choiceKey: string, choiceOptKey: string) => {
    setChoiceOptionKeys({
      ...choiceOptionKeys,
      [choiceKey]: choiceOptionKeys[choiceKey].filter(k => k != choiceOptKey),
    });

    if (!isDash1st(choiceOptKey)) {
      dispatch(
        choiceOptionAction.submitChoiceOption({ id: choiceOptKey, active: false })
      );
    }
  };

  return (
    <>
      <Form.Item>
        <Button type="dashed" onClick={addChoices}>
          Add Choices
        </Button>
      </Form.Item>
      {Object.keys(choiceOptionKeys).map((key: string, i: number) => (
        <div key={key} style={{ marginBottom: '8px' }}>
          <Card
            style={{ width: '100%' }}
            title={
              <Form.Item>
                {getFieldDecorator(`choices[${key}].name`, {
                  rules: [FormRules.required('Choices Name')],
                  initialValue: choices[key]?.name,
                })(<Input placeholder="Choice Name" />)}
              </Form.Item>
            }
            actions={[
              <Popconfirm
                placement="bottom"
                title="Are you sure you want to remove this option?"
                onConfirm={() => deleteChoices(key)}
                okText="Yes"
                cancelText="No"
                key="delete"
              >
                <DeleteOutlined />
              </Popconfirm>,
            ]}
          >
            {choiceOptionKeys[key].map((optKey: string, ii: number) => (
              <Row key={optKey}>
                <Col span={16}>
                  <Form.Item style={{ width: '100%' }}>
                    {getFieldDecorator(`choices[${key}].choiceOptions[${optKey}].name`, {
                      rules: [FormRules.required(`${i}.${ii} option name`)],
                      initialValue: choiceOptions[optKey]?.name,
                    })(<Input />)}
                  </Form.Item>
                </Col>
                <Col span={6}>
                  <Form.Item>
                    {getFieldDecorator(`choices[${key}].choiceOptions[${optKey}].price`, {
                      rules: [FormRules.required(`${i}.${ii} option price`)],
                      initialValue: choiceOptions[optKey]?.price,
                    })(<InputNumber />)}
                  </Form.Item>
                </Col>
                <Col span={2}>
                  <Popconfirm
                    placement="bottomRight"
                    title="Are you sure you want to remove this option?"
                    onConfirm={() => deleteChoiceOptions(key, optKey)}
                    okText="Yes"
                    cancelText="No"
                  >
                    <CloseOutlined />
                  </Popconfirm>
                </Col>
              </Row>
            ))}
            <Button
              type="dashed"
              style={{ width: '60%' }}
              onClick={() => addChoiceOption(key)}
            >
              Add field
            </Button>
          </Card>
        </div>
      ))}
    </>
  );
};

export default ChoicesInput;
