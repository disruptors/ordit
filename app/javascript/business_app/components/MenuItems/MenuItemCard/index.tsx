import history from '@/business/history';
import PriceDisplay from '@/customer/components/shared/PriceDisplay';
import { StoreState } from '@/store';
import { menuItemAction } from '@/store/entities/menuItem';
import { DeleteOutlined, EditOutlined, EllipsisOutlined } from '@ant-design/icons';
import { Card, Popconfirm } from 'antd';
import React, { FunctionComponent } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useRouteMatch } from 'react-router-dom';
import styled from 'styled-components';

type Props = {
  menuItem: MenuItemProps;
  rest?: any;
};

type MenuItemProps = {
  id: string;
  currentImage?: string | null;
  category: string;
  price: string;
  name: string;
  description: string | null;
};

const MenuItemCard: FunctionComponent<Props> = ({ menuItem, ...rest }) => {
  const match = useRouteMatch();
  const dispatch = useDispatch();
  const categories = useSelector((state: StoreState) => state.entities.category);

  return (
    <Card
      hoverable
      cover={<CoverImage alt="example" src={menuItem.currentImage || undefined} />}
      actions={[
        <EditOutlined
          onClick={() => history.push(`${match.url}/${menuItem.id}/edit`)}
          key="edit"
        />,
        <Popconfirm
          key="delete"
          title="Are you sure to delete this?"
          okText="Yes"
          onConfirm={() => dispatch(menuItemAction.removeMenuItem(menuItem.id))}
        >
          <DeleteOutlined key="delete" />
        </Popconfirm>,
        <EllipsisOutlined key="ellipsis" />,
      ]}
      {...rest}
    >
      <Card.Meta title={menuItem.name} description={menuItem.description} />
      <strong>
        <PriceDisplay value={menuItem.price} />
      </strong>
      <p>{categories.byId[menuItem.category]?.name}</p>
    </Card>
  );
};

const CoverImage = styled.img`
  height: 250px;
  object-fit: cover;
`;

export default MenuItemCard;
