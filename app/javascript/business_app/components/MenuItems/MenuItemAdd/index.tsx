import MenuItemForm from '@/business/components/MenuItems/MenuItemForm';
import { getCurrentBusinessId } from '@/store/session/selectors';
import React, { FC } from 'react';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

interface Params {
  businessId?: string;
}

const MenuItemAdd: FC = () => {
  const params = useParams<Params>();
  const currentBusinessId = useSelector(getCurrentBusinessId);
  const businessId = params.businessId || currentBusinessId;

  return businessId ? (
    <MenuItemForm title="Add Menu Item" businessId={businessId} />
  ) : null;
};

export default MenuItemAdd;
