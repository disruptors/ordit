import CategoryIndex from '@/business/components/Categories';
import MenuItemAdd from '@/business/components/MenuItems/MenuItemAdd';
import MenuItemEdit from '@/business/components/MenuItems/MenuItemEdit';
import MenuItemsIndex from '@/business/components/MenuItems/MenuItemIndex';
import NotFound from '@/business/components/NotFound';
import MainLayout from '@/business/components/shared/layouts/MainLayout';
import { Tabs } from 'antd';
import React, { FunctionComponent } from 'react';
import { Route, Switch, useHistory, useRouteMatch } from 'react-router-dom';

const { TabPane } = Tabs;

const MenuItems: FunctionComponent = () => {
  const match = useRouteMatch();
  const { push } = useHistory();
  const onChange = (keyTo: string) => push(keyTo);

  return (
    <MainLayout>
      <Switch>
        <Tabs activeKey={match.path} onChange={onChange}>
          <TabPane tab="Menu" key="/menu_items">
            <Switch>
              <Route exact path="/menu_items" children={<MenuItemsIndex />} />
              <Route exact path={`${match.path}/add`} children={<MenuItemAdd />} />
              <Route
                exact
                path={`${match.path}/:menuItemId/edit`}
                children={<MenuItemEdit />}
              />
              <Route children={<NotFound />} />
            </Switch>
          </TabPane>
          <TabPane tab="Category" key="/categories">
            <Switch>
              <Route exact path="/categories" children={<CategoryIndex />} />
              <Route children={<NotFound />} />
            </Switch>
          </TabPane>
        </Tabs>
      </Switch>
    </MainLayout>
  );
};

export default MenuItems;
