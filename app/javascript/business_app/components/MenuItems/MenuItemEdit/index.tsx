import MenuItemForm from '@/business/components/MenuItems/MenuItemForm';
import { choiceAction } from '@/store/entities/choice';
import { getChoices } from '@/store/entities/choice/selectors';
import { menuItemAction, menuItemSelectors } from '@/store/entities/menuItem';
import { getCurrentBusinessId } from '@/store/session/selectors';
import React, { FunctionComponent, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

interface Params {
  menuItemId: string;
  businessId?: string;
}

const MenuItemEdit: FunctionComponent = () => {
  const params = useParams<Params>();
  const { menuItemId } = params;
  const dispatch = useDispatch();
  const menuItem = useSelector(menuItemSelectors.getMenuItem(menuItemId));
  const choices = useSelector(getChoices(menuItemId));
  const currentBusinessId = useSelector(getCurrentBusinessId);
  const businessId = params.businessId || currentBusinessId;

  useEffect(() => {
    dispatch(choiceAction.fetchChoices({ menuItemId }));
    dispatch(menuItemAction.fetchMenuItem(menuItemId));
  }, [dispatch, menuItemId]);

  return businessId ? (
    <MenuItemForm
      initialValues={{ ...menuItem, choices }}
      title="Edit Menu Item"
      businessId={businessId}
    />
  ) : null;
};

export default MenuItemEdit;
