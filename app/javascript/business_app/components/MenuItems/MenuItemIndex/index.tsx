import MenuItemCard from '@/business/components/MenuItems/MenuItemCard';
import SearchForm from '@/business/components/MenuItems/MenuItemIndex/SearchForm';
import ButtonLink from '@/business/components/shared/buttons/ButtonLink';
import MarginGroup from '@/business/components/shared/MarginGroup';
import { getMenuItemsByIds } from '@/store/entities/menuItem/selectors';
import { getCurrentBusinessId } from '@/store/session/selectors';
import usePushQuery from '@/utils/hooks/usePushQuery';
import useSearchList from '@/utils/hooks/useSearchList';
import { Col, Pagination, Row, Card } from 'antd';
import qs from 'qs';
import React, { FunctionComponent } from 'react';
import { useSelector } from 'react-redux';
import { useLocation, useRouteMatch } from 'react-router-dom';
import PageHeader from '@/flyd/PageHeader';
import Empty from '@/flyd/Empty';
import SpinOrEmpty from '@/flyd/SpinOrEmpty';

interface Params {
  businessId?: string;
}

const MenuItemIndex: FunctionComponent = () => {
  const match = useRouteMatch<Params>();
  const location = useLocation();
  const currentBusinessId = useSelector(getCurrentBusinessId);
  const businessId = match.params.businessId || currentBusinessId;
  const { list, isFetching, totalCount, perPage } = useSearchList(
    `/api/search?resource=menuItem&businessId=${businessId}`
  );
  const menuItems = useSelector(getMenuItemsByIds(list));
  const pushQuery = usePushQuery();

  const query = qs.parse(location.search.slice(1));

  return (
    <MarginGroup>
      <PageHeader
        title="Menu Items"
        extra={
          <>
            <ButtonLink type="primary" to={`${match.url}/add`}>
              Add Menu
            </ButtonLink>
          </>
        }
      >
        <SearchForm />
      </PageHeader>

      <Card>
        <SpinOrEmpty
          fetching={isFetching}
          empty={menuItems.length === 0}
          renderEmpty={<Empty ctaText="Add item on Menu" ctaLink={`${match.url}/add`} />}
        >
          <MarginGroup>
            <Row gutter={24}>
              {menuItems.map(menuItem => (
                <Col key={menuItem.id} xs={24 / 1} sm={24 / 2} md={24 / 3} lg={24 / 4}>
                  <div style={{ padding: '0.5rem 0' }}>
                    <MenuItemCard menuItem={menuItem} />
                  </div>
                </Col>
              ))}
            </Row>

            <Pagination
              hideOnSinglePage
              pageSize={perPage || 0}
              current={+query.page || 1}
              total={totalCount || 0}
              onChange={page => pushQuery({ ...query, page })}
            />
          </MarginGroup>
        </SpinOrEmpty>
      </Card>
    </MarginGroup>
  );
};

export default MenuItemIndex;
