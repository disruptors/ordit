import usePushQuery from '@/utils/hooks/usePushQuery';
import { SearchOutlined } from '@ant-design/icons';
import { Form } from '@ant-design/compatible';
import '@ant-design/compatible/assets/index.css';
import { Button, Input, Select } from 'antd';
import { FormComponentProps } from '@ant-design/compatible/lib/form';
import qs from 'qs';
import React, { FormEvent, FunctionComponent } from 'react';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { getCurrentBusinessId } from '@/store/session/selectors';
import { getCategoryPerBusiness } from '@/store/entities/category/selectors';
import { Category } from '@/schemas';

const SearchForm: FunctionComponent<FormComponentProps> = ({ form }) => {
  const pushQuery = usePushQuery();
  const { search } = useLocation();
  const currentBusinessId = useSelector(getCurrentBusinessId);
  const categories = useSelector(getCategoryPerBusiness(currentBusinessId));

  const { validateFieldsAndScroll, getFieldDecorator } = form;
  const query = qs.parse(search.slice(1));

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    validateFieldsAndScroll((err, values) => {
      if (!err) pushQuery(values);
    });
  };

  const categoriesOption = categories.map((category: Category) => (
    <Select.Option key={category.id} value={category.id}>
      {category.name}
    </Select.Option>
  ));

  return (
    <Form layout="inline" onSubmit={handleSubmit}>
      <Form.Item>
        {getFieldDecorator('name', {
          initialValue: query.name,
        })(<Input placeholder="Menu Name" />)}
      </Form.Item>

      <Form.Item>
        {getFieldDecorator('categoryId', {
          initialValue: query.categoryId,
        })(
          <Select placeholder="Category" style={{ width: 150 }} allowClear>
            {categoriesOption}
          </Select>
        )}
      </Form.Item>

      <Form.Item>
        <Button ghost type="primary" icon={<SearchOutlined />} htmlType="submit">
          Search
        </Button>
      </Form.Item>
    </Form>
  );
};

export default Form.create()(SearchForm);
