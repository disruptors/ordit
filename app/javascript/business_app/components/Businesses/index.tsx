import BusinessAdd from '@/business/components/Businesses/BusinessAdd';
import BusinessEdit from '@/business/components/Businesses/BusinessEdit';
import BusinessIndex from '@/business/components/Businesses/BusinessIndex';
import MenuItemAdd from '@/business/components/MenuItems/MenuItemAdd';
import MenuItemEdit from '@/business/components/MenuItems/MenuItemEdit';
import MenuItemsIndex from '@/business/components/MenuItems/MenuItemIndex';
import NotFound from '@/business/components/NotFound';
import QrCodeList from '@/business/components/QrCodes/QrCodeIndex';
import MainLayout from '@/business/components/shared/layouts/MainLayout';
import PrivateRoute from '@/business/components/shared/routes/PrivateRoute';
import { Role } from '@/schemas';
import React, { FunctionComponent } from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

const Businesses: FunctionComponent = () => {
  const match = useRouteMatch();

  return (
    <MainLayout>
      <Switch>
        <PrivateRoute
          exact
          path={match.path}
          allowed={[Role.Admin]}
          children={<BusinessIndex />}
        />
        <PrivateRoute
          exact
          path={`${match.path}/add`}
          allowed={[Role.Admin]}
          children={<BusinessAdd />}
        />
        <PrivateRoute
          exact
          path={`${match.path}/:businessId/menu_items`}
          allowed={[Role.Admin]}
          children={<MenuItemsIndex />}
        />
        <PrivateRoute
          exact
          path={`${match.path}/:businessId/menu_items/add`}
          allowed={[Role.Admin]}
          children={<MenuItemAdd />}
        />
        <PrivateRoute
          exact
          path={`${match.path}/:businessId/menu_items/:menuItemId/edit`}
          allowed={[Role.Admin]}
          children={<MenuItemEdit />}
        />
        <PrivateRoute
          exact
          path={`${match.path}/:businessId/qr_codes`}
          allowed={[Role.Admin]}
          children={<QrCodeList />}
        />
        <Route exact path={`${match.path}/:id/edit`} children={<BusinessEdit />} />
        <Route children={<NotFound />} />
      </Switch>
    </MainLayout>
  );
};

export default Businesses;
