import SearchForm from '@/business/components/Businesses/BusinessIndex/SearchForm';
import ButtonLink from '@/business/components/shared/buttons/ButtonLink';
import MarginGroup from '@/business/components/shared/MarginGroup';
import { Business } from '@/schemas';
import { getBusinessesByIds } from '@/store/entities/business/selectors';
import { TransformedData } from '@/utils/ApiTransformer';
import usePushQuery from '@/utils/hooks/usePushQuery';
import useSearchList from '@/utils/hooks/useSearchList';
import { EllipsisOutlined } from '@ant-design/icons';
import { Button, Card, Divider, Dropdown, Menu, PageHeader, Table } from 'antd';
import { ColumnProps, TableProps } from 'antd/lib/table';
import qs from 'qs';
import React, { FunctionComponent } from 'react';
import { useSelector } from 'react-redux';
import { Link, useLocation, useRouteMatch } from 'react-router-dom';

const columns: ColumnProps<TransformedData<Business>>[] = [
  {
    title: 'Business Name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Owner',
    dataIndex: 'user',
    key: 'user',
  },
  {
    title: 'Phone / Cellphone No.',
    dataIndex: 'phone',
    key: 'phone',
  },
  {
    title: 'Location',
    dataIndex: 'location',
    key: 'location',
  },
  {
    title: 'Created At',
    dataIndex: 'createdAt',
    key: 'createdAt',
  },
  {
    title: 'Action',
    dataIndex: 'action',
    key: 'action',
    render: (_, record) => {
      const menu = (
        <Menu>
          <Menu.Item>
            <Link to={`/businesses/${record.id}/menu_items`}>View Menu Items</Link>
          </Menu.Item>
          <Menu.Item>
            <Link to={`/businesses/${record.id}/qr_codes`}>View QR Codes</Link>
          </Menu.Item>
          <Menu.Item>Deactivate</Menu.Item>
        </Menu>
      );

      return (
        <>
          <Link to={`/businesses/${record.id}/edit`}>Edit</Link>
          <Divider type="vertical" />
          <Dropdown overlay={menu}>
            <Button shape="circle" icon={<EllipsisOutlined />} />
          </Dropdown>
        </>
      );
    },
  },
];

const BusinessIndex: FunctionComponent = () => {
  const match = useRouteMatch();
  const location = useLocation();
  const { list, isFetching, totalCount, perPage } = useSearchList(
    '/api/search?resource=business'
  );
  const pushQuery = usePushQuery();
  const businesses = useSelector(getBusinessesByIds(list));
  const query = qs.parse(location.search.slice(1));

  const handleTabChange: TableProps<typeof businesses[0]>['onChange'] = pagination => {
    pushQuery({ ...query, page: pagination.current });
  };

  return (
    <MarginGroup>
      <PageHeader
        title="Businesses"
        style={{ backgroundColor: 'white', border: '1px solid rgb(235, 237, 240)' }}
        extra={
          <>
            <ButtonLink
              type="primary"
              shape="circle"
              icon="plus"
              to={`${match.url}/add`}
            />
          </>
        }
      >
        <SearchForm />
      </PageHeader>

      <Card>
        <Table
          loading={isFetching}
          columns={columns}
          rowKey={record => record.id}
          dataSource={businesses}
          scroll={{ x: true }}
          onChange={handleTabChange}
          pagination={{
            current: +query.page || 1,
            total: totalCount,
            pageSize: perPage,
            hideOnSinglePage: true,
          }}
        />
      </Card>
    </MarginGroup>
  );
};

export default BusinessIndex;
