import BusinessForm from '@/business/components/Businesses/BusinessForm';
import React from 'react';

const BusinessAdd = () => <BusinessForm title="Add Business" />;

export default BusinessAdd;
