import BusinessForm from '@/business/components/Businesses/BusinessForm';
import { businessActions } from '@/store/entities/business';
import { getBusiness } from '@/store/entities/business/selectors';
import { getTagPerBusiness } from '@/store/entities/tag/selectors';
import React, { FunctionComponent, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

interface Params {
  id: string;
}

const BusinessEdit: FunctionComponent = () => {
  const { id: businessId } = useParams<Params>();
  const dispatch = useDispatch();
  const business = useSelector(getBusiness(businessId));
  const tagsAttributes = useSelector(getTagPerBusiness(businessId));

  useEffect(() => {
    dispatch(businessActions.fetchBusiness(businessId));
  }, [dispatch, businessId]);

  return (
    <BusinessForm title="Edit Business" initialValues={{ ...business, tagsAttributes }} />
  );
};

export default BusinessEdit;
