import RestrictContent from '@/business/components/shared/auth/RestrictContent';
import AdminForm from '@/business/components/shared/forms/AdminForm';
import AvatarUploader from '@/business/components/shared/forms/AvatarUploader';
import { Role } from '@/schemas';
import { Tag } from '@/schemas/Tag';
import { businessActions } from '@/store/entities/business';
import { userAction } from '@/store/entities/user';
import { getUsers } from '@/store/entities/user/selectors';
import { getCurrentUser } from '@/store/session/selectors';
import * as FormRules from '@/utils/FormRules';
import { Form } from '@ant-design/compatible';
import '@ant-design/compatible/assets/index.css';
import { Button, Input, Select } from 'antd';
import { FormComponentProps } from '@ant-design/compatible/lib/form/Form';
import React, { FunctionComponent, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

interface FormValues {
  id?: string;
  avatar?: string;
  name: string;
  location: string;
  phone: string;
  userId?: string;
  tagsAttributes?: Array<Partial<Tag>>;
}

export interface Props extends FormComponentProps {
  initialValues?: Partial<FormValues & { user: string }>;
  submitText?: string;
  title: string;
}

const BusinessForm: FunctionComponent<Props> = ({
  form,
  initialValues,
  submitText,
  title,
}) => {
  const { getFieldDecorator } = form;
  const dispatch = useDispatch();
  const currentUser = useSelector(getCurrentUser);
  const users = useSelector(getUsers);
  const isEdit = !!initialValues;

  const tagsAttributes: Array<Partial<Tag>> = initialValues?.tagsAttributes || [];
  const tagsName: string[] = tagsAttributes.map(tag => String(tag.name).toLowerCase());

  useEffect(() => {
    dispatch(userAction.fetchUsers());
  }, [dispatch]);

  const handleSubmit = (e: any) => {
    e.preventDefault();

    form.validateFieldsAndScroll((err: any, data: any) => {
      if (err) {
        return;
      }

      if (isEdit) {
        data.id = initialValues?.id;
      }

      const getTagId = (name: string) =>
        tagsAttributes.filter(
          tag => String(tag.name).toLowerCase() == name.toLowerCase()
        )[0].id;

      const { tagsAttributes: selectedTags = [] } = data;

      let tagsData = selectedTags.map((name: string) =>
        tagsName.includes(name.toLowerCase()) ? { id: getTagId(name), name } : { name }
      );

      // add _destory: true key for
      // those deselected tags
      tagsName.forEach(name => {
        if (!selectedTags.includes(name)) {
          tagsData = [...tagsData, { id: getTagId(name), name, _destroy: true }];
        }
      });

      data.userId = data.userId || currentUser?.id;
      data.tagsAttributes = tagsData;

      dispatch(businessActions.submitBusiness(data));
    });
  };

  return (
    <AdminForm
      title={title}
      onSubmit={handleSubmit}
      actions={
        <Button type="primary" htmlType="submit">
          {submitText || (isEdit ? 'Update' : 'Create')}
        </Button>
      }
    >
      <Form.Item label="Avatar">
        {getFieldDecorator(
          'avatar',
          {}
        )(<AvatarUploader previewImage={initialValues?.avatar} />)}
      </Form.Item>

      <RestrictContent allowed={[Role.Admin]}>
        <Form.Item label="User">
          {getFieldDecorator('userId', {
            rules: [FormRules.required('userId')],
            initialValue: initialValues?.user || currentUser?.id,
          })(
            <Select style={{ minWidth: 200 }}>
              {users.map(user => (
                <Select.Option key={user.id} value={user.id}>
                  {user.email}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
      </RestrictContent>

      <Form.Item label="Name">
        {getFieldDecorator('name', {
          rules: [FormRules.required('name')],
          initialValue: initialValues?.name,
        })(<Input placeholder="Name of your business" />)}
      </Form.Item>

      <Form.Item label="Tag" help="You can use `,` for separator">
        {getFieldDecorator('tagsAttributes', {
          rules: [FormRules.required('tags')],
          initialValue: tagsName,
        })(
          <Select
            mode="tags"
            style={{ width: '100%' }}
            notFoundContent={null}
            tokenSeparators={[',']}
            placeholder="Fast Food"
          />
        )}
      </Form.Item>

      <Form.Item label="Location">
        {getFieldDecorator('location', {
          rules: [FormRules.required('location')],
          initialValue: initialValues?.location,
        })(<Input placeholder="Location of your business" />)}
      </Form.Item>

      <Form.Item label="Phone">
        {getFieldDecorator('phone', {
          rules: [],
          initialValue: initialValues?.phone,
        })(<Input placeholder="09123456789" />)}
      </Form.Item>
    </AdminForm>
  );
};

export default Form.create<Props>({ name: 'business_form' })(BusinessForm);
