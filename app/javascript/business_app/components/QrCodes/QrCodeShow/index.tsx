import React, { useRef, useEffect } from 'react';
import ReactToPrint from 'react-to-print';
import { Button, Card } from 'antd';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { qrCodeAction } from '@/store/entities/qrCode';
import MarginGroup from '@/business/components/shared/MarginGroup';
import { getQrCode } from '@/store/entities/qrCode/selectors';
import PageHeader from '@/flyd/PageHeader';
import history from '@/business/history';
import Label from '@/flyd/Label';

//Using class based components because of ref.
class Printable extends React.Component<any> {
  render() {
    const { qrCode } = this.props;

    return (
      !!qrCode && (
        <div className="text-center mt-5">
          <h1 className="text-lg">{qrCode?.tableName}</h1>
          <div className="mb-2 text-base">
            <Label>Step 1</Label>
            <span className="ml-1 font-bold">Scan QR Code</span>
          </div>
          <img src={qrCode.svg} width="220" />
          <div className="mt-4 mb-4 text-base">
            <div className="mb-2">
              <Label>Step 2: </Label>
              <span className="ml-1 font-bold">Enter Pin Code</span>
            </div>
            <p className="mb-0 text-2xl">{qrCode?.pinNumber}</p>
            <Label>Pin Code</Label>
          </div>
        </div>
      )
    );
  }
}

interface Params {
  id: string;
}

const QrCodeShow = () => {
  const { id } = useParams<Params>();
  const componentRef = useRef(null);
  const dispatch = useDispatch();
  const qrCode = useSelector(getQrCode(id));

  useEffect(() => {
    dispatch(qrCodeAction.fetchQrCode(id));
  }, [id, dispatch]);

  return (
    <MarginGroup>
      <PageHeader
        title={qrCode?.tableName}
        onBack={() => history.push('/qr_codes')}
        extra={
          <ReactToPrint
            trigger={() => <Button type="primary">Print</Button>}
            content={() => componentRef.current}
          />
        }
      />

      <Card>
        <Printable ref={componentRef} qrCode={qrCode} />
      </Card>
    </MarginGroup>
  );
};

export default QrCodeShow;
