import { QrCode } from '@/schemas';
import { qrCodeAction } from '@/store/entities/qrCode';
import { MoreOutlined, PrinterOutlined, QrcodeOutlined } from '@ant-design/icons';
import { Card, Dropdown, Menu, Popconfirm, Popover, Row, Typography } from 'antd';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import React, { FunctionComponent } from 'react';
import { useDispatch } from 'react-redux';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import Label from '@/flyd/Label';

dayjs.extend(relativeTime);

const { Paragraph } = Typography;

const CardContainer = styled(Card)`
  .ant-card-head {
    padding: 0px !important;
  }
  .ant-card-body {
    padding: 0px;
    label {
      display: block;
    }
  }
`;

const CardTitle = styled(Row)`
  display: block;
  width: 100%;
  background-color: ${props => props.theme.primaryColor};
  color: white;
  padding: 20px;
  text-align: center;
  min-height: 95px;

  .ant-typography-edit {
    color: white !important;
  }
  .ant-typography {
    width: 100%;
    margin: 0px;
    font-size: 22px;
    color: white !important;
  }
`;

const CardBody = styled(Row)`
  padding: 20px;
`;

interface Props {
  qrCode: QrCode;
}

const QrCodeCard: FunctionComponent<Props> = ({ qrCode }) => {
  const dispatch = useDispatch();

  const handleEdit = (value: string) => {
    // Dont update if value was not changed, or if there is no value
    if (value === qrCode.name || value.length <= 0) {
      return;
    }
    dispatch(qrCodeAction.submitQrCode({ id: qrCode.id, name: value }));
  };

  const menu = (
    <Menu>
      <Menu.Item key="1">
        <a href={qrCode.storeUrl} target="_blank" rel="noopener noreferrer">
          Share Link
        </a>
      </Menu.Item>
      <Menu.Item key="2">
        <Popconfirm
          key="delete"
          title="Are you sure to generate new pin code?"
          okText="Yes"
          onConfirm={() =>
            dispatch(qrCodeAction.callService('GeneratePinCode', { qrCodeId: qrCode.id }))
          }
        >
          New Pin Code
        </Popconfirm>
      </Menu.Item>
      <Menu.Item key="4">
        <Popconfirm
          key="delete"
          title="Are you sure to delete this?"
          okText="Yes"
          onConfirm={() => dispatch(qrCodeAction.removeQrCode(qrCode.id))}
        >
          Delete
        </Popconfirm>
      </Menu.Item>
    </Menu>
  );

  return (
    <CardContainer
      actions={[
        <Popover
          key="qrcode"
          title="Scan QR Code"
          content={<img src={qrCode.svg} width="150" />}
          trigger={'click'}
          placement="bottom"
        >
          <QrcodeOutlined />
        </Popover>,
        <Link key="print" to={`/qr_codes/${qrCode.id}`}>
          <PrinterOutlined />
        </Link>,
        <Dropdown key="redirect" overlay={menu}>
          <a>
            <MoreOutlined />
          </a>
        </Dropdown>,
      ]}
    >
      <CardTitle>
        <Paragraph editable={{ onChange: handleEdit }}>{qrCode.name}</Paragraph>
        <small className="block">{`PIN: ${qrCode.pinNumber}`}</small>
      </CardTitle>
      <CardBody>
        <div>
          <Label size="10px">Last Used</Label>
          <span>{dayjs(qrCode.updatedAt).fromNow()}</span>
        </div>
      </CardBody>
    </CardContainer>
  );
};

export default QrCodeCard;
