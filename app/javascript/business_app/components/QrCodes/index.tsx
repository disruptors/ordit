import NotFound from '@/business/components/NotFound';
import QrCodeAdd from '@/business/components/QrCodes/QrCodeAdd';
import QrCodeIndex from '@/business/components/QrCodes/QrCodeIndex';
import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import MainLayout from '../shared/layouts/MainLayout';
import QrCodeShow from './QrCodeShow';

const QrCodes = () => {
  const match = useRouteMatch();

  return (
    <MainLayout>
      <Switch>
        <Route exact path={match.path} children={<QrCodeIndex />} />
        <Route exact path={`${match.path}/add`} children={<QrCodeAdd />} />
        <Route exact path={`${match.path}/:id`} children={<QrCodeShow />} />
        <Route children={<NotFound />} />
      </Switch>
    </MainLayout>
  );
};

export default QrCodes;
