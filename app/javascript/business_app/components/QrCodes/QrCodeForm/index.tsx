import AdminForm from '@/business/components/shared/forms/AdminForm';
import FormActions from '@/business/components/shared/forms/FormActions';
import { qrCodeAction } from '@/store/entities/qrCode';
import { getCurrentUser } from '@/store/session/selectors';
import * as FormRules from '@/utils/FormRules';
import { Form } from '@ant-design/compatible';
import '@ant-design/compatible/assets/index.css';
import { Card, Col, Input, InputNumber, Row, Switch } from 'antd';
import { FormComponentProps } from '@ant-design/compatible/lib/form';
import times from 'lodash/times';
import React, { FormEvent, FunctionComponent, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

interface Props extends FormComponentProps<FormValues> {
  title: string;
}

type FormValues = {
  qrCode: [{ name: string }];
};

const QrCodeForm: FunctionComponent<Props> = ({ form, title }) => {
  const [noQrCode, setNoQrCode] = useState<number>(1);
  const [perNumber, setPerNumber] = useState<boolean>(false);
  const [startAt, setStartAt] = useState<number>(1);
  const {
    getFieldValue,
    getFieldDecorator,
    validateFieldsAndScroll,
    setFieldsValue,
  } = form;
  const dispatch = useDispatch();
  const currentUser = useSelector(getCurrentUser);

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    validateFieldsAndScroll((err: any, values: FormValues) => {
      if (err) {
        return;
      }

      const qrCode = {
        qrCode: values.qrCode,
        businessId: currentUser?.currentBusinessId,
      };

      dispatch(qrCodeAction.submitQrCode(qrCode));
    });
  };

  useEffect(() => {
    const setQrCodeName = {};
    const tableName = (i: number) => (perNumber ? `Table ${startAt + i}` : '');

    times(noQrCode, (i: number) => {
      setQrCodeName[`qrCode[${i}].name`] = tableName(i);
    });

    setFieldsValue(setQrCodeName);
  }, [noQrCode, perNumber, startAt, getFieldValue, setFieldsValue]);

  const mapTableNames: Array<JSX.Element> = [...Array(noQrCode)].map((_, i: number) => (
    <Col
      // eslint-disable-next-line react/no-array-index-key
      key={i}
      span={6}
      style={{ marginTop: '5px', textAlign: 'center', marginBottom: '10px' }}
    >
      <Card size="small">
        <Form.Item
          htmlFor={`name-${i}`}
          wrapperCol={{ xs: { span: 24 } }}
          style={{ marginBottom: '0px' }}
        >
          {getFieldDecorator(`qrCode[${i}].name`, {
            rules: [FormRules.required('name')],
          })(
            <Input
              id={`name-${i}`}
              placeholder="Table Name"
              style={{ textAlign: 'center' }}
            />
          )}
        </Form.Item>
      </Card>
    </Col>
  ));

  return (
    <AdminForm
      title={title}
      onSubmit={handleSubmit}
      actions={<FormActions cancelTo="/qr_codes" />}
    >
      <Form.Item label="How Many Tables?">
        <InputNumber
          min={1}
          max={12}
          defaultValue={1}
          onChange={(v: any) => setNoQrCode(v)}
        />
      </Form.Item>
      <Form.Item label="Fill-up Table Names?">
        <Switch onChange={(checked: boolean) => setPerNumber(checked)} />
      </Form.Item>
      {perNumber && (
        <Form.Item label="Start At">
          <InputNumber
            min={1}
            max={12}
            defaultValue={1}
            onChange={(v: any) => setStartAt(v)}
          />
        </Form.Item>
      )}
      <Form.Item label="Tables">
        <Row gutter={16}>{mapTableNames}</Row>
      </Form.Item>
    </AdminForm>
  );
};

export default Form.create<Props>({ name: 'qrCodeForm' })(QrCodeForm);
