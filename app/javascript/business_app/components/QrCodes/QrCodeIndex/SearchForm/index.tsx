import usePushQuery from '@/utils/hooks/usePushQuery';
import { SearchOutlined } from '@ant-design/icons';
import { Form } from '@ant-design/compatible';
import '@ant-design/compatible/assets/index.css';
import { Button, Input } from 'antd';
import { FormComponentProps } from '@ant-design/compatible/lib/form';
import qs from 'qs';
import React, { FormEvent, FunctionComponent } from 'react';
import { useLocation } from 'react-router-dom';

interface FormValues {
  name: string;
}

interface Props extends FormComponentProps<FormValues> {
  isFetching: boolean;
}

const SearchForm: FunctionComponent<Props> = ({ form, isFetching }) => {
  const { search } = useLocation();
  const pushQuery = usePushQuery();

  const query = qs.parse(search.slice(1));
  const { validateFields, getFieldDecorator } = form;

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) pushQuery(values);
    });
  };

  return (
    <Form layout="inline" onSubmit={handleSubmit}>
      <Form.Item>
        {getFieldDecorator('name', {
          initialValue: query.name,
        })(<Input placeholder="Table Name" />)}
      </Form.Item>

      <Form.Item>
        <Button
          ghost
          loading={isFetching}
          type="primary"
          icon={<SearchOutlined />}
          htmlType="submit"
        >
          Search
        </Button>
      </Form.Item>
    </Form>
  );
};

export default Form.create<Props>()(SearchForm);
