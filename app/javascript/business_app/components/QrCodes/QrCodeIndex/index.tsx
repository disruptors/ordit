import QrCodeCard from '@/business/components/QrCodes/QrCodeCard';
import ButtonLink from '@/business/components/shared/buttons/ButtonLink';
import MarginGroup from '@/business/components/shared/MarginGroup';
import { getQrCodesByIds } from '@/store/entities/qrCode/selectors';
import { getCurrentBusinessId } from '@/store/session/selectors';
import usePushQuery from '@/utils/hooks/usePushQuery';
import useSearchList from '@/utils/hooks/useSearchList';
import { Card, Col, Pagination, Row } from 'antd';
import qs from 'qs';
import React, { FunctionComponent } from 'react';
import { useSelector } from 'react-redux';
import { useLocation, useRouteMatch } from 'react-router-dom';
import SearchForm from './SearchForm';
import PageHeader from '@/flyd/PageHeader';
import Empty from '@/flyd/Empty';
import SpinOrEmpty from '@/flyd/SpinOrEmpty';

interface Params {
  businessId?: string;
}

const QrCodeList: FunctionComponent = () => {
  const match = useRouteMatch<Params>();
  const location = useLocation();
  const currentBusinessId = useSelector(getCurrentBusinessId);
  const businessId = match.params.businessId || currentBusinessId;
  const { list, isFetching, totalCount, perPage } = useSearchList(
    `/api/search?resource=qrCode&businessId=${businessId}`
  );
  const qrCodes = useSelector(getQrCodesByIds(list));
  const pushQuery = usePushQuery();

  const query = qs.parse(location.search.slice(1));

  return (
    <MarginGroup>
      <PageHeader
        title="Tables"
        extra={
          <>
            <ButtonLink type="primary" to={`${match.url}/add`}>
              Add Table
            </ButtonLink>
          </>
        }
      >
        <SearchForm isFetching={isFetching} />
      </PageHeader>

      <Card>
        <SpinOrEmpty
          fetching={isFetching}
          empty={qrCodes.length === 0}
          renderEmpty={<Empty ctaText="Generate QR Code" ctaLink={`${match.url}/add`} />}
        >
          <MarginGroup>
            <Row gutter={[24, 24]}>
              {qrCodes.map(qrCode => (
                <Col
                  xs={24 / 1}
                  sm={24 / 2}
                  md={24 / 3}
                  lg={24 / 4}
                  xl={24 / 6}
                  key={qrCode.id}
                >
                  <QrCodeCard qrCode={qrCode} />
                </Col>
              ))}
            </Row>

            <Pagination
              hideOnSinglePage
              pageSize={perPage || 0}
              current={+query.page || 1}
              total={totalCount || 0}
              onChange={page => pushQuery({ ...query, page })}
            />
          </MarginGroup>
        </SpinOrEmpty>
      </Card>
    </MarginGroup>
  );
};

export default QrCodeList;
