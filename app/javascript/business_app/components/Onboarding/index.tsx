import React from 'react';
import AuthLayout from '../shared/layouts/AuthLayout';
import BusinessForm from './BusinessForm';

const Onboarding = () => (
  <AuthLayout title="Setup Business">
    <BusinessForm />
  </AuthLayout>
);

export default Onboarding;
