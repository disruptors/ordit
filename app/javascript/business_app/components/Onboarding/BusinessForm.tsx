import { businessActions } from '@/store/entities/business';
import { getCurrentUserId } from '@/store/session/selectors';
import * as FormRules from '@/utils/FormRules';
import { Form } from '@ant-design/compatible';
import '@ant-design/compatible/assets/index.css';
import { Button, Input } from 'antd';
import { FormComponentProps } from '@ant-design/compatible/lib/form';
import React, { FormEvent, FunctionComponent } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { sessionActions } from '@/store/session';
import { StoreState } from '@/store';

const BusinessForm: FunctionComponent<FormComponentProps> = ({ form }) => {
  const { getFieldDecorator } = form;
  const dispatch = useDispatch();
  const currentUserId = useSelector(getCurrentUserId);
  const loading = useSelector((state: StoreState) => state.loaders.SUBMIT_BUSINESS);

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    form.validateFieldsAndScroll((err: any, values: any) => {
      if (!err) {
        values.userId = currentUserId;
        dispatch(
          businessActions.submitBusiness(values, {
            redirectTo: '/dashboard',
            onboarding: true,
          })
        );
      }
    });
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Form.Item label="Name">
        {getFieldDecorator('name', {
          rules: [FormRules.required('name')],
        })(<Input placeholder="Name of your business" />)}
      </Form.Item>

      <Form.Item label="Location">
        {getFieldDecorator('location', {
          rules: [FormRules.required('location')],
        })(<Input placeholder="Location of your business" />)}
      </Form.Item>

      <Form.Item label="Phone">
        {getFieldDecorator('phone', {
          rules: [],
        })(<Input placeholder="Location of your business" />)}
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit" loading={loading}>
          Continue
        </Button>

        <a
          onClick={() => dispatch(sessionActions.serverSignout())}
          style={{ float: 'right' }}
          className="ant-btn"
        >
          <span>Logout</span>
        </a>
      </Form.Item>
    </Form>
  );
};

export default Form.create({ name: 'business_form' })(BusinessForm);
