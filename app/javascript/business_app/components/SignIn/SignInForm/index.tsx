import { Form } from '@ant-design/compatible';
import '@ant-design/compatible/assets/index.css';
import { Button, Input } from 'antd';
import { FormComponentProps, FormProps } from '@ant-design/compatible/lib/form';
import React, { FunctionComponent } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { sessionActions } from '@/store/session';

const SignInForm: FunctionComponent<FormComponentProps> = ({ form }) => {
  const dispatch = useDispatch();
  const { getFieldDecorator, validateFieldsAndScroll } = form;

  const handleSubmit: FormProps['onSubmit'] = async e => {
    e.preventDefault();
    validateFieldsAndScroll((err, values) => {
      if (!err) {
        dispatch(sessionActions.login(values));
      }
    });
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Form.Item label="E-mail">
        {getFieldDecorator('email', {
          rules: [
            {
              type: 'email',
              message: 'The input is not valid E-mail!',
            },
            {
              required: true,
              message: 'Please input your E-mail!',
            },
          ],
        })(<Input />)}
      </Form.Item>

      <Form.Item label="Password">
        {getFieldDecorator('password', {
          rules: [
            {
              required: true,
              message: 'Please input your password!',
            },
          ],
        })(<Input.Password />)}
      </Form.Item>

      <Form.Item>
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <Button type="primary" htmlType="submit">
            Sign In
          </Button>

          <Link to="/sign_up">Sign Up</Link>
        </div>
      </Form.Item>
    </Form>
  );
};

export default Form.create({ name: 'sign_in' })(SignInForm);
