import React from 'react';
import AuthLayout from '../shared/layouts/AuthLayout';
import SignInForm from './SignInForm';

const SignIn = () => (
  <AuthLayout title="Sign In">
    <SignInForm />
  </AuthLayout>
);

export default SignIn;
