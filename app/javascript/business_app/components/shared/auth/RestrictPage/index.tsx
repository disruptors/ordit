import Unauthorized from '@/business/components/Unauthorized';
import { Role } from '@/schemas';
import { getIsLoggedIn } from '@/store/session/selectors';
import useRestrictUser from '@/utils/hooks/useRestrictUser';
import React, { ComponentType, FC, ReactElement } from 'react';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';

interface Props {
  allowed: Role[];
  children: ReactElement;
  fallback?: ComponentType;
}

const RestrictPage: FC<Props> = ({
  allowed,
  children,
  fallback: Fallback = Unauthorized,
}) => {
  const isLoggedIn = useSelector(getIsLoggedIn);
  const isUserRestricted = useRestrictUser(allowed);

  if (!isLoggedIn && isUserRestricted) {
    return <Redirect to="/sign_in" />;
  }

  return isUserRestricted ? <Fallback /> : children;
};

export default RestrictPage;
