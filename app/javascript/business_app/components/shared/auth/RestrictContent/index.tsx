import { Role } from '@/schemas';
import useRestrictUser from '@/utils/hooks/useRestrictUser';
import { FC, ReactElement } from 'react';

interface Props {
  allowed: Role[];
  children: ReactElement;
}

/**
 * Uses `useRestrictUser` hook under the hood. The difference of this component
 * with `<RestrictPage>` component is that, this is mainly used when you just
 * want to hide a component, or a portion of your component that is restricted
 * to only Admin users. This does not handle redirection, unlike the `<RestrictPage>`
 * component.
 */
const RestrictContent: FC<Props> = ({ children, allowed }) => {
  const isRestricted = useRestrictUser(allowed);

  return isRestricted ? null : children;
};

export default RestrictContent;
