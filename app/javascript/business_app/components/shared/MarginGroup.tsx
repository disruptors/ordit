import React, { Children, FunctionComponent } from 'react';
import styled from 'styled-components';

interface Props {
  margin?: string;
  inline?: boolean;
  justifyCenter?: boolean;
  alignCenter?: boolean;
}

const MarginGroup: FunctionComponent<Props> = ({
  children,
  margin = '1rem',
  justifyCenter = false,
  alignCenter = false,
  inline = false,
}) => (
  <Container
    margin={margin}
    inline={inline}
    justifyCenter={justifyCenter}
    alignCenter={alignCenter}
  >
    {Children.map(children, child => child && <Child padding={margin}>{child}</Child>)}
  </Container>
);

interface ContainerProps {
  margin: string;
  inline: boolean;
  justifyCenter: boolean;
  alignCenter: boolean;
}
const Container = styled.div<ContainerProps>`
  margin: ${({ margin }) => `calc(${margin} / -2)`};
  display: flex;
  justify-content: ${({ justifyCenter }) => (justifyCenter ? 'center' : undefined)};
  align-items: ${({ alignCenter }) => (alignCenter ? 'center' : undefined)};
  flex-direction: ${({ inline }) => (inline ? 'row' : 'column')};
`;

interface ChildProps {
  padding: string;
}
const Child = styled.div<ChildProps>`
  padding: ${({ padding }) => `calc(${padding} / 2)`};
`;

export default MarginGroup;
