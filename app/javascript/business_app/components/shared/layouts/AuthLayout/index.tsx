import { Card, Layout } from 'antd';
import React, { FunctionComponent } from 'react';
import styled from 'styled-components';

const { Content } = Layout;

const Wrapper = styled(Layout)`
  min-height: 100vh !important;
  padding-top: 5rem;
`;

const FormContainer = styled(Card)`
  max-width: 350px;
  margin: auto !important;
`;

interface Props {
  title: string;
}

const AuthLayout: FunctionComponent<Props> = ({ children, title }) => (
  <Wrapper>
    <Content>
      <FormContainer title={title}>{children}</FormContainer>
    </Content>
  </Wrapper>
);

export default AuthLayout;
