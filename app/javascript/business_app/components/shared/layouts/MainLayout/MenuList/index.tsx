import RestrictContent from '@/business/components/shared/auth/RestrictContent';
import { useMainLayoutContext } from '@/business/components/shared/layouts/MainLayout';
import MarginGroup from '@/business/components/shared/MarginGroup';
import { Role } from '@/schemas';
import { sessionActions } from '@/store/session';
import { getCurrentBusinessId, getCurrentUser } from '@/store/session/selectors';

import {
  DashboardOutlined,
  LikeOutlined,
  LogoutOutlined,
  QrcodeOutlined,
  ReadOutlined,
  SettingOutlined,
  ShopOutlined,
  ShoppingCartOutlined,
  UserOutlined,
} from '@ant-design/icons';

import { Menu } from 'antd';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useLocation } from 'react-router-dom';
import styled from 'styled-components';

const MenuTitle = styled.h4`
  font-size: 0.75rem;
  font-weight: bold;
  text-transform: uppercase;
  color: #636e72;
  margin: 0 1rem;
`;

const MenuList = () => {
  const { collapsed } = useMainLayoutContext();
  const { pathname } = useLocation();
  const dispatch = useDispatch();
  const currentUser = useSelector(getCurrentUser);
  const currentBusinessId = useSelector(getCurrentBusinessId);

  const basePath = `/${pathname.split('/')[1]}`;

  return (
    <MarginGroup margin="1.5rem">
      <MarginGroup margin="0">
        {/* Unrestricted Menu List */}
        <Menu theme="dark" selectedKeys={[basePath]}>
          <Menu.Item key="/dashboard">
            <DashboardOutlined />
            <Link to="/dashboard">
              <span>Dashboard</span>
            </Link>
          </Menu.Item>
        </Menu>

        {/* Business Menu List */}
        {currentUser?.currentBusinessId && (
          <Menu theme="dark" selectedKeys={[basePath]}>
            <Menu.Item key="/menu_items">
              <ReadOutlined />
              <Link to="/menu_items">
                <span>Menu</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="/qr_codes">
              <QrcodeOutlined />
              <Link to="/qr_codes">
                <span>Tables</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="/orders">
              <ShoppingCartOutlined />
              <Link to="/orders">
                <span>Orders</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="/customers">
              <UserOutlined />
              <Link to="/customers">
                <span>Customers</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="/feedbacks">
              <LikeOutlined />
              <Link to="/feedbacks">
                <span>Feedback</span>
              </Link>
            </Menu.Item>
            <Menu.Item key={`/businesses/${currentBusinessId}/edit`}>
              <SettingOutlined />
              <Link to={`/businesses/${currentBusinessId}/edit`}>
                <span>Settings</span>
              </Link>
            </Menu.Item>
          </Menu>
        )}
      </MarginGroup>

      {/* Admin Menu List */}
      <RestrictContent allowed={[Role.Admin]}>
        <>
          {!collapsed && <MenuTitle>Admin</MenuTitle>}
          <Menu theme="dark" selectedKeys={[basePath]}>
            <Menu.Item key="/users">
              <UserOutlined />
              <Link to="/users">
                <span>Users</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="/businesses">
              <ShopOutlined />
              <Link to="/businesses">
                <span>Business</span>
              </Link>
            </Menu.Item>
          </Menu>
        </>
      </RestrictContent>

      {/* Account Menu List */}
      <>
        {!collapsed && <MenuTitle>Account</MenuTitle>}
        <Menu theme="dark" selectedKeys={[basePath]}>
          <Menu.Item key="/profile">
            <UserOutlined />
            <Link to="/profile">
              <span>Profile</span>
            </Link>
          </Menu.Item>

          <Menu.Item onClick={() => dispatch(sessionActions.serverSignout())}>
            <LogoutOutlined />
            <span>Logout</span>
          </Menu.Item>
        </Menu>
      </>
    </MarginGroup>
  );
};

export default MenuList;
