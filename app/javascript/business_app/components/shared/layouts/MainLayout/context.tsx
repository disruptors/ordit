import React, { createContext, FC, useContext, useMemo, useState } from 'react';

interface MainLayoutContextValue {
  collapsed: boolean;
  setCollapsed: (value: boolean, type: string) => void;
}

const MainLayoutContext = createContext<MainLayoutContextValue | undefined>(undefined);

export const MainLayoutProvider: FC = ({ children }) => {
  const initialState = localStorage.getItem('menu_collapsed') === 'true';
  const [collapsed, setCollapsed] = useState(initialState);

  const value: MainLayoutContextValue = useMemo(
    () => ({
      collapsed,
      setCollapsed: (value, type) => {
        localStorage.setItem('menu_collapsed', value.toString());
        setCollapsed(value);
        return type;
      },
    }),
    [collapsed, setCollapsed]
  );

  return (
    <MainLayoutContext.Provider value={value}>{children}</MainLayoutContext.Provider>
  );
};

export const useMainLayoutContext = () => {
  const mainLayoutContext = useContext(MainLayoutContext);
  if (!mainLayoutContext) {
    throw new Error('This hook must be used under <MainLayoutProvider>');
  }
  return mainLayoutContext;
};
