import { useMainLayoutContext } from '@/business/components/shared/layouts/MainLayout';
import MarginGroup from '@/business/components/shared/MarginGroup';
import { StoreState } from '@/store';
import { getCurrentUser } from '@/store/session/selectors';
import { EditOutlined, UserOutlined } from '@ant-design/icons';
import { Avatar, Tooltip } from 'antd';
import React, { FunctionComponent } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const Logo = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 1.5rem;
  color: white;
  text-align: center;
  min-height: 75px;
`;

const LogoName = styled.span`
  font-size: 1.25rem;
`;

const MenuBanner: FunctionComponent = () => {
  const { collapsed } = useMainLayoutContext();
  const currentUser = useSelector(getCurrentUser);
  const currentBusiness = useSelector(
    (state: StoreState) =>
      currentUser && state.entities.business.byId[currentUser?.currentBusinessId]
  );

  if (!currentUser?.currentBusinessId) {
    return null;
  }

  return (
    <Logo>
      <MarginGroup>
        <Avatar size="large" src={currentBusiness?.avatar} icon={<UserOutlined />} />
        {!collapsed && (
          <MarginGroup margin="0.5rem" inline justifyCenter alignCenter>
            <LogoName>{currentBusiness?.name}</LogoName>
            <Tooltip title="Edit Business">
              <Link to={`/businesses/${currentBusiness?.id}/edit`}>
                <EditOutlined />
              </Link>
            </Tooltip>
          </MarginGroup>
        )}
      </MarginGroup>
    </Logo>
  );
};

export default MenuBanner;
