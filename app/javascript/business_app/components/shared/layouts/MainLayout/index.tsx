import {
  MainLayoutProvider,
  useMainLayoutContext,
} from '@/business/components/shared/layouts/MainLayout/context';
import MarginGroup from '@/business/components/shared/MarginGroup';
import { Card, Layout } from 'antd';
import React, { FunctionComponent, useState } from 'react';
import MenuBanner from './MenuBanner';
import MenuList from './MenuList';

// Re-export
export { MainLayoutProvider, useMainLayoutContext };

const { Content, Sider } = Layout;

interface Props {
  /** If set to true, it will wrap the main content with the <Card> component. Default: false */
  wrapCard?: boolean;
}

const MainLayout: FunctionComponent<Props> = ({ children, wrapCard = false }) => {
  const { collapsed, setCollapsed } = useMainLayoutContext();
  const [breaked, setBreaked] = useState(false);
  const changeBreak = (val: boolean) => {
    setBreaked(val);
  };

  return (
    <Layout style={{ minHeight: '100vh' }} hasSider>
      <Sider
        style={{
          zIndex: 2,
          height: '100vh',
          position: 'sticky',
          top: 0,
          left: 0,
        }}
        width={200}
        trigger={breaked ? breaked : null}
        collapsedWidth="0"
        breakpoint="lg"
        collapsible
        collapsed={collapsed}
        onCollapse={setCollapsed}
        onBreakpoint={changeBreak}
      >
        <MarginGroup>
          <MenuBanner />
          <MenuList />
        </MarginGroup>
      </Sider>
      <Layout style={{ zIndex: 1 }}>
        <Content
          style={{
            maxWidth: '1360px',
            margin: 'auto',
            width: '100%',
            padding: '1rem',
            overflow: 'initial',
          }}
        >
          {wrapCard ? <Card>{children}</Card> : children}
        </Content>
      </Layout>
    </Layout>
  );
};

export default MainLayout;
