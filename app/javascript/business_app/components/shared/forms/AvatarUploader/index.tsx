import { Icon as LegacyIcon } from '@ant-design/compatible';
import { Upload } from 'antd';
import { UploadProps } from 'antd/lib/upload';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

const StyledUpload = styled(Upload)`
  &.avatar-uploader > .ant-upload {
    width: 128px;
    height: 128px;
  }
`;

interface Props extends UploadProps {
  previewImage?: string | null;
}

const AvatarUploader = React.forwardRef<Upload, Props>((props, ref) => {
  const { onChange, previewImage } = props;
  const [loading, setLoading] = useState(false);
  const [image, setImage] = useState(previewImage);

  const beforeUpload: UploadProps['beforeUpload'] = file => {
    getBase64(file, (url: any) => {
      setImage(url);
      setLoading(false);

      if (onChange) {
        onChange(url);
      }
    });

    return false;
  };

  useEffect(() => {
    setImage(previewImage);
  }, [previewImage]);

  const triggerChange = changedValue => {
    console.log(changedValue);
  };

  const getBase64 = (img: File, callback: Function) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  };

  const uploadButton = (
    <div>
      <LegacyIcon type={loading ? 'loading' : 'plus'} />
      <div className="ant-upload-text">Upload</div>
    </div>
  );

  return (
    <StyledUpload
      ref={ref}
      name="avatar"
      listType="picture-card"
      className="avatar-uploader"
      showUploadList={false}
      onChange={triggerChange}
      beforeUpload={beforeUpload}
    >
      {image ? <img src={image} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
    </StyledUpload>
  );
});

export default AvatarUploader;
