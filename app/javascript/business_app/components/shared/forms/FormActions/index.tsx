import ButtonLink from '@/business/components/shared/buttons/ButtonLink';
import { Form } from '@ant-design/compatible';
import '@ant-design/compatible/assets/index.css';
import { Button } from 'antd';
import React, { FunctionComponent } from 'react';
import styled from 'styled-components';

interface Props {
  cancelTo: string;
  isEdit?: boolean;
}

const CancelButton = styled(ButtonLink)`
  margin-right: 8px;
`;

const FormActions: FunctionComponent<Props> = ({
  cancelTo,
  isEdit = false,
  children,
}) => (
  <Form.Item>
    <CancelButton to={cancelTo} type="default">
      Cancel
    </CancelButton>
    <Button type="primary" htmlType="submit">
      {isEdit ? 'Update' : 'Create'}
    </Button>
    {children}
  </Form.Item>
);

export default FormActions;
