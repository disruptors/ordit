import { Form } from '@ant-design/compatible';
import '@ant-design/compatible/assets/index.css';
import { Card } from 'antd';
import { FormItemProps } from 'antd/lib/form';
import React, { FC, ReactNode } from 'react';
import { FormProps } from '@ant-design/compatible/lib/form';

/**
 * Contains default values for layout related `Form` props
 */
const defaultLayoutProps: Partial<FormProps> = {
  labelCol: {
    sm: { span: 24 },
    md: { span: 6 },
  },
  wrapperCol: {
    sm: { span: 24 },
    md: { span: 18 },
  },
  labelAlign: 'left',
};

/**
 * Contains default values for actions/footer layout `Form.Item` props
 */
const defaultActionItemProps: Partial<FormItemProps> = {
  wrapperCol: {
    sm: {
      span: 24,
      offset: 0,
    },
    md: {
      span: 18,
      offset: 6,
    },
  },
};

interface Props extends FormProps {
  title: string;
  actions: ReactNode;
  actionItemProps?: FormItemProps;
}

/**
 * This component requires the children components to be wrapped in
 * antd's `<Form.Item>` component for the layout to work.
 */
const AdminForm: FC<Props> = ({
  children,
  title,
  actions,
  actionItemProps,
  ...props
}) => {
  return (
    <Card title={title}>
      <Form {...defaultLayoutProps} {...props}>
        {children}

        <Form.Item {...defaultActionItemProps} {...actionItemProps}>
          {actions}
        </Form.Item>
      </Form>
    </Card>
  );
};

export default AdminForm;
