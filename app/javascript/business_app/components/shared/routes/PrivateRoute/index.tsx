import RestrictPage from '@/business/components/shared/auth/RestrictPage';
import { Role } from '@/schemas';
import { getCurrentUser } from '@/store/session/selectors';
import React, { FC, ReactElement } from 'react';
import { useSelector } from 'react-redux';
import { Redirect, Route, RouteProps } from 'react-router-dom';

interface Props extends RouteProps {
  children: ReactElement;
  allowed?: ExtractProps<typeof RestrictPage>['allowed'];
}

const PrivateRoute: FC<Props> = ({ children, allowed = [], ...rest }) => {
  const currentUser = useSelector(getCurrentUser);

  return (
    <Route {...rest}>
      <RestrictPage allowed={allowed}>
        {currentUser?.role === Role.Admin || currentUser?.onboardingCompleted ? (
          children
        ) : (
          <Redirect to="/business/new" />
        )}
      </RestrictPage>
    </Route>
  );
};

export default PrivateRoute;
