import { Role } from '@/schemas';
import { getCurrentUser, getIsLoggedIn } from '@/store/session/selectors';
import React, { FC } from 'react';
import { useSelector } from 'react-redux';
import { Redirect, Route, RouteProps } from 'react-router-dom';

const OnboardingRoute: FC<RouteProps> = props => {
  const isLoggedIn = useSelector(getIsLoggedIn);
  const currentUser = useSelector(getCurrentUser);

  if (!isLoggedIn) {
    return <Redirect to="/sign_in" />;
  }

  if (currentUser?.role === Role.Admin || currentUser?.onboardingCompleted) {
    return <Redirect to="/dashboard" />;
  }

  return <Route {...props} />;
};

export default OnboardingRoute;
