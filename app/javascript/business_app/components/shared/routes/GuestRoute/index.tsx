import { getIsLoggedIn } from '@/store/session/selectors';
import React, { FC } from 'react';
import { useSelector } from 'react-redux';
import { Redirect, Route, RouteProps } from 'react-router-dom';

const GuestRoute: FC<RouteProps> = props => {
  const isLoggedIn = useSelector(getIsLoggedIn);

  if (isLoggedIn) {
    return <Redirect to="/dashboard" />;
  }

  return <Route {...props} />;
};

export default GuestRoute;
