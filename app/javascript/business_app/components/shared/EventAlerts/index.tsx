import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { StoreState } from '@/store';
import { notification } from 'antd';

const EventAlerts = () => {
  const events = useSelector((state: StoreState) => state.entities.events);

  useEffect(() => {
    events.all.map(id => {
      notification.info({
        message: events.byId[id].name,
        description: 'Customer needs waiter',
      });
    });
  }, [events]);

  return <></>;
};

export default EventAlerts;
