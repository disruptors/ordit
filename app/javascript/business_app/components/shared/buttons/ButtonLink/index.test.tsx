import ButtonLink from '@/business/components/shared/buttons/ButtonLink';
import { fireEvent, render } from '@testing-library/react';
import { createBrowserHistory } from 'history';
import React from 'react';
import { Router } from 'react-router-dom';

test('Clicking the button will trigger push to history', () => {
  const history = createBrowserHistory();

  expect(history.location.pathname).toBe('/');

  const { getByTestId } = render(
    <Router history={history}>
      <ButtonLink data-testid="button" to="/test">
        Test
      </ButtonLink>
    </Router>
  );

  fireEvent.click(getByTestId('button'));

  expect(history.location.pathname).toBe('/test');
});
