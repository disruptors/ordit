import { Button } from 'antd';
import { ButtonProps } from 'antd/lib/button';
import React, { FunctionComponent } from 'react';
import { useHistory } from 'react-router-dom';

interface Props extends ButtonProps {
  to: string;
}

const ButtonLink: FunctionComponent<Props> = ({ children, to, ...rest }) => {
  const { push } = useHistory();
  return (
    <Button {...rest} onClick={() => push(to)}>
      {children}
    </Button>
  );
};

export default ButtonLink;
