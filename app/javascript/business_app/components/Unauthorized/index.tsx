import ButtonLink from '@/business/components/shared/buttons/ButtonLink';
import { Result } from 'antd';
import React, { FC } from 'react';

const Unauthorized: FC = () => (
  <Result
    status="403"
    title="403"
    subTitle="Sorry, you do not have the right permission to access this page"
    extra={
      <ButtonLink type="primary" to="/">
        Back Home
      </ButtonLink>
    }
  />
);

export default Unauthorized;
