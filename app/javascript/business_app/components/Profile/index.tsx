import MainLayout from '@/business/components/shared/layouts/MainLayout';
import UserForm from '@/business/components/Users/UserForm';
import { userAction } from '@/store/entities/user';
import { getCurrentUser } from '@/store/session/selectors';
import React, { FunctionComponent } from 'react';
import { useDispatch, useSelector } from 'react-redux';

const Profile: FunctionComponent = () => {
  const dispatch = useDispatch();
  const currentUser = useSelector(getCurrentUser);

  const handleSubmit: ExtractProps<typeof UserForm>['onSubmit'] = values => {
    if (!currentUser) {
      return;
    }
    dispatch(userAction.updateUser(currentUser.id, values));
  };

  return (
    currentUser && (
      <MainLayout>
        <UserForm
          title="Edit Profile"
          onSubmit={handleSubmit}
          initialValues={currentUser}
        />
      </MainLayout>
    )
  );
};

export default Profile;
