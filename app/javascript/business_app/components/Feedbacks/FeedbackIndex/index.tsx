import React from 'react';
import { Card } from 'antd';
import MarginGroup from '@/business/components/shared/MarginGroup';
import PageHeader from '@/flyd/PageHeader';

export const FeedbackIndex = () => {
  return (
    <MarginGroup>
      <PageHeader title="Feedbacks" />
      <Card className="text-center">
        <h1 className="text-3xl">Comming Soon</h1>
      </Card>
    </MarginGroup>
  );
};

export default FeedbackIndex;
