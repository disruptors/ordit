import React from 'react';
import MainLayout from '@/business/components/shared/layouts/MainLayout';
import FeedbackIndex from '@/business/components/Feedbacks/FeedbackIndex';

const Feedbacks = () => {
  return (
    <MainLayout>
      <FeedbackIndex />
    </MainLayout>
  );
};

export default Feedbacks;
