import NotFound from '@/business/components/NotFound';
import React, { FunctionComponent } from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import MainLayout from '../shared/layouts/MainLayout';
import UsersAdd from './UsersAdd';
import UsersEdit from './UsersEdit';
import UsersIndex from './UsersIndex';

const Users: FunctionComponent = () => {
  const match = useRouteMatch();

  return (
    <MainLayout>
      {/* Users Routes */}
      <Switch>
        <Route exact path={match.path} children={<UsersIndex />} />
        <Route exact path={`${match.path}/add`} children={<UsersAdd />} />
        <Route exact path={`${match.path}/:id/edit`} children={<UsersEdit />} />
        <Route children={<NotFound />} />
      </Switch>
    </MainLayout>
  );
};

export default Users;
