import ButtonLink from '@/business/components/shared/buttons/ButtonLink';
import MarginGroup from '@/business/components/shared/MarginGroup';
import SearchForm from '@/business/components/Users/UsersIndex/SearchForm';
import { User } from '@/schemas';
import { getUsersByIds } from '@/store/entities/user/selectors';
import { TransformedData } from '@/utils/ApiTransformer';
import usePushQuery from '@/utils/hooks/usePushQuery';
import useSearchList from '@/utils/hooks/useSearchList';
import { Card, Divider, PageHeader, Table, Button } from 'antd';
import { ColumnProps, TableProps } from 'antd/lib/table';
import qs from 'qs';
import React, { FunctionComponent } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link, useLocation, useRouteMatch } from 'react-router-dom';
import { sessionActions } from '@/store/session';
import history from '@/business/history';
import { StoreState } from '@/store';

const UsersIndex: FunctionComponent = () => {
  const match = useRouteMatch();
  const location = useLocation();
  const { list, isFetching, totalCount, perPage } = useSearchList(
    '/api/search?resource=user'
  );
  const pushQuery = usePushQuery();
  const users = useSelector(getUsersByIds(list));
  const business = useSelector((state: StoreState) => state.entities.business.byId);
  const dispatch = useDispatch();

  const query = qs.parse(location.search.slice(1));

  const handleTabChange: TableProps<typeof users[0]>['onChange'] = pagination => {
    pushQuery({ ...query, page: pagination.current });
  };

  const loginAsUser = (id: string) => {
    dispatch(sessionActions.become({ id, successCallback: () => history.push('/') }));
  };

  const columns: ColumnProps<TransformedData<Omit<User, 'password'>>>[] = [
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Business',
      dataIndex: 'business',
      key: 'business',
      render: (_, record) => <div>{business[record.currentBusinessId]?.name}</div>,
    },
    {
      title: 'Account Type',
      dataIndex: 'role',
      key: 'role',
    },
    {
      title: 'Created At',
      dataIndex: 'createdAt',
      key: 'createdAt',
    },
    {
      title: 'Action',
      dataIndex: 'action',
      key: 'action',
      render: (text, record) => (
        <>
          <Button onClick={() => loginAsUser(record.id)}>Login</Button>
          <Divider type="vertical" />
          <Link to={`/users/${record.id}/edit`}>Edit</Link>
          <Divider type="vertical" />
          <a>Remove</a>
        </>
      ),
    },
  ];

  return (
    <MarginGroup>
      <PageHeader
        title="Users"
        style={{ backgroundColor: 'white', border: '1px solid rgb(235, 237, 240)' }}
        extra={
          <>
            <ButtonLink
              type="primary"
              shape="circle"
              icon="user-add"
              to={`${match.url}/add`}
            />
          </>
        }
      >
        <SearchForm />
      </PageHeader>

      <Card>
        <Table
          loading={isFetching}
          columns={columns}
          rowKey={record => record.id}
          dataSource={users}
          scroll={{ x: true }}
          onChange={handleTabChange}
          pagination={{
            current: +query.page || 1,
            total: totalCount,
            pageSize: perPage,
            hideOnSinglePage: true,
          }}
        />
      </Card>
    </MarginGroup>
  );
};

export default UsersIndex;
