import UserForm from '@/business/components/Users/UserForm';
import { userAction, userSelectors } from '@/store/entities/user';
import React, { FunctionComponent, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

interface Params {
  id: string;
}

const UsersEdit: FunctionComponent = () => {
  const { id } = useParams<Params>();
  const dispatch = useDispatch();
  const user = useSelector(userSelectors.getUser(id));

  useEffect(() => {
    dispatch(userAction.fetchUser(id));
  }, [dispatch, id]);

  const handleSubmit: ExtractProps<typeof UserForm>['onSubmit'] = values => {
    dispatch(userAction.updateUser(id, values));
  };

  return <UserForm title="Edit User" onSubmit={handleSubmit} initialValues={user} />;
};

export default UsersEdit;
