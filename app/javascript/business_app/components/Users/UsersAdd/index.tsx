import UserForm from '@/business/components/Users/UserForm';
import { userAction } from '@/store/entities/user';
import React from 'react';
import { useDispatch } from 'react-redux';

const UsersAdd = () => {
  const dispatch = useDispatch();
  const handleSubmit: ExtractProps<typeof UserForm>['onSubmit'] = values => {
    dispatch(userAction.createUser(values));
  };

  return <UserForm title="Add User" onSubmit={handleSubmit} />;
};

export default UsersAdd;
