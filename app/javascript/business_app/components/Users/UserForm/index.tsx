import RestrictContent from '@/business/components/shared/auth/RestrictContent';
import AdminForm from '@/business/components/shared/forms/AdminForm';
import { Role, User } from '@/schemas';
import { Form } from '@ant-design/compatible';
import '@ant-design/compatible/assets/index.css';
import { Button, Input, Select } from 'antd';
import { FormComponentProps } from '@ant-design/compatible/lib/form';
import React, { FormEvent, FunctionComponent } from 'react';

interface FormValues extends Pick<User, 'name' | 'email' | 'password' | 'role'> {
  confirmPassword: string;
}

interface Props extends FormComponentProps<FormValues> {
  title: string;
  onSubmit: (values: FormValues) => void;
  initialValues?: Partial<FormValues>;
}

const UserForm: FunctionComponent<Props> = ({ form, title, onSubmit, initialValues }) => {
  const { getFieldDecorator, validateFieldsAndScroll } = form;

  const isEdit = !!initialValues;

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    validateFieldsAndScroll((err, values) => {
      if (!err) {
        onSubmit(values);
      }
    });
  };

  return (
    <AdminForm
      title={title}
      onSubmit={handleSubmit}
      actions={
        <Button type="primary" htmlType="submit">
          {isEdit ? 'Update' : 'Create'}
        </Button>
      }
    >
      <Form.Item label="Name">
        {getFieldDecorator('name', {
          rules: [],
          initialValue: initialValues?.name,
        })(<Input />)}
      </Form.Item>

      <Form.Item label="E-mail">
        {getFieldDecorator('email', {
          rules: [],
          initialValue: initialValues?.email,
        })(<Input />)}
      </Form.Item>

      <Form.Item label="Password">
        {getFieldDecorator('password', {
          rules: [],
        })(<Input.Password />)}
      </Form.Item>

      <RestrictContent allowed={[Role.Admin]}>
        <Form.Item label="Accunt Type">
          {getFieldDecorator('role', {
            rules: [],
            initialValue: initialValues?.role ?? undefined,
          })(
            <Select placeholder="Select Account Type" allowClear>
              <Select.Option value="">Regular</Select.Option>
              <Select.Option value="admin">Admin</Select.Option>
            </Select>
          )}
        </Form.Item>
      </RestrictContent>
    </AdminForm>
  );
};

export default Form.create<Props>({ name: 'userForm' })(UserForm);
