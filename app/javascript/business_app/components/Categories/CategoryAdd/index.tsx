import React, { FunctionComponent, SetStateAction, Dispatch, useState } from 'react';
import { Form } from '@ant-design/compatible';
import '@ant-design/compatible/assets/index.css';
import { Input, Modal } from 'antd';
import { useDispatch } from 'react-redux';
import { categoryActions } from '@/store/entities/category';
import usePushQuery from '@/utils/hooks/usePushQuery';
import qs from 'qs';
import { useLocation } from 'react-router-dom';

interface Props {
  businessId: string | null | undefined;
  categoriesName: Array<string>;
  visible: boolean;
  setVisible: Dispatch<SetStateAction<boolean>>;
  reload?: boolean;
}

const CategoryAdd: FunctionComponent<Props> = ({
  visible,
  setVisible,
  businessId,
  categoriesName,
  reload,
}) => {
  const { search } = useLocation();
  const pushQuery = usePushQuery();
  const dispatch = useDispatch();
  const [category, setCategory] = useState<string>('');
  const [error, setError] = useState<string | null>(null);

  const onOk = () => {
    if (category.length == 0) {
      setError('Category is required');
    } else if (categoriesName.includes(category.toLowerCase())) {
      setError('Category is already exist!');
    } else {
      dispatch(categoryActions.submitCategory({ name: category, businessId }));

      if (reload) {
        pushQuery({ ...qs.parse(search.slice(1)), new: new Date().getTime() });
      }

      setVisible(false);
    }
  };

  return (
    <Modal
      title="Add Category"
      closable={false}
      visible={visible}
      onOk={onOk}
      onCancel={() => setVisible(false)}
      width={300}
    >
      <Form.Item validateStatus={error === null ? '' : 'error'} help={error}>
        <Input
          value={category}
          style={{ width: '100%' }}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            setCategory(e.target.value)
          }
        />
      </Form.Item>
    </Modal>
  );
};

export default CategoryAdd;
