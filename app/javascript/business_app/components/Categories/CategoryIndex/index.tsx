import CategoryAdd from '@/business/components/Categories/CategoryAdd';
import MarginGroup from '@/business/components/shared/MarginGroup';
import { StoreState } from '@/store';
import { categoryActions } from '@/store/entities/category';
import { selectedCategories } from '@/store/entities/category/selectors';
import { getCurrentBusinessId } from '@/store/session/selectors';
import useSearchList from '@/utils/hooks/useSearchList';
import { trimDowncase } from '@/utils/Text';
import { SearchOutlined } from '@ant-design/icons';
import { Icon as LegacyIcon } from '@ant-design/compatible';
import { Button, notification, PageHeader, Popconfirm, Table, Typography } from 'antd';
import ButtonGroup from 'antd/lib/button/button-group';
import qs from 'qs';
import React, { FunctionComponent, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import SearchForm from './SearchForm';

const ActionButton = (_: any, { category, actions }) => {
  const { active, id: categoryId } = category;

  return (
    <ButtonGroup>
      <Button
        icon={<SearchOutlined />}
        onClick={() => actions.push(`/menu_items?category_id=${categoryId}`)}
      />
      <Popconfirm
        placement="bottomRight"
        title={`Are you sure you want to ${active ? 'Deactivate' : 'Activate'}`}
        onConfirm={() => actions.onConfirm(categoryId, !active)}
      >
        <Button
          icon={<LegacyIcon type={active ? 'close' : 'check'} />}
          type={active ? 'danger' : 'primary'}
        />
      </Popconfirm>
    </ButtonGroup>
  );
};

const ActionName = (_: any, { category, actions }) => {
  return (
    <>
      <Typography.Paragraph
        editable={{
          onChange: (name: string) => actions.onChangeCategoryName(category.id, name),
        }}
      >
        {category.name}
      </Typography.Paragraph>
    </>
  );
};

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    render: ActionName,
  },
  {
    title: 'Total Menu Items',
    dataIndex: 'totalMenuItems',
    key: 'totalMenuItems',
  },
  {
    title: 'Status',
    dataIndex: 'status',
    key: 'status',
  },
  {
    title: 'Action',
    dataIndex: 'action',
    key: 'action',
    render: ActionButton,
  },
];

const CategoriesIndex: FunctionComponent = () => {
  const { search } = useLocation();
  const { push } = useHistory();
  const dispatch = useDispatch();

  const currentBusinessId = useSelector(getCurrentBusinessId);
  const categories = useSelector(
    (state: StoreState) => state.entities.category.byId || {}
  );

  const [visibleModal, setVisibleModal] = useState<boolean>(false);

  // filter name and use in unique validations
  const categoriesName: string[] = Object.keys(categories).map((categoryId: string) =>
    trimDowncase(categories[categoryId].name)
  );

  const { list, isFetching } = useSearchList(
    `/api/search?resource=category&businessId=${currentBusinessId}`
  );

  const urlQuery = useMemo(() => qs.parse(search.slice(1)), [search]);
  const categoryIds = useSelector(
    selectedCategories(list, urlQuery?.name, urlQuery?.active)
  );

  const onConfirm = (categoryId: string | undefined, active: boolean) => {
    dispatch(categoryActions.submitCategory({ id: categoryId, active }));
  };

  const onChangeCategoryName = (categoryId: string, name: string) => {
    name = trimDowncase(name);

    let description: string = '';

    if (name.length == 0) {
      description = 'Category Name is required!';
    } else if (categoriesName.includes(name)) {
      if (trimDowncase(categories[categoryId].name) == name) {
        notification.info({ message: 'Category', description: 'No Changes' });
      } else {
        description = 'Category Name should be unique!';
      }
    } else {
      dispatch(categoryActions.submitCategory({ id: categoryId, name }));
    }

    if (description) {
      notification.error({ message: 'Update Failed', description });
    }
  };

  const dataSource = categoryIds.map((categoryId: string) => {
    const { name, totalMenuItems, active } = categories[categoryId];

    return {
      name,
      totalMenuItems,
      status: active ? 'Show' : 'Hide',
      category: categories[categoryId],
      actions: {
        onChangeCategoryName,
        onConfirm,
        push,
      },
    };
  });

  return (
    <MarginGroup>
      <PageHeader
        title="Category"
        style={{ backgroundColor: 'white', border: '1px solid rgb(235, 237, 240)' }}
        extra={
          <Button type="primary" onClick={() => setVisibleModal(!visibleModal)}>
            Add Category
          </Button>
        }
      >
        <SearchForm isFetching={isFetching} />
      </PageHeader>

      <Table
        loading={isFetching}
        rowKey="name"
        dataSource={dataSource}
        columns={columns}
        pagination={false}
      />

      <CategoryAdd
        businessId={currentBusinessId}
        categoriesName={categoriesName}
        visible={visibleModal}
        setVisible={setVisibleModal}
        reload
      />
    </MarginGroup>
  );
};

export default CategoriesIndex;
