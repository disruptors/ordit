import usePushQuery from '@/utils/hooks/usePushQuery';
import { SearchOutlined } from '@ant-design/icons';
import { Form } from '@ant-design/compatible';
import '@ant-design/compatible/assets/index.css';
import { Button, Input, Select } from 'antd';
import { FormComponentProps } from '@ant-design/compatible/lib/form';
import qs from 'qs';
import React, { FormEvent, FunctionComponent } from 'react';
import { useLocation } from 'react-router-dom';

interface Props extends FormComponentProps<{}> {
  isFetching: boolean;
}

const SearchForm: FunctionComponent<Props> = ({ form, isFetching }) => {
  const pushQuery = usePushQuery();
  const { search } = useLocation();

  const { validateFieldsAndScroll, getFieldDecorator } = form;
  const query = qs.parse(search.slice(1));

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    validateFieldsAndScroll((err, values) => {
      if (!err) pushQuery(values);
    });
  };

  return (
    <Form layout="inline" onSubmit={handleSubmit}>
      <Form.Item>
        {getFieldDecorator('name', {
          initialValue: query.name,
        })(<Input placeholder="Category Name" />)}
      </Form.Item>

      <Form.Item>
        {getFieldDecorator('active', {
          initialValue: query.active,
        })(
          <Select placeholder="Active" style={{ width: 150 }} allowClear>
            <Select.Option value="active">Active</Select.Option>
            <Select.Option value="inactive">Inactive</Select.Option>
          </Select>
        )}
      </Form.Item>

      <Form.Item>
        <Button loading={isFetching} icon={<SearchOutlined />} htmlType="submit">
          Search
        </Button>
      </Form.Item>
    </Form>
  );
};

export default Form.create<Props>()(SearchForm);
