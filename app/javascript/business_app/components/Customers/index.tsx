import CustomerIndex from '@/business/components/Customers/CustomerIndex';
import MainLayout from '@/business/components/shared/layouts/MainLayout';
import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

const Customers: React.FC = () => {
  const match = useRouteMatch();

  return (
    <MainLayout>
      <Switch>
        <Route exact path={match.path} children={<CustomerIndex />} />
      </Switch>
    </MainLayout>
  );
};

export default Customers;
