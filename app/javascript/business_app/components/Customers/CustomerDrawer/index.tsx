import { QrCode } from '@/schemas';
import { StoreState } from '@/store';
import { qrCodeAction } from '@/store/entities/qrCode';
import { sittingAction } from '@/store/entities/sitting';
import { getSitting } from '@/store/entities/sitting/selectors';
import useMediaQuery from '@/utils/hooks/useMediaQuery';
import { Button, Drawer, List, Popconfirm, Col, Row } from 'antd';
import { DrawerProps } from 'antd/lib/drawer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import PriceDisplay from '@/customer/components/shared/PriceDisplay';
import Empty from '@/flyd/Empty';
import SpinOrEmpty from '@/flyd/SpinOrEmpty';

const DRAWER_WIDTH = 640;

interface Props extends DrawerProps {
  qrCode: QrCode;
}

const DrawerBody = styled.div`
  .total-section {
    margin-top: 20px;
  }
`;

const DrawerFooter = styled.div`
  margin: 40px 0px;
`;

const CustomerDrawer: React.FC<Props> = ({ qrCode, visible, onClose }) => {
  const { currentSittingId } = qrCode;
  const dispatch = useDispatch();
  const sitting = useSelector(getSitting(currentSittingId));
  const orders = useSelector((state: StoreState) => state.entities.orders);
  const matchesDrawerWidth = useMediaQuery(`(max-width: ${DRAWER_WIDTH}px)`);
  const isFetching = useSelector((state: StoreState) => state.loaders.FETCH_SITTING);

  useEffect(() => {
    if (currentSittingId) {
      dispatch(sittingAction.fetchSitting({ id: currentSittingId }));
    }
  }, [dispatch, currentSittingId]);

  const clearSitting = () => {
    dispatch(qrCodeAction.callService('clear_table', { id: qrCode.id }));
  };

  return (
    <Drawer
      width={matchesDrawerWidth ? '100%' : DRAWER_WIDTH}
      destroyOnClose
      onClose={onClose}
      placement="right"
      closable
      visible={visible}
    >
      {!!qrCode && <h1>{qrCode.name}</h1>}
      <SpinOrEmpty
        empty={!sitting}
        fetching={isFetching}
        renderEmpty={<Empty description="Table is Empty" />}
      >
        {sitting && (
          <>
            <DrawerBody>
              <List
                dataSource={sitting.orders}
                renderItem={orderId => (
                  <List.Item>
                    <Row style={{ width: '100%' }}>
                      <Col span={12}>
                        {`${orders.byId[orderId]?.quantity} X ${orders.byId[orderId]?.name}`}
                      </Col>
                      <Col span={12}>
                        <PriceDisplay value={orders.byId[orderId]?.amount} />
                      </Col>
                    </Row>
                  </List.Item>
                )}
              />
              <Row className="total-section">
                <Col span={12}>Total:</Col>
                <Col span={12}>
                  <strong>
                    <PriceDisplay value={sitting.totalOrderAmount} />
                  </strong>
                </Col>
              </Row>
            </DrawerBody>
            <DrawerFooter>
              <Popconfirm
                title="Are you sure to clear this table?"
                onConfirm={clearSitting}
                okText="Yes"
                cancelText="No"
              >
                <Button type="primary" size="large">
                  Clear Table
                </Button>
              </Popconfirm>
            </DrawerFooter>
          </>
        )}
      </SpinOrEmpty>
    </Drawer>
  );
};

export default CustomerDrawer;
