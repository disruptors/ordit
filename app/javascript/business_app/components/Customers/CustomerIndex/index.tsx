import MarginGroup from '@/business/components/shared/MarginGroup';
import { qrCodeAction } from '@/store/entities/qrCode';
import { UserOutlined } from '@ant-design/icons';
import { Card, Table, Avatar } from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import CustomerDrawer from '@/business/components/Customers/CustomerDrawer';
import { QrCode } from '@/schemas';
import { getQrCodes } from '@/store/entities/qrCode/selectors';
import _ from 'lodash';
import styled from 'styled-components';
import PageHeader from '@/flyd/PageHeader';

const TableContainer = styled(Card)`
  .empty {
    background: #f7f7f7;
  }
`;
const CustomerIndex: React.FC = () => {
  const dispatch = useDispatch();
  const [drawerVisible, setDrawerVisible] = useState(false);
  const [drawerSitting, setDrawerOrder] = useState<QrCode | null>(null);
  const tables = useSelector(getQrCodes);

  useEffect(() => {
    const query = {
      includes: 'customers',
    };

    dispatch(qrCodeAction.fetchQrCodes(query));
  }, [dispatch]);

  const columns = [
    {
      title: 'Table #',
      dataIndex: 'name',
      key: 'id',
      width: 150,
    },
    {
      title: 'Customers',
      dataIndex: 'customerCount',
      width: 300,
      render: (customerCount: number) => {
        const users = [...Array(customerCount)].map((_, i) => {
          return (
            <Avatar
              icon={<UserOutlined />}
              key={i}
              style={{ backgroundColor: '#87d068' }}
            />
          );
        });

        return <div>{users}</div>;
      },
    },
    {
      title: '',
      dataIndex: 'status',
    },
  ];

  const dataSource = _.sortBy(tables, ['currentSittingId'], ['desc']).map(
    (table: QrCode) => {
      return {
        customerCount: table.sittingCustomersCount,
        ...table,
      };
    }
  );

  const showDrawer = (record: QrCode) => {
    setDrawerOrder(record);
    setDrawerVisible(true);
  };

  const hideDrawer = () => {
    setDrawerVisible(false);
  };

  return (
    <MarginGroup>
      <PageHeader title="Customers" />
      <TableContainer>
        <Table
          rowKey="id"
          dataSource={dataSource}
          columns={columns}
          rowClassName={record => (record.currentSittingId ? 'occupied' : 'empty')}
          onRow={record => {
            return {
              onClick: () => {
                showDrawer(record);
              },
            };
          }}
        />
        {drawerSitting && (
          <CustomerDrawer
            qrCode={drawerSitting}
            onClose={hideDrawer}
            visible={drawerVisible}
          />
        )}
      </TableContainer>
    </MarginGroup>
  );
};

export default CustomerIndex;
