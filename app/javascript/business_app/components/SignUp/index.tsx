import React from 'react';
import AuthLayout from '../shared/layouts/AuthLayout';
import RegistrationForm from './RegistrationForm';

const SignUp = () => (
  <AuthLayout title="Sign Up">
    <RegistrationForm />
  </AuthLayout>
);

export default SignUp;
