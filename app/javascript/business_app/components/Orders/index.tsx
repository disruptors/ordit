import OrderIndex from '@/business/components/Orders/OrderIndex';
import MainLayout from '@/business/components/shared/layouts/MainLayout';
import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

const Orders: React.FC = () => {
  const match = useRouteMatch();

  return (
    <MainLayout>
      <Switch>
        <Route exact path={match.path} children={<OrderIndex />} />
      </Switch>
    </MainLayout>
  );
};

export default Orders;
