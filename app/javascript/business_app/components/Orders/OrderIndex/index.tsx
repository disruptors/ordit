import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { orderAction } from '@/store/entities/order';
import {
  CheckCircleOutlined,
  DownOutlined,
  HourglassOutlined,
  StopOutlined,
} from '@ant-design/icons';
import { Table, Card, Dropdown, Menu, Button } from 'antd';
import MarginGroup from '@/business/components/shared/MarginGroup';
import { getOrders } from '@/store/entities/order/selectors';
import * as _ from 'lodash';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import { Order } from '@/schemas';
import styled from 'styled-components';
import { StoreState } from '@/store';
import PageHeader from '@/flyd/PageHeader';
import Tip from '@/flyd/Tip';
import Empty from '@/flyd/Empty';
import SpinOrEmpty from '@/flyd/SpinOrEmpty';

dayjs.extend(relativeTime);

const OrdersContainer = styled.div`
  .order-table {
    margin-bottom: 40px;

    .table-name {
      margin: 0px;
    }

    .ordt-tip {
      margin-bottom: 10px;
    }
  }

  .PENDING {
    border-color: #faad14;
    color: #faad14;
  }
  .SERVED {
    border-color: #52c41a;
    color: #52c41a;
  }
`;

const OrderIndex: React.FC = () => {
  const dispatch = useDispatch();
  const orders = useSelector(getOrders);
  const isFetching = useSelector((state: StoreState) => state.loaders.FETCH_ORDERS);

  useEffect(() => {
    dispatch(orderAction.fetchOrders());
  }, [dispatch]);

  const dataSource = (table: string) => {
    return _.sortBy(tables[table], o => o.createdAt)
      .reverse()
      .map(order => order);
  };

  const tables = _.groupBy(orders, order => order.table);

  const columns = [
    {
      title: 'Qty',
      dataIndex: 'quantity',
      key: 'quantity',
      width: 100,
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      width: 200,
    },
    {
      title: 'Submitted',
      dataIndex: 'createdAt',
      key: 'createdAt',
      render: (createdAt: string) => {
        return dayjs(createdAt).fromNow();
      },
    },
    {
      title: 'Action',
      align: 'right' as 'right',
      dataIndex: 'id',
      key: 'id',
      render: (id: string) => {
        const order = orders.find(order => order.id === id);
        return (
          <div>
            {order && (
              <div>
                <Dropdown overlay={menu(order)} className={order.orderStatus}>
                  <Button>
                    <span>{_.capitalize(order.orderStatus)}</span>
                    <DownOutlined />
                  </Button>
                </Dropdown>
              </div>
            )}
          </div>
        );
      },
    },
  ];

  const updateOrder = (order: Order, status: string) => {
    dispatch(orderAction.submitOrder({ id: order.id, orderStatus: status }));
  };

  const menu = (order: Order) => (
    <Menu>
      <Menu.Item key="1" onClick={() => updateOrder(order, 'SERVED')}>
        <CheckCircleOutlined />
        Serve
      </Menu.Item>
      <Menu.Item key="2" onClick={() => updateOrder(order, 'CANCELLED')}>
        <StopOutlined />
        Cancel
      </Menu.Item>
      <Menu.Item key="3" onClick={() => updateOrder(order, 'PENDING')}>
        <HourglassOutlined />
        Pending
      </Menu.Item>
    </Menu>
  );

  const tableIds = Object.keys(tables);
  return (
    <OrdersContainer>
      <MarginGroup>
        <PageHeader title="Orders" />
        <Card>
          <SpinOrEmpty
            fetching={isFetching}
            empty={tableIds.length === 0}
            renderEmpty={<Empty description="No Orders Yet." />}
          >
            {tableIds.map(table => (
              <div key={table} className="order-table">
                <Tip>
                  <h3 className="table-name">{`Table ${table}`}</h3>
                </Tip>
                <Table
                  className="mt-2"
                  pagination={false}
                  rowKey="id"
                  tableLayout="fixed"
                  dataSource={dataSource(table)}
                  columns={columns}
                />
              </div>
            ))}
          </SpinOrEmpty>
        </Card>
      </MarginGroup>
    </OrdersContainer>
  );
};

export default OrderIndex;
