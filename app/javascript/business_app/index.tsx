import { MainLayoutProvider } from '@/business/components/shared/layouts/MainLayout';
import configureStore from '@/store/configureStore';
import 'antd/dist/antd.css';
import React from 'react';
import ReactDOM from 'react-dom';
//Redux Provider
import { Provider } from 'react-redux';
import App from './App';
import { ThemeProvider } from 'styled-components';
import OrditTheme from '@/utils/OrditTheme';

const store = configureStore({});

document.addEventListener('DOMContentLoaded', () => {
  const rootElement = document.getElementById('root');

  ReactDOM.render(
    <Provider store={store}>
      <MainLayoutProvider>
        <ThemeProvider theme={OrditTheme}>
          <App />
        </ThemeProvider>
      </MainLayoutProvider>
    </Provider>,
    rootElement
  );
});
