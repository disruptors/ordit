import ResourceApi from 'api/resourceApi';

class Customers extends ResourceApi {
  constructor() {
    super('customers');
  }
}
export default new Customers();
