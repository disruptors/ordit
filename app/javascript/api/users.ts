import ResourceApi from 'api/resourceApi';

class Users extends ResourceApi {
  constructor() {
    super('users');
  }
}

export default new Users();
