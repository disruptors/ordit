import ApiClient from '@/utils/ApiClient';

abstract class ResourceApi {
  url: string;
  constructor(private name: string) {
    this.url = `/api/${this.name}`;
  }

  getAll = (query?: any) => {
    return ApiClient.get(`/api/${this.name}`, query);
  };

  getById = (id: string) => {
    return ApiClient.get(`/api/${this.name}/${id}`);
  };

  create = (data: any) => {
    return ApiClient.post(`/api/${this.name}`, data);
  };

  update = (id: string, data: any) => {
    return ApiClient.patch(`/api/${this.name}/${id}`, data);
  };

  callService(name: string, data: any) {
    return ApiClient.post(`/api/${this.name}/service/${name}`, data);
  }

  get http() {
    return ApiClient;
  }
}

export default ResourceApi;
