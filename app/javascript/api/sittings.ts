import ResourceApi from 'api/resourceApi';

class Sittings extends ResourceApi {
  constructor() {
    super('sittings');
  }
}

export default new Sittings();
