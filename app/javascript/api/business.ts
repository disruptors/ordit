import ResourceApi from 'api/resourceApi';

class Businesses extends ResourceApi {
  constructor() {
    super('businesses');
  }
}

export default new Businesses();
