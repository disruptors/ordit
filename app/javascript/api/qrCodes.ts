import ResourceApi from './resourceApi';

class QrCodes extends ResourceApi {
  constructor() {
    super('qr_codes');
  }
}

export default new QrCodes();
