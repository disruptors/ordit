import ResourceApi from './resourceApi';

class Needs extends ResourceApi {
  constructor() {
    super('needs');
  }
}

export default new Needs();
