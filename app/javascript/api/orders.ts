import ResourceApi from 'api/resourceApi';

class Orders extends ResourceApi {
  constructor() {
    super('orders');
  }
}
export default new Orders();
