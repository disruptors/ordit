import ResourceApi from './resourceApi';

class Sessions extends ResourceApi {
  constructor() {
    super('auth');
  }

  become = (id: string) => {
    return this.http.post(`${this.url}/become`, { id });
  };
}

export default new Sessions();
