import styled from 'styled-components';

interface Props {
  size?: string;
}

const Label = styled.label<Props>`
  text-transform: uppercase !important;
  letter-spacing: 0.025em !important;
  font-size: ${props => props.size || '0.75rem'};
  font-weight: 700 !important;
  color: ${props => props.theme.textColorSecondary};
`;

export default Label;
