import React from 'react';
import { Spin } from 'antd';

interface Props {
  fetching: boolean;
  empty: boolean;
  renderEmpty: React.ReactNode;
}

const SpinOrEmpty: React.FC<Props> = ({
  children,
  fetching,
  empty = true,
  renderEmpty,
}) => {
  return <Spin spinning={!!fetching}>{empty && !fetching ? renderEmpty : children}</Spin>;
};

export default SpinOrEmpty;
