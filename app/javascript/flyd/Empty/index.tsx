import ButtonLink from '@/business/components/shared/buttons/ButtonLink';
import { Empty as AntdEmpty } from 'antd';
import { EmptyProps } from 'antd/lib/empty';
import React, { FC } from 'react';
import EmptyImg from '../../assets/images/empty.png';

interface Props extends EmptyProps {
  ctaLink?: string;
  ctaText?: string;
  description?: string;
}

const Empty: FC<Props> = ({
  ctaLink,
  ctaText,
  style,
  description = 'Empty',
  ...rest
}) => (
  <AntdEmpty
    image={EmptyImg}
    imageStyle={{ height: 60 }}
    description={<span>{description}</span>}
    style={{ marginTop: '50px', ...style }}
    {...rest}
  >
    {ctaLink && (
      <ButtonLink type="primary" to={ctaLink}>
        {ctaText}
      </ButtonLink>
    )}
  </AntdEmpty>
);

export default Empty;
