import React from 'react';
import { PageHeader as AntdPageheader } from 'antd';
import { PageHeaderProps } from 'antd/lib/page-header';
import styled from 'styled-components';

interface Props extends PageHeaderProps {
  light?: boolean;
}

const StyledPageHeader = styled(AntdPageheader)<Props>`
  ${props => (props.light ? 'background-color: white;' : '')}
  border: 1px solid rgb(235, 237, 240);
`;

const PageHeader: React.FC<Props> = ({ light = true, children, ...rest }) => {
  return (
    <StyledPageHeader light={light} {...rest}>
      {children}
    </StyledPageHeader>
  );
};

export default PageHeader;
