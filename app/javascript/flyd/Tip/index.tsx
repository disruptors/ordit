import React from 'react';
import styled from 'styled-components';

interface TipProps {
  type?: 'primary' | 'warning' | 'danger' | 'success'; //default to primary
  children: any;
}

const TipContainer = styled.div`
  border-left: solid;
`;

const Tip: React.FC<TipProps> = ({ type = 'primary', children, ...rest }) => {
  const typeMap = {
    primary: 'blue',
    warning: 'yellow',
    danger: 'red',
    success: 'green',
  };

  return (
    <TipContainer
      className={`bg-${typeMap[type]}-100 border-l-4 border-${typeMap[type]}-500 text-${typeMap[type]}-700 p-4`}
      {...rest}
    >
      {children}
    </TipContainer>
  );
};

export default Tip;
