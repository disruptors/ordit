import History from '@/customer/history';
import configureStore from '@/store/configureStore';
import 'antd-mobile/dist/antd-mobile.css';
import 'antd/dist/antd.css';
import 'assets/styles.css';
import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
//Redux Provider
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import App from './App';

const store = configureStore({});

document.addEventListener('DOMContentLoaded', () => {
  const rootElement = document.getElementById('root');

  ReactDOM.render(
    <Provider store={store}>
      <Suspense fallback={null}>
        <Router history={History}>
          <App />
        </Router>
      </Suspense>
    </Provider>,
    rootElement
  );
});
