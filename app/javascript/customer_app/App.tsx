import SlideMotion from '@/customer/components/shared/Animate/SlideMotion';
import NavLayout from '@/customer/components/shared/layouts/NavLayout';
import StoreLayout from '@/customer/components/shared/layouts/StoreLayout';
import GuestRoute from '@/customer/components/shared/routes/GuestRoute';
import PrivateRoute from '@/customer/components/shared/routes/PrivatetRoute';
import { sessionActions } from '@/store/session';
import { AnimatePresence } from 'framer-motion';
import React, { lazy, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Route, Switch, useLocation } from 'react-router-dom';

const EnterPin = lazy(() => import('@/customer/components/EnterPin'));
const MenuItems = lazy(() => import('@/customer/components/MenuItems'));
const MenuItemShow = lazy(() => import('@/customer/components/MenuItems/MenuItemShow'));
const Checkout = lazy(() => import('@/customer/components/Checkout'));
const OrderIndex = lazy(() => import('@/customer/components/Orders/OrderIndex'));
const NotFound = lazy(() => import('@/business/components/NotFound'));

/**
 * TODO:
 * Adding layout in PrivateRoute would create
 * own layout per page.
 */

const App = () => {
  const dispatch = useDispatch();
  const location = useLocation();

  useEffect(() => {
    dispatch(sessionActions.validateCustomer());
  }, [dispatch]);

  return (
    <AnimatePresence exitBeforeEnter>
      <SlideMotion key={location.pathname}>
        <Switch location={location}>
          <GuestRoute exact path={['/auths/new']} children={<EnterPin />} />
          <PrivateRoute
            exact
            path={['/store/:businessId', '/store/:businessId/menu_items']}
            children={<MenuItems />}
            layout={StoreLayout}
          />
          <PrivateRoute
            exact
            path="/store/:businessId/menu_items/:menuItemId"
            children={<MenuItemShow />}
            layout={NavLayout}
          />
          <PrivateRoute
            exact
            path="/store/:businessId/checkout"
            children={<Checkout />}
            layout={NavLayout}
          />
          <PrivateRoute
            exact
            path="/store/:businessId/orders"
            children={<OrderIndex />}
            layout={StoreLayout}
          />

          <Route children={<NotFound />} />
        </Switch>
      </SlideMotion>
    </AnimatePresence>
  );
};

export default App;
