import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Button, Row, Col } from 'antd';
import { sittingAction } from '@/store/entities/sitting';
import { qrCodeAction } from '@/store/pages/qrCode';
import { getCurrentCustomer } from '@/store/session/selectors';
import { getQrCode } from '@/store/entities/qrCode/selectors';
import styled from 'styled-components';
import OrderList from '@/customer/components/shared/OrderList';
import TotalOrders from '@/customer/components/shared/TotalOrders';
import useCartStorage, { CartItemType } from '@/utils/hooks/useCartStorage';
import history from '@/customer/history';

const CheckoutContainer = styled.div`
  display: block;
  .ant-list-items {
    background-color: white;
  }
  .ctn-footer {
    position: fixed;
    width: 100%;
    bottom: 0px;
    padding: 20px;
  }
  .item-row {
    padding: 20px;
    width: 100%;
    text-align: center;
  }

  .item-price {
    text-align: right;
  }
  .item-name {
    text-align: left;
  }
  .item-qty {
    text-align: left;
  }

  .total-ctn {
    width: 100%;
  }
`;

const Checkout = () => {
  const dispatch = useDispatch();
  const currentCustomer = useSelector(getCurrentCustomer);
  const qrCode = useSelector(getQrCode(currentCustomer?.qrCode));
  const [cart, { addItem, clearCart }] = useCartStorage();

  useEffect(() => {
    const qrCodeId = currentCustomer?.qrCode;
    dispatch(qrCodeAction.fetchQrCode(qrCodeId));
  }, [dispatch, currentCustomer]);

  const updateOrders = (item: any, value: number) => {
    item.quantity = value;

    addItem(item);
  };

  const submitOrder = () => {
    const sitting = {
      id: qrCode?.currentSittingId,
      qrCodeId: qrCode.id,
      customerIds: [currentCustomer?.id],
      businessId: currentCustomer?.business,
      ordersAttributes: cart.items.map((item: CartItemType) => ({
        amount: item.price,
        qrCodeId: qrCode.id,
        menuItemId: item.id,
        customerId: currentCustomer?.id,
        quantity: item.quantity,
      })),
      customersAttributes: [currentCustomer],
    };

    const options = {
      successMessage: 'Orders Submitted',
    };

    dispatch(
      sittingAction.submitSitting({
        sitting,
        successCallback: () => {
          clearCart();
          history.push(`/store/${currentCustomer?.business}/orders`);
        },
        options,
      })
    );
  };

  return (
    <CheckoutContainer>
      <OrderList items={cart.items} onOrderChange={updateOrders} />
      <TotalOrders items={cart.items} />
      {!!cart.items.length && (
        <Row className="ctn-footer">
          <Col span={24}>
            <Button
              onClick={submitOrder}
              block
              className="checkout-btn"
              size="large"
              type="primary"
            >
              Submit Order
            </Button>
          </Col>
        </Row>
      )}
    </CheckoutContainer>
  );
};

export default Checkout;
