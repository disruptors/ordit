import { orderAction } from '@/store/entities/order';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import OrderList from '@/customer/components/shared/OrderList';
import { getOrders } from '@/store/entities/order/selectors';
import { Order } from '@/schemas';
import TotalOrders from '@/customer/components/shared/TotalOrders';
import styled from 'styled-components';
import { Button } from 'antd-mobile';
import ScreenFooter from '@/customer/components/shared/ScreenFooter';
import history from '@/customer/history';
import { useParams } from 'react-router-dom';

const OrderIndexContainer = styled.div`
  display: block;
`;
const OrderIndexHeader = styled.div`
  text-align: center;
`;

const OrderIndex = () => {
  const dispatch = useDispatch();
  const orders = useSelector(getOrders);
  const { businessId } = useParams();

  useEffect(() => {
    dispatch(orderAction.fetchOrders());
  }, [dispatch]);

  const orderItems = orders.map((order: Order) => ({
    id: order.id,
    quantity: order.quantity,
    name: order.name,
    price: order.amount,
  }));

  return (
    <OrderIndexContainer>
      <OrderIndexHeader>
        <h1 className="m-0 pt-4">Your Orders</h1>
        <p className="mb-4">We are now preparing your food.</p>
      </OrderIndexHeader>
      <OrderList items={orderItems} />
      <TotalOrders items={orderItems} />
      <ScreenFooter>
        <Button
          className="submit-btn m-3"
          type="primary"
          size="large"
          onClick={() => history.push(`/store/${businessId}/menu_items`)}
        >
          Continue to menu
        </Button>
      </ScreenFooter>
    </OrderIndexContainer>
  );
};

export default OrderIndex;
