import MenuItemIndex from '@/customer/components/MenuItems/MenuItemIndex';
import React, { FunctionComponent } from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

const MenuItems: FunctionComponent = () => {
  const match = useRouteMatch();
  return (
    <Switch>
      <Route exact path={`${match.path}`} children={<MenuItemIndex />} />
    </Switch>
  );
};

export default MenuItems;
