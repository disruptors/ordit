import React from 'react';
import { List } from 'antd-mobile';
import { Row, Col, Typography } from 'antd';
import PriceDisplay from '@/customer/components/shared/PriceDisplay';
import history from '@/customer/history';
import styled from 'styled-components';
const { Title } = Typography;

interface Props {
  items: any[];
  businessId: string;
}

const ListContainer = styled(List)`
  .am-list-item img {
    width: 100% !important;
    height: 90px !important;
    object-fit: cover;
  }
`;

const MenuItemList: React.FC<Props> = ({ items = [], businessId }) => {
  return (
    <ListContainer>
      {items.map(item => (
        <List.Item
          wrap
          key={item.id}
          onClick={() => history.push(`/store/${businessId}/menu_items/${item.id}`)}
        >
          <Row style={{ width: '100%', padding: '0px 10px' }} gutter={16}>
            <Col span={8}>
              <img src={item.image} />
            </Col>
            <Col span={16}>
              <Title level={4}>{item.title}</Title>
              <PriceDisplay value={item.price} />
            </Col>
          </Row>
        </List.Item>
      ))}
    </ListContainer>
  );
};

export default MenuItemList;
