import OrderStatus from '@/customer/components/MenuItems/OrderStatus';
import CallWaiterButton from '@/customer/components/shared/CallWaiterButton';
import Cart from '@/customer/components/shared/Cart';
import { businessActions } from '@/store/entities/business';
import { menuItemAction, menuItemSelectors } from '@/store/entities/menuItem';
import { StoreState } from '@/store/index';
import { getCurrentCustomerId } from '@/store/session/selectors';
import { Avatar, Layout, Row } from 'antd';
import React, { useEffect, FC } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import styled from 'styled-components';
import { Tabs } from 'antd-mobile';
import _ from 'lodash';
import MenuItemList from '@/customer/components/MenuItems/MenuItemIndex/MenuItemList';
import BillOutButton from '@/customer/components/MenuItems/MenuItemIndex/BillOutButton';

const { Content } = Layout;

const BusinessSection = styled.div`
  background-color: #fff;
  text-align: center;
  padding: 20px 0px 20px 0px;
`;

const ListContainer = styled.div`
  .menu-header {
    padding: 10px 20px 10px 20px;
    color: #888;
    display: block;
    font-size: 16px;
    font-weight: 600;
  }
  .menu-list {
    background-color: #fff;
  }
`;

const FooterSection = styled.div`
  margin: 30px 20px 80px 20px;
`;

interface Params {
  businessId: string;
}

const MenuItemIndex: FC = () => {
  const { businessId } = useParams<Params>();
  const dispatch = useDispatch();
  const business = useSelector(
    (state: StoreState) => state.entities.business.byId[businessId]
  );
  const category = useSelector((state: StoreState) => state.entities.category);
  const menuItems = useSelector(menuItemSelectors.getBusinessMenuItems(businessId));
  const customerId = useSelector(getCurrentCustomerId);

  useEffect(() => {
    dispatch(businessActions.fetchBusiness(businessId));
  }, [dispatch, businessId]);

  useEffect(() => {
    dispatch(menuItemAction.fetchMenuItems());
  }, [dispatch]);

  const items = _.groupBy(menuItems, item => item.category);

  const itemCategories = Object.keys(items);

  const tabs = itemCategories.map(categoryId => ({
    title: (category.byId[categoryId] || { name: 'Others' }).name,
  }));

  const dataSource = (categoryId: string) => {
    return items[categoryId].map(item => ({
      id: item.id,
      title: item.name,
      price: item.price,
      image: item.currentImage || '',
    }));
  };

  return (
    <Content>
      <BusinessSection>
        <Avatar src={business?.avatar} size="large" />
        <h1 style={{ textAlign: 'center' }}>{business?.name}</h1>
      </BusinessSection>
      <div>
        {!!customerId && <OrderStatus customerId={customerId} businessId={businessId} />}
      </div>
      <ListContainer>
        <div>
          <span className="menu-header">Menu</span>
        </div>
        <Tabs tabs={tabs}>
          {itemCategories.map(categoryId => (
            <MenuItemList
              key={categoryId}
              items={dataSource(categoryId)}
              businessId={businessId}
            />
          ))}
        </Tabs>
      </ListContainer>
      <FooterSection>
        {!!customerId && (
          <BillOutButton
            className="mb-2"
            businessId={businessId}
            customerId={customerId}
          />
        )}
        <CallWaiterButton businessId={businessId} customerId={customerId} />
      </FooterSection>
      <Row>
        <Cart businessId={businessId} />
      </Row>
    </Content>
  );
};

export default MenuItemIndex;
