import React from 'react';
import { Toast, Button } from 'antd-mobile';
import { ButtonProps } from 'antd-mobile/lib/button';
import { useDispatch } from 'react-redux';
import { needAction } from '@/store/entities/need';

interface Props extends ButtonProps {
  businessId: string;
  customerId: string;
}

const BillOutButton: React.FC<Props> = ({ businessId, customerId, ...rest }) => {
  const dispatch = useDispatch();
  const billOut = () => {
    const params = {
      need: {
        name: 'Bill Out',
        customerId,
        businessId,
      },
      successCallback: () => {
        Toast.success('Waiter is on the way.');
      },
    };

    dispatch(needAction.createNeed(params));
  };

  return (
    <Button onClick={billOut} {...rest}>
      Bill Out
    </Button>
  );
};

export default BillOutButton;
