import React, { useEffect } from 'react';
import { orderAction } from '@/store/entities/order';
import { useDispatch, useSelector } from 'react-redux';
import { Card } from 'antd';
import { Link } from 'react-router-dom';
import { StoreState } from '@/store';

interface Props {
  customerId: string;
  businessId: string;
}

const OrderStatus: React.FC<Props> = ({ customerId, businessId }) => {
  const dispatch = useDispatch();
  const orders = useSelector((state: StoreState) => state.entities.orders);

  useEffect(() => {
    const query = { customerId };
    dispatch(orderAction.fetchOrders(query));
  }, [dispatch, customerId]);

  if (!!orders.all.length) {
    return (
      <Card style={{ textAlign: 'center', margin: '20px' }}>
        <p>Orders are being prepared.</p>
        <Link to={`/store/${businessId}/orders`}>View Orders</Link>
      </Card>
    );
  }
  return null;
};

export default OrderStatus;
