import PriceDisplay from '@/customer/components/shared/PriceDisplay';
import history from '@/customer/history';
import { StoreState } from '@/store';
import { menuItemAction } from '@/store/entities/menuItem';
import useCartStorage from '@/utils/hooks/useCartStorage';
import {
  Button,
  Carousel,
  Flex,
  Stepper,
  WingBlank,
  Card,
  WhiteSpace,
} from 'antd-mobile';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import ScreenFooter from '../../shared/ScreenFooter';

interface Params {
  menuItemId: string;
  businessId: string;
}

const MenuItemShow: React.FC = () => {
  const { menuItemId, businessId } = useParams<Params>();
  const dispatch = useDispatch();
  const [cart, { addItem, getOrder }] = useCartStorage(); //eslint-disable-line @typescript-eslint/no-unused-vars
  const [quantity, setQuantity] = useState(1);
  const menuItem = useSelector(
    (state: StoreState) => state.entities.menuItems.byId[menuItemId]
  );

  useEffect(() => {
    dispatch(menuItemAction.fetchMenuItem(menuItemId));
  }, [dispatch, menuItemId]);

  const addToCart = (menuItem: any) => {
    const order = {
      quantity,
      price: parseInt(menuItem.price),
      id: menuItem.id,
      name: menuItem.name,
    };
    const existingOrder = getOrder(menuItem.id);
    if (existingOrder) {
      order.quantity += existingOrder.quantity;
    }

    addItem(order);
    history.push(`/store/${businessId}/menu_items`);
  };

  const onChangeQty = (qty: any) => {
    setQuantity(parseInt(qty));
  };

  return (
    <>
      {!!menuItem && (
        <div>
          <Carousel autoplay={false} infinite>
            {[menuItem.currentImage || ''].map((image: string) => (
              <img
                key={image}
                src={image}
                alt=""
                style={{ width: '100%', verticalAlign: 'top' }}
                onLoad={() => {
                  window.dispatchEvent(new Event('resize'));
                }}
              />
            ))}
          </Carousel>
          <Card full>
            <WingBlank size="lg">
              <h1>{menuItem.name}</h1>
              <strong>
                <PriceDisplay value={menuItem.price} />
              </strong>
            </WingBlank>
          </Card>
          <WingBlank size="sm" />
          <ScreenFooter>
            <Card full>
              <Flex justify="center">
                <Stepper
                  className="mt-8"
                  showNumber
                  max={50}
                  min={0}
                  defaultValue={1}
                  onChange={onChangeQty}
                />
              </Flex>
            </Card>
            <WhiteSpace />
            <WingBlank>
              <Button
                className="submit-btn"
                type="primary"
                size="large"
                onClick={() => addToCart(menuItem)}
              >
                Add To Tray
              </Button>
              <WhiteSpace />
            </WingBlank>
          </ScreenFooter>
        </div>
      )}
    </>
  );
};

export default MenuItemShow;
