import React from 'react';
import { motion, Variants } from 'framer-motion';

interface Props {
  children: React.ReactNode;
}

const transition = {
  duration: 0.4,
  x: { type: 'spring', stiffness: 300, damping: 200 },
};

const variants: Variants = {
  enter: () => {
    return { x: 0, opacity: 1 };
  },
  exit: () => {
    return { x: '-50%', opacity: 0, transition: { duration: 0.3 } };
  },
};
const SlideMotion: React.FC<Props> = ({ children, ...rest }) => {
  return (
    <motion.div
      style={{ height: '100vh' }}
      transition={transition}
      initial="exit"
      exit="exit"
      animate="enter"
      variants={variants}
      {...rest}
    >
      {children}
    </motion.div>
  );
};

export default SlideMotion;
