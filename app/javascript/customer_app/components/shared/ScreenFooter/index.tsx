import React from 'react';
import styled from 'styled-components';

const FooterContainer = styled.div`
  position: fixed;
  bottom: 0px;
  width: 100%;
`;

interface Props {
  children: React.ReactNode;
}

const ScreenFooter: React.FC<Props> = ({ children }) => {
  return <FooterContainer>{children}</FooterContainer>;
};

export default ScreenFooter;
