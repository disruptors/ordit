import React from 'react';
import { Button, Toast } from 'antd-mobile';
import { useDispatch } from 'react-redux';
import { needAction } from '@/store/entities/need';

interface CallWaiterProps {
  businessId: string;
  customerId: string | undefined;
}

const CallWaiterButton: React.FC<CallWaiterProps> = ({ businessId, customerId }) => {
  const dispatch = useDispatch();
  const callWaiter = () => {
    const params = {
      need: {
        businessId,
        customerId,
        name: 'Call Waiter',
      },
      successCallback: () => {
        Toast.success('Waiter is on the way.');
      },
    };
    dispatch(needAction.createNeed(params));
  };

  return <Button onClick={callWaiter}>Call Waiter</Button>;
};

export default CallWaiterButton;
