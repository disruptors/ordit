import NotFound from '@/business/components/NotFound';
import { getCurrentCustomer } from '@/store/session/selectors';
import React, { ComponentType, FC, ReactElement } from 'react';
import { useSelector } from 'react-redux';
import { Route, RouteProps } from 'react-router-dom';

interface Props extends RouteProps {
  children: ReactElement;
  layout?: ComponentType;
}

const PrivateRoute: FC<Props> = ({ children, layout: Layout, ...rest }) => {
  const currentCustomer = useSelector(getCurrentCustomer);

  const renderComponent = Layout ? (
    <Layout {...rest}>
      {!currentCustomer?.expiredAt ? children : <Route children={<NotFound />} />}
    </Layout>
  ) : (
    children
  );

  return <Route {...rest}>{renderComponent}</Route>;
};

export default PrivateRoute;
