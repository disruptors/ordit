import React, { ComponentType, FC } from 'react';
import { Route, RouteProps } from 'react-router-dom';

interface Props extends RouteProps {
  layout?: ComponentType;
}

const GuestRoute: FC<Props> = ({ children, layout: Layout, ...rest }) => {
  const renderComponent = Layout ? <Layout>{children}</Layout> : children;

  return <Route {...rest}>{renderComponent}</Route>;
};

export default GuestRoute;
