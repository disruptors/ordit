import { Badge, Card, Col, Row } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import PriceDisplay from '@/customer/components/shared/PriceDisplay';
import useCartStorage, { CartItemType } from '@/utils/hooks/useCartStorage';

interface Props {
  businessId: string;
}

const CartPreviewButton = styled(Card)`
  position: fixed;
  bottom: 0px;
  width: 100%;
`;

const Cart: React.FC<Props> = props => {
  const { businessId } = props;
  const [cart] = useCartStorage();
  const { items } = cart;

  const orderPrice = (item: CartItemType) => {
    return item.quantity * item.price;
  };

  const proceedToCheckout = () => {
    return `/store/${businessId}/checkout`;
  };

  const totalQuantity = () => {
    return items.map((item: CartItemType) => item.quantity).reduce((a, b) => a + b, 0);
  };

  const totalAmount = () => {
    return items.map(item => orderPrice(item)).reduce((a, b) => a + b, 0);
  };

  return (
    <div>
      {!!items.length && (
        <CartPreviewButton>
          <Row>
            <Col span={8}>
              <Badge count={totalQuantity()} />
            </Col>
            <Col span={8} style={{ textAlign: 'center' }}>
              <Link to={proceedToCheckout}>View Cart</Link>
            </Col>
            <Col span={8}>
              <span style={{ float: 'right' }}>
                <strong>
                  <PriceDisplay value={totalAmount()} />
                </strong>
              </span>
            </Col>
          </Row>
        </CartPreviewButton>
      )}
    </div>
  );
};

export default Cart;
