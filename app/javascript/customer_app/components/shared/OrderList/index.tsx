import React from 'react';
import { List, Stepper } from 'antd-mobile';
import { Row, Col } from 'antd';
import PriceDisplay from '../PriceDisplay';
import styled from 'styled-components';

interface ItemType {
  id: string;
  quantity: number;
  name: string;
  price: number;
}

interface OrderListType {
  items: ItemType[];
  onOrderChange?: Function;
}

const ListContainer = styled.div`
  .item-price {
    text-align: right;
  }
`;

const QuantityInput = styled(Stepper)`
  width: 100% !important;
  min-width: 100px;
`;

const OrderList: React.FC<OrderListType> = ({ items, onOrderChange }) => {
  const orderPrice = (item: ItemType) => {
    return item.quantity * item.price;
  };

  return (
    <ListContainer>
      <List>
        {items.map(item => (
          <List.Item wrap key={item.id} align="middle">
            <Row gutter={16}>
              <Col span={!!onOrderChange ? 8 : 4}>
                {!!onOrderChange ? (
                  <QuantityInput
                    value={item.quantity}
                    showNumber
                    min={0}
                    max={20}
                    onChange={value => onOrderChange(item, value)}
                  />
                ) : (
                  `${item.quantity} X`
                )}
              </Col>
              <Col span={!!onOrderChange ? 9 : 13}>
                <strong>{item.name}</strong>
              </Col>
              <Col span={7} className="item-price">
                <PriceDisplay value={orderPrice(item)} />
              </Col>
            </Row>
          </List.Item>
        ))}
      </List>
    </ListContainer>
  );
};

export default OrderList;
