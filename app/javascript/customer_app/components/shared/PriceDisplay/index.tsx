import NumberFormat from 'react-number-format';
import React from 'react';

const PriceDisplay = ({ value }) => {
  return <NumberFormat value={value} displayType="text" prefix="PHP" thousandSeparator />;
};

export default PriceDisplay;
