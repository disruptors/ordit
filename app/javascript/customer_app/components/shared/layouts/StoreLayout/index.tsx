import { MenuOutlined } from '@ant-design/icons';
import { Menu, NavBar } from 'antd-mobile';
import { ValueType } from 'antd-mobile/lib/menu/PropsType';
import React, { FunctionComponent, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import styled from 'styled-components';

const menuData = [
  {
    value: 'menu',
    label: 'Menu',
  },
  {
    value: 'promo_coupons',
    label: 'Promo / Coupons',
  },
  {
    value: 'inquiry',
    label: 'Inquiry',
  },
];

const NavBarContainer = styled(NavBar)`
  position: relative;
  background-color: #008ae6;
  color: #fff;
`;

const StoreLayout: FunctionComponent<any> = ({ children }) => {
  const { businessId } = useParams();
  const { push } = useHistory();
  const [show, setShow] = useState(false);

  const onLeftClickNavBar = (e: React.MouseEvent<HTMLDivElement>) => {
    e.preventDefault();
    setShow(prev => !prev);
  };

  const onChangeMenu = (menu: ValueType | undefined) => {
    if (menu?.includes('menu')) {
      push(`/store/${businessId}`);
    }

    setShow(prev => !prev);
  };

  const menuEl = (
    <Menu
      data={menuData}
      value={['menu']}
      onChange={onChangeMenu}
      height={document.documentElement.clientHeight - 45}
      level={1}
    />
  );

  return (
    <>
      <NavBarContainer
        mode="light"
        icon={<MenuOutlined />}
        onLeftClick={onLeftClickNavBar}
      />

      {/* TODO: */}
      {/* 1st condition should have loading indicator */}
      {/* no need to merge below condition */}
      {show ? menuEl : null}

      {/* don't show app when menu open */}
      {!show && children}
    </>
  );
};

export default StoreLayout;
