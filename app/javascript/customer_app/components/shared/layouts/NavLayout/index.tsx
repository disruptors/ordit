import history from '@/customer/history';
import { Icon, NavBar } from 'antd-mobile';
import React, { FunctionComponent } from 'react';
import styled from 'styled-components';

const NavBarContainer = styled(NavBar)`
  position: relative;
  background-color: #008ae6;
  color: #fff;
`;

const NavLayout: FunctionComponent<any> = ({ children }) => {
  const onLeftClickNavBar = (e: React.MouseEvent<HTMLDivElement>) => {
    e.preventDefault();
    history.goBack();
  };

  return (
    <>
      <NavBarContainer
        mode="light"
        icon={<Icon type="left" />}
        onLeftClick={onLeftClickNavBar}
      />
      {/* TODO: */}
      {/* Should have loading indicator */}
      {children}
    </>
  );
};

export default NavLayout;
