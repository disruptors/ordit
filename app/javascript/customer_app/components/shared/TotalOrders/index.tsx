import React from 'react';
import { Row, Col } from 'antd';
import PriceDisplay from '@/customer/components/shared/PriceDisplay';
import _ from 'lodash';
import { Card } from 'antd-mobile';

interface TotalOrdersProps {
  items: { quantity: number; price: number }[];
}

const TotalOrders: React.FC<TotalOrdersProps> = ({ items }) => {
  const totalAmount = () => {
    return _.sumBy(items, item => item.quantity * item.price);
  };

  const totalQuantity = () => {
    return _.sumBy(items, item => item.quantity);
  };

  return (
    <Card full>
      <Card.Body>
        <Row className="total-ctn" style={{ paddingTop: '20px' }}>
          <Col span={7} style={{ textAlign: 'left' }}>{`${totalQuantity()} Items`}</Col>
          <Col span={10} style={{ textAlign: 'right' }}>
            Total:
          </Col>
          <Col span={7} style={{ textAlign: 'right' }}>
            <PriceDisplay value={totalAmount()} />
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
};

export default TotalOrders;
