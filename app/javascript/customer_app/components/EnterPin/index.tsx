import { sessionActions } from '@/store/session';
import { Form } from '@ant-design/compatible';
import '@ant-design/compatible/assets/index.css';
import { Button } from 'antd';
import qs from 'qs';
import React, { FormEvent, useState } from 'react';
import PinInput from 'react-pin-input';
import { useDispatch } from 'react-redux';
import { useLocation, useHistory } from 'react-router-dom';
import styled from 'styled-components';

const EnterPinContainer = styled.div`
  padding-top: 30px;
  text-align: center;

  .ctn-footer {
    width: 100%;
    padding: 20px;
    position: fixed;
    bottom: 0px;
  }
`;

const EnterPin: React.FC = () => {
  const location = useLocation();
  const history = useHistory();
  const { business_id, qr_code_id } = qs.parse(location.search.slice(1));
  const [pinNumber, setPinNumber] = useState('');
  const dispatch = useDispatch();

  const onChange = (val: string) => {
    setPinNumber(val);
  };

  const validPin = () => {
    return pinNumber.length >= 4;
  };

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (validPin()) {
      const payload = {
        pinNumberUsed: pinNumber,
        qrCodeId: qr_code_id, // eslint-disable-line @typescript-eslint/camelcase
        businessId: business_id, // eslint-disable-line @typescript-eslint/camelcase
        successCallback: () => {
          history.push(`/store/${business_id}/menu_items`); // eslint-disable-line @typescript-eslint/camelcase
        },
      };

      dispatch(sessionActions.registerCustomer(payload));
    }
  };

  return (
    <EnterPinContainer>
      <h1>Enter Pin</h1>
      <p>Located bellow the QR Code</p>

      <Form onSubmit={handleSubmit}>
        <PinInput length={4} focus type="numeric" onChange={onChange} />
        <Form.Item className="ctn-footer">
          <Button
            block
            disabled={!validPin()}
            size="large"
            type="primary"
            htmlType="submit"
          >
            Enter
          </Button>
        </Form.Item>
      </Form>
    </EnterPinContainer>
  );
};

export default EnterPin;
