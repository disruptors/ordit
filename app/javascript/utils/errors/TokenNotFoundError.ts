import { BaseError } from '@/utils/errors/BaseError';

export class TokenNotFoundError extends BaseError {
  constructor(message?: string) {
    super(message || 'Token does not exist');
    this.name = 'TokenNotFoundError';
  }
}
