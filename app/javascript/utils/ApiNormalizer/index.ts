import normalizer from 'json-api-normalizer';

export default { normalize: normalizer };
