import Text from '@/utils/Text';

test('Converts camel cased string into snake case and also converts it to lower case', () => {
  expect(Text.camelToSnakeCase('camelCase')).toBe('camel_case');
});
