const camelToSnakeCase = (str: string) =>
  str.replace(/[A-Z]/g, letter => `_${letter.toLowerCase()}`);

export const trimDowncase = (str: string) => str.trim().toLowerCase();

export const isDash1st = (str: string): boolean => /^-/.test(str);

export default { camelToSnakeCase, isDash1st };
