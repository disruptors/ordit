// Values should be based here https://github.com/ant-design/ant-design/blob/master/components/style/themes/default.less
const colors = {
  primaryColor: '#1890ff', // primary color for all components
  linkColor: '#1890ff', // link color
  successColor: '#52c41a', // success state color
  warningColor: '#faad14', // warning state color
  errorColor: '#f5222d', // error state color
  headingColor: 'rgba(0, 0, 0, 0.85)', // heading text color
  textColor: 'rgba(0, 0, 0, 0.65)', // major text color
  textColorSecondary: 'rgba(0, 0, 0, 0.45)', // secondary text color
  disabledColor: 'rgba(0, 0, 0, 0.25)', // disable state color
  white: '#fff',
  black: '#000',
};

const fonts = {
  fontSizeBase: '14px', // major text font size
  fontSizeLg: '16px',
  fontSizeSm: '12px',
};

const theme = {
  ...colors,
  ...fonts,
};

export default theme;
