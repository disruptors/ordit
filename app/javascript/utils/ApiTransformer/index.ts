import { Attributes, ResourceData } from 'json-api-normalizer';

type NormalizedData<A> = {
  [id: string]: ResourceData<A>;
};

export type TransformedData<A extends Attributes> = ResourceData<A>['attributes'] &
  Omit<ResourceData<A>, 'attributes' | 'relationships'> & {
    // FIXME: Find a way to dynamically set relationship data type
    [relationship: string]: any;
  };

export interface TransformedEntities<A extends Attributes = Attributes> {
  [id: string]: TransformedData<A>;
}

// This expects a normalized structure from ApiNormalizer.
export const transform = <A extends Attributes = Attributes>(
  normalizedData: NormalizedData<A>
): TransformedEntities<A> =>
  Object.keys(normalizedData).reduce((transformed, entityId) => {
    const { attributes, relationships, id, type } = normalizedData[entityId];

    const associated = Object.keys(relationships).reduce((associated, entity) => {
      const { data } = relationships[entity];
      associated[entity] = data instanceof Array ? data.map(d => d.id) : data?.id || null;
      return associated;
    }, {});

    transformed[entityId] = {
      id,
      type,
      ...associated,
      ...attributes,
    };
    return transformed;
  }, {});

export default {
  transform,
};
