import { notification } from 'antd';

interface ApiError {
  success?: boolean;
  error?: string;

  // Validation error
  errors?: string[];

  // Rails specific errors.
  status?: string;
  exception?: string;
}

const notify = (error: Error | ApiError) => {
  if (error instanceof Error) {
    // Frontend application / javascript errors
    notification['error']({ message: error.message });
    console.error(error);
  } else {
    // API errors
    if (error.errors) {
      if (error.errors instanceof Array) {
        error.errors.map((error: string) => {
          notification['error']({ message: error });
        });
      } else {
        // Unexpected errors. We may want to post these errors to slack.
        notification['error']({ message: 'Something went wrong.' });
        console.error(error.errors);
      }
    }

    if (error.error) {
      notification['error']({ message: error.error || 'Something went wrong.' });
    }
  }
};

export default {
  notify,
};
