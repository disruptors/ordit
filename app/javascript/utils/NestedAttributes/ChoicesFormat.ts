import { ChoicesFormValues } from '@/business/components/MenuItems/MenuItemForm';
import { Choices, ChoiceOptions } from '@/schemas';
import { isDash1st } from '../Text';

const ChoicesFormat = (choices: ChoicesFormValues) =>
  Object.keys(choices || {})
    .map((choiceKey: string) => {
      const { name, choiceOptions = {} } = choices[choiceKey];

      const choice: Partial<Choices> & {
        choiceOptionsAttributes: Array<Partial<ChoiceOptions>>;
      } = { name, choiceOptionsAttributes: [] };

      if (!isDash1st(choiceKey)) {
        choice.id = choiceKey;
      }

      const options = Object.keys(choiceOptions).map((choiceOptKey: string) => {
        const choiceOption: Partial<ChoiceOptions> = choiceOptions[choiceOptKey];

        if (!isDash1st(choiceOptKey)) {
          choiceOption.id = choiceOptKey;
        }

        return choiceOption;
      });

      if (!isDash1st) {
        choice.id = choiceKey;
        choice.active = options.length === 0;
      }

      return { ...choice, choiceOptionsAttributes: options };
    })
    .filter(choice => choice.id || choice.choiceOptionsAttributes.length > 0);

export const ChoicesInactiveFormat = (
  choiceKey: string,
  choiceOptionKeys: { [key: string]: Array<string> }
) => {
  const choiceOptionsAttributes = choiceOptionKeys[choiceKey]
    .filter((optionKey: string) => !isDash1st(optionKey))
    .map((optionKey: string) => ({
      id: optionKey,
      active: false,
    }));

  return {
    id: choiceKey,
    active: false,
    choiceOptionsAttributes,
  };
};

export default ChoicesFormat;
