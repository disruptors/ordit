import { ActionType } from '@/store/entities/helpers';
import { sessionActions } from '@/store/session';
import ErrorNotifier from '@/utils/ErrorNotifier';
import { call, put } from 'redux-saga/effects';

function* requestCycle(cb: any, action: ActionType) {
  try {
    yield put({ type: 'REQUEST_TRIGGER', payload: action.type });
    yield call(cb, action);
  } catch (error) {
    if (
      ['TokenNotFoundError'].includes(error.name) ||
      ['SESSION_VALIDATE_CUSTOMER', 'SESSION_VALIDATE'].includes(action.type)
    ) {
      yield put(sessionActions.clear());
    } else {
      ErrorNotifier.notify(error);
    }
  } finally {
    yield put({ type: 'REQUEST_DONE', payload: action.type });
  }
}

export default {
  requestCycle,
};
