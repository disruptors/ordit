export const required = (field: string) => ({
  required: true,
  message: `Please input ${field}`,
});
