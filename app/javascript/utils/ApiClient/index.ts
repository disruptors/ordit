import UserAuth from '@/utils/UserAuth';
import { notification } from 'antd';
import { Toast } from 'antd-mobile';
import qs from 'qs';

const defaultOptions = {
  showAlerts: false,
  successMessage: null,
  headers: {},
};

const post = async (url: string, body = {}, options: any = defaultOptions) => {
  try {
    const response = await fetch(url, {
      method: 'POST',
      headers: {
        ...tokenHeaders(),
        ...(options.headers || {}),
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    });
    if (!response.ok) {
      throw response;
    } else if (!!options.showAlerts) {
      notifySuccess(options.successMessage || 'Update Successful!');
    }

    const json = await response.json();
    return { headers: response.headers, ...json };
  } catch (err) {
    if (err.name === 'AbortError') {
      throw err;
    }

    throw await err.json();
  }
};

const patch = async (url: string, body = {}, options: any = defaultOptions) => {
  try {
    const response = await fetch(url, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        ...tokenHeaders(),
        ...(options.headers || {}),
      },
      body: JSON.stringify(body),
    });

    if (!response.ok) {
      throw response;
    } else if (!!options.showAlerts) {
      notifySuccess(options.successMessage || 'Update Successful!');
    }

    return await response.json();
  } catch (err) {
    if (err.name === 'AbortError') {
      throw err;
    }

    throw await err.json();
  }
};

const get = async <R = any>(
  url: string,
  query = {},
  options?: RequestInit
): Promise<R> => {
  try {
    if (Object.keys(query).length > 0) {
      url = `${url}?${qs.stringify(query)}`;
    }

    const response = await fetch(url, {
      method: 'GET',
      ...options,
      headers: {
        ...options?.headers,
        ...tokenHeaders(),
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    });

    if (!response.ok) {
      throw response;
    }

    return await response.json();
  } catch (err) {
    if (err.name === 'AbortError') {
      throw err;
    }

    throw await err.json();
  }
};

const destroy = async (url: string, options: any = defaultOptions) => {
  try {
    const response = await fetch(url, {
      method: 'DELETE',
      headers: {
        ...tokenHeaders(),
        ...(options.headers || {}),
        'Content-Type': 'application/json',
      },
    });

    if (!response.ok) {
      throw response;
    } else if (options.showAlerts) {
      notifySuccess('Remove Successful!');
    }
  } catch (err) {
    if (err.name === 'AbortError') {
      throw err;
    }

    throw await err.json();
  }
};

const tokenHeaders = () => {
  const { token, uid, client } = UserAuth.getData();
  const headers = {
    'access-token': `${token}`,
    uid: `${uid}`,
    client: `${client}`,
  };

  return UserAuth.isLoggedIn() ? headers : {};
};

const notifySuccess = (message: string) => {
  const { prefix } = UserAuth.getData();
  if (prefix === 'customer') {
    Toast.success(message);
  } else {
    notification['success']({ message });
  }
};

export default {
  post,
  get,
  patch,
  destroy,
};
