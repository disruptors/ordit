import { END, eventChannel } from 'redux-saga';
import ApiNormalizer from '@/utils/ApiNormalizer';
import consumer from './consumer';
import { notification } from 'antd';

function createChannel(channelName: string, payload: any) {
  return eventChannel(emmiter => {
    const channel = consumer.subscriptions.create(
      {
        channel: channelName,
        ...payload,
      },
      {
        connected() {
          console.log('connected');
        },
        received(data: any) {
          emmiter(data);
        },
        disconnected() {
          emmiter(END);
        },
      }
    );

    return () => {
      channel.unsubscribe();
    };
  });
}

// Temporary way of alerting notifications from sockets.
// Ideal would be is to create a component and attach to layout
// and listen for events in the store.
function notifyEvent(payload: any) {
  const normalized = ApiNormalizer.normalize(payload);
  Object.keys(normalized.event).map((id: string) => {
    notification.info({
      message: normalized.event[id].attributes.name,
      description: normalized.event[id].attributes.description,
    });
  });
}

export default {
  createChannel,
  notifyEvent,
};
