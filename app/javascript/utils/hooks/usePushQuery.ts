import identity from 'lodash/identity';
import pickBy from 'lodash/pickBy';
import qs from 'qs';
import { useEffect, useState } from 'react';
import { useHistory, useRouteMatch } from 'react-router-dom';

const usePushQuery = () => {
  const { push } = useHistory();
  const { path } = useRouteMatch();
  const [query, setQuery] = useState<{ [param: string]: any } | undefined>();

  useEffect(() => {
    if (query) {
      push({ pathname: path, search: qs.stringify(pickBy(query, identity)) });
    }
  }, [path, push, query]);

  return setQuery;
};

export default usePushQuery;
