import { Role } from '@/schemas';
import { getCurrentUser } from '@/store/session/selectors';
import { useSelector } from 'react-redux';

const isRestricted = (allowedRoles: Role[], currentRole: Role) =>
  allowedRoles.length > 0 && !allowedRoles.includes(currentRole);

const useRestrictUser = (allowedRoles: Role[]) => {
  const currentUser = useSelector(getCurrentUser);

  if (!currentUser) {
    return true;
  }

  return isRestricted(allowedRoles, currentUser.role);
};

export default useRestrictUser;
