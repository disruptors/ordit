import ApiClient from '@/utils/ApiClient';
import ErrorNotifier from '@/utils/ErrorNotifier';
import qs from 'qs';
import { useEffect, useMemo, useState } from 'react';

interface Query {
  [param: string]: string;
}

/**
 * This is a hook which basically makes a GET request using ApliClient.get,
 * which properly handles request abortion, and keeps track of request status.
 */
const useFetch = <R = any>(
  url: string,
  /**
   * The `query` object must be memoized when passed. This is to prevent
   * it from unnecessarily re-triggering the request. We can easily memoize
   * the `query` object using the `useMemo` hook.
   */
  query?: Query
) => {
  const [isFetching, setIsFetching] = useState(false);
  const [result, setResult] = useState<R>();

  const mergedQuery = useMemo(
    // If url already has query params, merge them.
    () => ({ ...query, ...qs.parse(url.split('?')[1]) }),
    [query, url]
  );

  useEffect(() => {
    setIsFetching(true);

    const controller = new AbortController();
    const { signal } = controller;

    ApiClient.get<R>(url.split('?')[0], mergedQuery, { signal })
      .then(res => {
        setResult(res);
        setIsFetching(false);
      })
      .catch(err => {
        if (err.name !== 'AbortError') {
          setIsFetching(false);
          ErrorNotifier.notify(err);
        }
      });

    return () => {
      controller.abort();
    };
  }, [mergedQuery, query, url]);

  return {
    result,
    isFetching,
  };
};

export default useFetch;
