import { useEffect, useMemo, useState } from 'react';

const useMediaQuery = (query: string) => {
  const mediaQueryList = useMemo(() => window.matchMedia(query), [query]);

  const [matches, setMatches] = useState(mediaQueryList.matches);

  useEffect(() => {
    const handler = (e: MediaQueryListEvent) => setMatches(e.matches);

    mediaQueryList.addEventListener('change', handler);

    return () => {
      mediaQueryList.removeEventListener('change', handler);
    };
  }, [mediaQueryList]);

  return matches;
};

export default useMediaQuery;
