import { mergeEntities } from '@/store/entities/actions';
import useFetch from '@/utils/hooks/useFetch';
import { JsonApiObject, ResourceData } from 'json-api-normalizer';
import qs from 'qs';
import { useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router-dom';

/**
 * A hook that is used for getting an array of IDs from an API
 * that follows the JSON API Specification. It uses `useFetch`
 * under the hood. It also handles merging the entities.
 */
const useSearchList = (url: string) => {
  const { search } = useLocation();
  const dispatch = useDispatch();
  const [list, setList] = useState<string[]>([]);
  const [totalCount, setTotalCount] = useState<number>();
  const [perPage, setPerPage] = useState<number>();

  const urlQuery = useMemo(() => qs.parse(search.slice(1)), [search]);
  const { result, isFetching } = useFetch<JsonApiObject<ResourceData[]>>(url, urlQuery);

  useEffect(() => {
    if (result) {
      setList(result.data.map(data => data.id));
      setTotalCount(result?.meta?.totalPages * result?.meta?.perPage || undefined);
      setPerPage(result?.meta?.perPage || undefined);
      dispatch(mergeEntities(result));
    }
  }, [url, dispatch, result, urlQuery]);

  return {
    list,
    isFetching,
    totalCount,
    perPage,
  };
};

export default useSearchList;
