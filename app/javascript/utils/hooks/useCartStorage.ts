import { useState } from 'react';

export interface CartItemType {
  id: string;
  name: string;
  quantity: number;
  price: number;
}

interface UseCartStorageType {
  items: CartItemType[];
}

const defaultValue = {
  items: [],
};

const useCartStorage = (
  initialValue: UseCartStorageType = defaultValue
): [
  UseCartStorageType,
  { setCart: Function; addItem: Function; clearCart: Function; getOrder: Function }
] => {
  const key = 'cart';

  const [storedValue, setStoredValue] = useState(() => {
    try {
      const item = window.localStorage.getItem(key);
      return item ? JSON.parse(item) : initialValue;
    } catch (error) {
      return initialValue;
    }
  });

  const setCart = (value: UseCartStorageType) => {
    try {
      const valueToStore = value instanceof Function ? value(storedValue) : value;
      // Save state
      setStoredValue(valueToStore);
      // Save to local storage
      window.localStorage.setItem(key, JSON.stringify(valueToStore));
    } catch (error) {
      console.log(error);
    }
  };

  const addItem = (order: CartItemType) => {
    const cart = storedValue;
    const existingOrder = getOrder(order.id);

    if (existingOrder) {
      setCart({
        ...cart,
        items: order.quantity === 0 ? removeItem(order) : updateItem(order),
      });
    } else {
      setCart({
        ...cart,
        items: [...cart.items, order],
      });
    }
  };

  const clearCart = () => {
    setCart(defaultValue);
  };

  const updateItem = (order: CartItemType) => {
    const cart = storedValue;

    return cart.items.map((i: any) => (i.id === order.id ? { ...i, ...order } : i));
  };

  const removeItem = (order: CartItemType) => {
    const cart = storedValue;
    return cart.items.filter((item: CartItemType) => item.id !== order.id);
  };

  const getOrder = (orderId: string) => {
    const cart = storedValue;
    return cart.items.find((item: CartItemType) => item.id === orderId);
  };

  return [storedValue, { setCart, addItem, clearCart, getOrder }];
};

export default useCartStorage;
