export interface Order {
  id: string;
  qrCodeId: string;
  createdAt: string;
  updatedAt: string;
  orderStatus: string;
  name: string;
  quantity: number;
  amount: number;
}
