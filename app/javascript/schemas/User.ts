export enum Role {
  Owner = 'owner',
  Admin = 'admin',
}

export interface User {
  id: string;
  name: string;
  email: string;
  password: string;
  role: Role;
  onboardingCompleted: boolean;
  currentBusinessId: string;
  createdAt: string;
  updatedAt: string;
}
