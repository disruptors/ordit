export interface Tag {
  id: string | number;
  name: string;
  active: boolean;
  createdAt: string;
  updatedAt: string;
}
