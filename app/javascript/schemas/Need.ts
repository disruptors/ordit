export interface Need {
  name: string;
  customerId: string;
  businessId: string;
}
