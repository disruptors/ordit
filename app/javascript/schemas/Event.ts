export interface Event {
  resourceId: string;
  resourceType: string;
  action: string;
  name: string;
  description: string;
  createdAt: string;
  sittingId: string;
}
