export interface MenuItem {
  id: string;
  createdAt: string;
  updatedAt: string;
  name: string;
  description: string | null;
  category: string;
  price: string;
  categoryId: string;
  businessId: string | null;
  currentImage: string | null;
}
