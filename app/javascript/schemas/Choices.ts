export interface Choices {
  id: string;
  menuItem: string;
  menuItemId: string;
  name: string;
  active: boolean;
  optional: boolean;
  createdAt: string;
  updatedAt: string;
}
