export interface Category {
  id: string;
  businessId: string;
  name: string;
  active: boolean;
  totalMenuItems: number;
  createdAt: string;
  updatedAt: string;
}
