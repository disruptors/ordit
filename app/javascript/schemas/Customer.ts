export interface Customer {
  id: string;
  expiredAt: string;
  qrCodeId: string;
  createdAt: string;
  updatedAt: string;
  qrCodeName: string;
}
