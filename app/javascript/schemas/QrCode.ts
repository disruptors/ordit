export interface QrCode {
  id: string;
  createdAt: string;
  updatedAt: string;
  name: string;
  pinNumber: string;
  svg: string;
  storeUrl: string;
  currentSittingId: string;
  sittingCustomersCount: number;
}
