export interface Sitting {
  id: string;
  qrCodeId: string;
  createdAt: string;
  updatedAt: string;
  name: string;
  totalOrderAmount: string;
  orders: any[];
}
