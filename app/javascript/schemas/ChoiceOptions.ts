export interface ChoiceOptions {
  id: string;
  choice: string;
  choiceId: string;
  name: string;
  price: string;
  active: boolean;
  createdAt: string;
  updatedAt: string;
}
