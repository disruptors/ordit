export interface Business {
  avatar: string;
  currentMenuId: string | number;
  id: string | number;
  location: string;
  name: string;
  phone: string;
  createdAt: string;
  updatedAt: string;
}
