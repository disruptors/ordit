class Event
  attr_accessor :id, :name, :resource_id,
    :resource_type, :creator_id, :creator_type,
    :created_at, :updated_at, :description

  def initialize(**args)
    args.keys.each do |key|
      send("#{key}=", args[key])
    end

    compose_description
  end

  def compose_description
    if creator_type == 'Customer'
      customer = Customer.find(creator_id)
      self.description = "#{name} by Table #{customer.sitting.name}"
    elsif creator_type == 'User'
      user = User.find(creator_id)
      self.description = "#{name} by #{user.current_business.name}"
    else
      self.description = "#{name}"
    end
  end
end
