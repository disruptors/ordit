class Auth
  include ActiveModel::Model

  attr_accessor :callback_url, :pin_number, :qr_code_id, :business_id
end
