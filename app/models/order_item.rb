class OrderItem < ApplicationRecord
  belongs_to :order
  belongs_to :menu_item
  belongs_to :customer

  validates :amount, presence: true
end
