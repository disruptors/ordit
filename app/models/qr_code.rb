class QrCode < ApplicationRecord
  belongs_to :business

  has_many :orders, dependent: :destroy
  has_many :customers, dependent: :destroy
  has_many :sittings, dependent: :destroy

  validates :pin_number, presence: true # 4 digit pin number
  validates :name, presence: true

  before_validation :normalize
  after_create :generate_qr_code_image

  accepts_nested_attributes_for :sittings

  def table_name
    "Table #{name}"
  end

  def generate_four_digit_number
    4.times.map { rand(1..9) }.join
  end

  def generate_qr_code_image
    ::QrCodes::GenerateImageService.run(qr_code: self)
  end

  def sitting
    sittings.find_by(active: true)
  end

  private

  def normalize
    self.pin_number ||= generate_four_digit_number
  end
end
