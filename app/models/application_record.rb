class ApplicationRecord < ActiveRecord::Base
  include ::ActiveStorageSupport::SupportForBase64

  self.abstract_class = true

  # Restrict fields depending on user/accessor.
  # By default allows all.
  def self.accessible_fields_by(accessor)
    accessor ? column_names.map(&:to_sym) : []
  end
end
