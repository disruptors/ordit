class Plan < ApplicationRecord
  BASIC = {
    name: 'BASIC',
    menu_item_limit: 20,
    menu_item_image_limit: 1,
    qr_code_limit: 15,
    price: 0
  }

  PREMIUM = {
    name: 'PREMIUM',
    menu_item_limit: 40,
    menu_item_image_limit: 3,
    qr_code_limit: 45,
    price: 200
  }

  belongs_to :user

  validates :name, presence: true, inclusion: { in: [BASIC, PREMIUM].map{|plan_type| plan_type[:name]} }

  after_initialize :default_values

  private

  def default_values
    return if name.present?

    BASIC.each do |key, val|
      send("#{key}=", val)
    end
  end
end
