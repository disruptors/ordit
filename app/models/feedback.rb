class Feedback < ApplicationRecord
  belongs_to :business
  belongs_to :customer

  validates :rate, presence: true
end
