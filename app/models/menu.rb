class Menu < ApplicationRecord
  has_many :menu_items, dependent: :nullify
  belongs_to :business
end
