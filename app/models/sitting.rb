class Sitting < ApplicationRecord
  include ::EventExtensions

  belongs_to :business
  belongs_to :qr_code
  has_many :customers, dependent: :restrict_with_error, inverse_of: :sitting
  has_many :orders, dependent: :restrict_with_error

  accepts_nested_attributes_for :orders
  accepts_nested_attributes_for :customers

  before_validation :normalize

  validate :no_active_sitting

  delegate :name, :table_name, to: :qr_code

  def normalize
    self.active = true if new_record?
    self.business_id ||= qr_code&.business_id
  end

  def no_active_sitting
    return if self.class.exists?(id)

    if qr_code.sittings.exists?(active: true)
      errors.add(:base, 'Table is still occupied.')
    end
  end

  def total_order_amount
    orders.where.not(order_status: 'cancelled').sum(:amount)
  end
end
