# frozen_string_literal: true

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  include DeviseTokenAuth::Concerns::User

  has_many :businesses, dependent: :restrict_with_error

  validates :email, presence: true

  has_one :plan, dependent: :nullify
  has_role
  has_current :business

  before_validation :normalize

  accepts_nested_attributes_for :plan

  def self.accessible_fields_by(accessor)
    restricted = []
    restricted = [:role] unless accessor&.admin_role?
    super - restricted
  end

  private

  def normalize
    set_role
    set_plan
  end

  def set_role
    self.role ||= 'owner'
  end

  def set_plan
    build_plan(name: Plan::BASIC['name'], user_id: id) if plan.nil?
  end
end
