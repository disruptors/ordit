# frozen_string_literal: true

class UserAbility
  include CanCan::Ability

  def initialize(user)
    if user.nil?
      can :read, :all
    else
      can :manage, :search
      can :manage, Choice
      can :manage, ChoiceOption

      if user.admin_role?
        can :manage, User
        can :manage, Business
      else
        can :manage, Business, user_id: user.id
        can :manage, User, id: user.id
      end

      current_business_id = user.current_business&.id

      can :manage, Menu,     business_id: current_business_id
      can :manage, MenuItem, business_id: current_business_id
      can :manage, QrCode,   business_id: current_business_id
      can :manage, Customer, business_id: current_business_id
      can :manage, Category, business_id: current_business_id
      can :manage, Order,    business_id: current_business_id
      can :manage, Sitting,  business_id: current_business_id
      can :manage, Feedback, business_id: current_business_id
    end
  end
end
