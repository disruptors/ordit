class CustomerAbility
  include CanCan::Ability

  def initialize(customer)
    if customer
      can :manage, Customer, id: customer.id
      can :manage, Sitting, id: customer.sitting_id
      can :manage, Order, customer_id: customer.id
      can :read, Menu, business_id: customer.business_id
      can :read, MenuItem, business_id: customer.business_id
      can :read, Business, id: customer.business_id
      can :manage, Feedback, customer_id: customer.business_id
      can :read, QrCode, id: customer.qr_code_id
    end
  end
end
