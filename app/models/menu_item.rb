class MenuItem < ApplicationRecord
  include Rails.application.routes.url_helpers
  include Plan::MenuItemExtensions

  belongs_to :menu
  belongs_to :business
  belongs_to :category, optional: true
  belongs_to :menu_item, inverse_of: :add_ons, optional: true

  has_many :orders, dependent: :restrict_with_error
  has_many :add_ons, class_name: 'MenuItem', dependent: :nullify, inverse_of: :menu_item
  has_many :choices
  has_many :choice_options, through: :choices

  accepts_nested_attributes_for :choices
  accepts_nested_attributes_for :choice_options

  validates :name, presence: true
  validates :price, presence: true
  validates :name, uniqueness: { scope: :business_id }

  before_validation :hydrate_menu
  validate :images_validation

  has_many_base64_attached :images

  def current_image
    images.first
  end

  private

  def hydrate_menu
    self.menu_id ||= business.current_menu.id
  end

  def images_validation
    unless images.attached?
      errors[:base] << 'Need to provide an image'
    end
  end
end
