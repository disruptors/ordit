# This extension attaches an event to a resource object. 
# Used for realtime/ActionCable purposes
module EventExtensions
  extend ActiveSupport::Concern

  included do
    attr_accessor :event_ids # Make serializer work to models without a table

    resource_name = "#{self.name.downcase}"
    resource_foreign_key = "#{resource_name}_id"

    "#{self.name}Serializer".constantize.class_eval do
      has_many :events
    end

    Event.class_eval do
      attr_accessor resource_foreign_key
    end

    EventSerializer.class_eval do
      set_id resource_foreign_key
      belongs_to resource_name
    end

    def event_ids
      @event_ids ||= []
    end

    def add_event(name, creator:)
      event = Event.new(
        id: id,
        name: name,
        resource_id: id,
        resource_type: self.class.name,
        creator_id: creator.id,
        creator_type: creator.class.name,
        created_at: DateTime.current,
        updated_at: DateTime.current,
      )

      resource_name = "#{self.class.name.downcase}"
      resource_foreign_key = "#{resource_name}_id"

      event.send("#{resource_foreign_key}=", id)
      events << event
      event_ids << event.id

      event
    end
  end

  def events
    @events ||= []
  end
end
