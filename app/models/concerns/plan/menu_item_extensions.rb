module Plan::MenuItemExtensions
  extend ::ActiveSupport::Concern

  included do
    validates :images, length: {
      maximum: proc { |mi| mi.business.user.plan.menu_item_image_limit },
      message: proc { |mi| "have maxed out. You are only allowed to create #{mi.business.user.plan.menu_item_image_limit}." }
    }
  end
end
