module Plan::BusinessExtensions
  extend ::ActiveSupport::Concern

  included do
    validates :qr_codes, length: {
      maximum: proc { |bizz| bizz.user.plan.qr_code_limit },
      message: proc { |bizz| "have maxed out. You are only allowed to create #{bizz.user.plan.qr_code_limit}." }
    }
    validates :menu_items, length: {
      maximum: proc { |bizz| bizz.user.plan.menu_item_limit },
      message: proc { |bizz| "have maxed out. You are only allowed to create #{bizz.user.plan.menu_item_limit}." }
    }
  end
end
