class Business < ApplicationRecord
  include ::Plan::BusinessExtensions
  include ::EventExtensions

  belongs_to :user

  has_many :qr_codes, dependent: :destroy
  has_many :menus, dependent: :destroy
  has_many :menu_items, dependent: :destroy
  has_many :customers, dependent: :destroy
  has_many :tags, dependent: :destroy

  has_one_base64_attached :avatar
  has_current :menu

  accepts_nested_attributes_for :qr_codes, allow_destroy: true
  accepts_nested_attributes_for :menus, allow_destroy: true
  accepts_nested_attributes_for :tags, allow_destroy: true
  accepts_nested_attributes_for :user

  ## VALIDATIONS ##
  validates :name, presence: true
  validates :location, presence: true

  ## CALLBACKS ##
  before_validation :normalize

  private

  def normalize
    set_user_onboarding_completed
    setup_empty_menu
    generate_default_qr_codes
  end

  def set_user_onboarding_completed
    user.onboarding_completed = true if user
  end

  def generate_default_qr_codes
    return unless self.new_record?

    10.times do |i|
      self.qr_codes.build(name: "Table #{i + 1}")
    end
  end

  def setup_empty_menu
    return if !self.new_record? || menus.any?

    menus.build
  end
end
