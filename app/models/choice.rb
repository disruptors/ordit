class Choice < ApplicationRecord
  belongs_to :menu_item
  has_many   :choice_options

  accepts_nested_attributes_for :choice_options
end
