class Order < ApplicationRecord
  belongs_to :qr_code
  belongs_to :sitting
  belongs_to :customer
  belongs_to :menu_item
  belongs_to :business

  before_validation :normalize

  validates :quantity, presence: true
  validates :amount, presence: true
  validates :order_status, presence: true

  delegate :name, to: :menu_item

  has_options :order_status

  protected

  def normalize
    self.business_id ||= customer&.business_id
    self.order_status ||= ::OrderStatus::PENDING if new_record?
    self.amount = menu_item.price.to_f if menu_item
  end
end
