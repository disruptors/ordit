class Category < ApplicationRecord
  belongs_to :business
  has_many :menu_items, dependent: :nullify

  validates :name, uniqueness: { scope: :business_id }
end
