class Customer < ApplicationRecord
  devise :database_authenticatable, :validatable, :registerable

  include ::DeviseTokenAuth::Concerns::User
  include ::EventExtensions

  has_many :orders, dependent: :destroy
  has_many :feedbacks, dependent: :destroy
  belongs_to :business
  belongs_to :qr_code
  belongs_to :sitting, inverse_of: :customers

  validates :pin_number_used, presence: true
  validate :valid_qr_pin

  before_validation :normalize

  private

  def normalize
    self.sitting ||= qr_code.sitting || build_sitting(qr_code_id: qr_code_id, business_id: business_id, active: true)
  end

  def valid_qr_pin
    unless QrCode.exists?(id: qr_code_id, business_id: business_id, pin_number: pin_number_used)
      errors.add(:pin_number_used, 'is invalid.')
    end
  end
end
