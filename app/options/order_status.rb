class OrderStatus < ::HasOptions::Base
  define_options('PENDING', 'CANCELLED', 'SERVED')
end
