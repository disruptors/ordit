class Api::SittingsController < Upframework::ResourcesController
  def update
    super do
      return if params.dig(:orders_attributes).blank? || current_customer.nil?

      channel = @sitting.business
      broadcast_serialized(channel, resource: @sitting, event: Events::NEW_ORDER)
    end
  end

  protected

  def extra_params
    [
      orders_attributes:    [:id, :total_amount, :qr_code_id, :customer_id, :menu_item_id, :quantity],
      customers_attributes: [:id, :sitting_id]
    ]
  end
end
