class Api::OrdersController < ::Upframework::ResourcesController
  def index
    @orders = @orders.joins(:sitting).where('sittings.active = ?', true)
    super
  end
end
