class Api::BusinessesController < Upframework::ResourcesController
  before_action :prepare_avatar, only: [:create, :update]
  skip_before_action :set_base_resource, only: :index

  def index
    args = base_resource_params.slice(:name, :user_id)
    args[:view_context] = view_context
    args[:current_ability] = current_ability

    businesses, meta_pagination = ::BusinessSearch.run(args).result

    render_serialized businesses, compound_opts: meta_pagination
  end

  protected

  def prepare_avatar
    return if params[:avatar].blank?
    base_resource.avatar = { data: params[:avatar] }
  end

  def extra_params
    [tags_attributes: [:id, :name, :_destroy]]
  end
end
