class Api::NeedsController < Upframework::ResourcesController
  skip_before_action :set_base_resource
  skip_authorize_resource

  def create
    @business = Business.find(params[:business_id])
    @customer = Customer.find(params[:customer_id])
    @channel = @business

    success = handle_event

    if success
      broadcast_serialized(@channel, resource: @business, event: params[:name])
      render_serialized(@business)
    else
      render_errors @business.errors.full_messages
    end
  end

  private

  def handle_event
    valid = false
    event = Events.lookup(params[:name])

    return valid if event.nil?

    case event
    when Events::CALL_WAITER
      valid = true
    when Events::BILL_OUT
      # Check if customer can bill out
      valid = @customer.sitting.orders.any?
      @business.errors.add(:base, "No orders yet.") unless valid
    else
      valid true
    end

    valid
  end
end
