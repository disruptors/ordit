class Api::QrCodesController < ::Upframework::ResourcesController
  def extra_params
    [sittings_attributes: [:id, :active]]
  end
end
