class Api::UsersController < ::Upframework::ResourcesController
  def profile
    render_serialized current_user
  end

  protected

  def extra_params
    [:password, :password_confirmation]
  end
end
