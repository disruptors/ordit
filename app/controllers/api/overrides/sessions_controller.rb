module Api
  module Overrides
    class SessionsController < ::DeviseTokenAuth::SessionsController
      include ::Upframework::RenderExtensions

      # override this method to customise how the resource is rendered. in this case an UserSerializer
      def render_create_success
        render_serialized @resource
      end

      def become
        return unless current_user.admin_role?

        @resource = User.find(params[:id])
        @token =  @resource.create_token
        @resource.save

        sign_in(:user, @resource)
        render_create_success
      end
    end
  end
end
