module Api
  module Overrides
    class Customers::RegistrationsController < ::DeviseTokenAuth::RegistrationsController
      include ::Upframework::TransformParamKeys
      include ::Upframework::RenderExtensions

      def create
        # This block is executed after successfull save.
        # https://github.com/lynndylanhurley/devise_token_auth/blob/master/app/controllers/devise_token_auth/registrations_controller.rb#L44
        super do
          channel = @resource.business
          broadcast_serialized(channel, resource: @resource, event: Events::NEW_CUSTOMER)
        end
      end

      # override this method to customise how the resource is rendered. in this case an UserSerializer
      def render_create_success
        render_serialized @resource
      end

      def render_create_error
        if @resource.errors.present?
          render_errors @resource.errors.full_messages
        else
          super
        end
      end

      protected

      def build_resource
        super

        # Create a guest account with dummy email and password
        @resource.email           = "guest_#{Time.now.to_i}#{rand(100)}@example.com"
        @resource.pin_number_used = sign_up_params[:pin_number_used]
        @resource.qr_code_id      = sign_up_params[:qr_code_id]
        @resource.business_id     = sign_up_params[:business_id]
        @resource.password        = 'password'
      end
    end
  end
end
