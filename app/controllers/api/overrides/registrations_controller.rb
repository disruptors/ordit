module Api
  module Overrides
    class RegistrationsController < ::DeviseTokenAuth::RegistrationsController
      include ::Upframework::RenderExtensions

      skip_before_action :authenticate!, only: [:create]

      # override this method to customise how the resource is rendered. in this case an UserSerializer
      def render_create_success
        render_serialized @resource
      end

      def render_create_error
        if @resource.errors.present?
          render_errors @resource.errors.full_messages
        else
          super
        end
      end
    end
  end
end

