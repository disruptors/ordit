class Api::CustomersController < Upframework::ResourcesController
  def profile
    if current_customer
      render_serialized current_customer
    else
      render_errors ['Customer not found.'], status: :not_found
    end
  end
end
