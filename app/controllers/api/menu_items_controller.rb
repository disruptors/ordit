class Api::MenuItemsController < Upframework::ResourcesController
  before_action :prepare_image, only: [:create, :update]

  private

  def prepare_image
    return if params[:image].blank?
    base_resource.images = { data: params[:image] }
  end

  def extra_params
    [choices_attributes: [:id, :name, :active, choice_options_attributes: [:id, :name, :price, :active]]]
  end
end
