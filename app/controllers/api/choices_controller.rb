class Api::ChoicesController < Upframework::ResourcesController
  def index
    args = base_resource_params.slice(:menu_item_id)
    args[:current_ability] = current_ability

    render_serialized ::ChoiceSearch.run(args).result, includes: [:choice_options]
  end

  private

  def extra_params
    [choice_options_attributes: [:id, :active]]
  end
end
