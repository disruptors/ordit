class PagesController < ApplicationController
  skip_before_action :authenticate!, only: [:business, :customer]
  skip_authorize_resource
  layout :resolve_layout

  def business; end
  def customer; end

  private

  def resolve_layout
    case action_name
    when 'business'
      'business_application'
    when 'customer'
      'customer_application'
    else
      'application'
    end
  end
end
