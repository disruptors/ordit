class AssetsController < ApplicationController
  skip_before_action :authenticate!, only: [:qr_code]
  skip_authorize_resource

  def qr_code
    qr_code = QrCode.find(params[:id])

    respond_to do |format|
      format.svg { render inline: qr_code.svg }
    end
  end
end
