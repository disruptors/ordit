# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protect_from_forgery unless: -> { request.format.json? }
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate!

  protected

  def current_ability
    # Hackish way to access other busiess data when admin
    if current_user&.admin_role? && params[:business_id].present?
      current_user.current_business_id = params[:business_id]
    end

    @current_ability ||= current_customer ? CustomerAbility.new(current_customer) : UserAbility.new(current_user)
  end

  def authenticate!
    if current_customer
      authenticate_customer!
    else
      authenticate_user!
    end
  end

  def current_member
    current_user || current_customer
  end

  def configure_permitted_parameters
    user_params = current_user ? User.accessible_fields_by(current_user) : []
    customer_params = [:pin_number_used, :business_id, :qr_code_id]

    devise_parameter_sanitizer.permit(:sign_up, keys: user_params.concat(customer_params))
    devise_parameter_sanitizer.permit(:account_update, keys: user_params)
  end
end
