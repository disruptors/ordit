module.exports = {
  extends: ['@onoya/eslint-config/react', 'plugin:prettier/recommended'],
  rules: {
    'generator-star-spacing': ['error', {
      before: false,
      after: true,
    }],
    'no-multi-spaces': 'error',
    'react/no-children-prop': 'off',
  },
  overrides: [
    {
      files: ['**/*.ts?(x)'],
      rules: {
        '@typescript-eslint/no-explicit-any': 'off', // Temporarily allow explicit any
      },
    }
  ],
};
