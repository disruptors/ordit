Rails.application.routes.draw do

  mount_devise_token_auth_for 'Customer', at: 'api/customer_auth', controllers: {
    registrations: 'api/overrides/customers/registrations'
  }

  mount_devise_token_auth_for 'User', at: 'api/auth', controllers: {
    sessions: 'api/overrides/sessions',
    registrations: 'api/overrides/registrations'
  }

  mount ::Upframework::Engine => "/api"
  ::Upframework::Services::Routes.load namespace: "api"
  #::Upframework::Searches::Routes.load namespace: "api"

  namespace :api, defaults: { format: 'json' } do
    devise_scope :user do
      post 'auth/become', to: 'overrides/sessions#become'
    end

    resources :users do
      get :profile, to: 'users#profile', on: :collection
    end

    resources :customers do
      get :profile, to: 'customers#profile', on: :collection
    end

    # NOTE: As much as possible mentain a 2 level deep route resource
    # ex.
    # good: orders/1/order_items/2
    # acceptable: sitting/1/orders/2/order_items/3
    # bad: business/1/sitting/2/orders/2/order_items/3

    resources :businesses

    resources :needs

    resources :users do
      resources :businesses
    end

    resources :qr_codes do
      get 'image',       on: :member
      post 'bulk_create', on: :collection

      resources :orders
      resources :sittings
    end

    resources :sittings

    resources :orders do
      resources :order_items
    end

    resources :menus do
      resources :menu_items
    end

    resources :menu_items

    resources :categories,     only: [:index, :create, :update]
    resources :choices,        only: [:index, :update]
    resources :choice_options, only: [:update]

    resources :feedbacks
  end

  get '/assets/qr_codes/:id', to: 'assets#qr_code', as: :qr_code, constraints: { format: 'svg' }

  get '/store/:id' => 'pages#customer', as: :store
  get '/store/:id/*path', to: 'pages#customer'
  get '/auths/new' => 'pages#customer', as: :new_auth

  root to: 'pages#business'
  get '*path', to: 'pages#business', constraints: ->(request) do
    !request.xhr? && request.format.html? && !request.path.match?(/(\/cable|\/rails).*/)
  end
end
