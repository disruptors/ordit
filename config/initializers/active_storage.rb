Rails.application.config.to_prepare do
  ActiveStorage::Blob.class_eval do
    require_dependency "active_storage/blob/analyzable"
    require_dependency "active_storage/blob/identifiable"
    require_dependency "active_storage/blob/representable"
  end
end
