process.env.NODE_ENV = process.env.NODE_ENV || 'production';

const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const environment = require('./environment');

if (process.env.ANALYZE_BUNDLE === 'true') {
  environment.plugins.append(
    'BundleAnalyzer',
    new BundleAnalyzerPlugin({
      generateStatsFile: true,
    })
  );
}

module.exports = environment.toWebpackConfig();
