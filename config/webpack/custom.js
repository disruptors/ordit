const path = require('path');

const resolvePath = pathName => path.resolve(__dirname, '..', '..', pathName);

module.exports = {
  resolve: {
    alias: {
      // When updating alias, you should also update `tsconfig.js`
      '@/business': resolvePath('app/javascript/business_app'),
      '@/customer': resolvePath('app/javascript/customer_app'),
      '@/utils': resolvePath('app/javascript/utils'),
      '@/store': resolvePath('app/javascript/store'),
      '@/api': resolvePath('app/javascript/api'),
      '@/schemas': resolvePath('app/javascript/schemas'),
      '@/flyd': resolvePath('app/javascript/flyd'),
    },
  },
};
