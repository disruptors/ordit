# Ordit

## System dependencies

- Docker & Docker Compose
- Ruby 2.6.3
- Node
- Yarn

## Setup

Clone the repository:

```sh
git clone git@gitlab.com:disruptors/ordit.git
```

Create a copy of `.env` file:

```sh
cp .env.sample .env
```

_(You may also want to update the PORT variable)_

Start up the containers for services such as PostgreSQL using docker:

```sh
docker-compose up -d
```

Install backend dependencies:

```sh
bundle install
```

Install frontend dependecies:

```sh
yarn --check-files
```

Create database and run migration:

```sh
rails db:create

rails db:migrate
```

Start the app:

```sh
rails s
```

In a different tab/pane, run webpack development server for frontend hot reloading:

```sh
./bin/webpack-dev-server
```

Your app will be running under the PORT specified in the `.env` file.

## Analyzing Webpack Bundle

To analyze a production webpack bundle, run the command below:

```sh
ANALYZE_BUNDLE=true NODE_ENV=production ./bin/webpack
```

Running the command above will open a webpage with the webpack bundle visualization on port *8888*.

## Misc

### Building a Production bundle

```sh
rails assets:precompile
```

### Creating an Admin account from a fresh install

```sh
rails c

User.create!(email: "admin@test.com", password: "Testing12345", role: "admin")
```
