module HasCurrent
  module ActiveRecord
    extend ActiveSupport::Concern

    class_methods do
      # Usage
      # ex.
      #
      # class User
      #   has_many :businesses
      #   has_current :business
      # end
      #
      # u = User.new
      # u.current_business # defaults to first business creation.
      #
      def has_current(relation, **options)
        origin_name = self.name.downcase

        # Set first records as current
        relation.to_s.classify.constantize.class_eval do
          after_create :set_current, if: Proc.new { |model| model.send(origin_name).send("current_#{relation}_id").nil? }

          define_method "set_current" do
            origin = send(origin_name)
            origin.send("current_#{relation}_id=", id)
            origin.save
          end
        end

        class_eval do
          define_method "current_#{relation}" do
            foreign_key = "current_#{relation}_id"

            unless respond_to?(foreign_key)
              raise "Must create column #{foreign_key} to #{self.class.table_name} table"
            end

            relation_id = send(foreign_key)
            relation.to_s.classify.constantize.find_by(id: relation_id)
          end
        end
      end
    end
  end
end
