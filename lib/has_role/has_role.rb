require_relative 'has_role/active_record'

module HasRole
  def self.configuration
    @configuration ||= {}
  end

  def self.configure
    yield(configuration)
  end
end

::ActiveRecord::Base.include(::HasRole::ActiveRecord)
