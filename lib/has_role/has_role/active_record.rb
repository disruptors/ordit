require 'active_support/concern'

module HasRole
  module ActiveRecord
    extend ActiveSupport::Concern

    included do
    end

    class_methods do
      # Abling models to have the role feature
      # === Usage
      #
      # class User < ActiveRecord::Base
      #   has_role
      # end
      #
      def has_role(**options)
        class_eval do
          # Set role of a resource.
          # Make sure role is listed in config/initializers/has_role.rb
          # === Example
          #
          # user.set_role(:admin)
          #
          def set_role(new_role)
            available_roles = ::HasRole.configuration[:available_roles].map(&:downcase)
            new_role = new_role.to_s.downcase

            if available_roles.exclude?(new_role)
              raise "Not a valid role!. Available roles are #{available_roles.join(",")}"
            end

            self.role = new_role
            save
          end

          def remove_role!
            self.role = nil
            save
          end

          # Define role checkers
          # === Examples
          #
          # user.admin_role?
          #
          ::HasRole.configuration[:available_roles].each do |role_name|
            define_method "#{role_name}_role?" do
              self.role == role_name.downcase
            end
          end
        end
      end
    end
  end
end
