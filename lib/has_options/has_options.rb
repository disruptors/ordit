require_relative 'has_options/active_record'
require_relative 'has_options/base'

# OPTIONS SPECS/Usage
#
# class Country < Options
#   define_constant 'US', 'PH', 'JP'
#
#   ## support nested
#   define_constant_group 'ASIA' do
#     [PH, JP]
#   end
# end
#
# Country::ALL
#=> ['US', 'PH', 'JP']
#
# Country::ASIA
#=> ['PH', 'JP']
#
# Country::US
#=> 'US'
#
# class User < ActiveRecord
#   has_option :country
# end
#
# user = User.new
# user.country = "TH"
# user.valid? => false
#
# user.country = Country::US
# user.valid? => true
#
# user.country = "US"
# user.valid? => true
#
module HasOptions
  def self.configuration
    @configuration ||= {}
  end

  def self.configure
    yield(configuration)
  end
end

ActiveSupport.on_load(:active_record) do
  ::ActiveRecord::Base.include(::HasOptions::ActiveRecord)
end
